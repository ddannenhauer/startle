import matplotlib
import matplotlib.pyplot as plt
from matplotlib import cm
import os
import copy
# get the most recent data file

#DATADIR = 'agent_data/'
#DATADIR = 'experiment_run_2/'
#DATADIR = '.'

max_runs_to_use = 20

# get the filenames
files = sorted([f for f in os.listdir(os.getcwd())])

random_actions_to_scores = {} # key is action, val is list of scores
explore_c3_actions_to_scores = {} # key is action, val is list of scores
explore_c4_actions_to_scores = {} # key is action, val is list of scores
explore_c5_actions_to_scores = {} # key is action, val is list of scores

def process_file(f,actions_to_scores):
    print("-- Processing  "+str(f))
    header = True
    success = True
    num_actions = 0
    actions_to_scores = copy.deepcopy(actions_to_scores)
    
    highest_score = 0
    highest_action_id = 0
    sum_scores = 0
    with open(f, 'r') as f:
        for line in f.readlines():
            if header:
                header = False
            else:
                row = line.strip().split(',')
                action_id = int(row[0])
                score = float(row[1])
                sum_scores += score
                num_actions+=1
                if action_id not in actions_to_scores.keys():
                    actions_to_scores[action_id] = [score]
                    #print('\tinserting action_id {}'.format(action_id))
                else:
                    actions_to_scores[action_id].append(score)

                if score > highest_score:
                    highest_score = score
                if action_id > highest_action_id:
                    highest_action_id = action_id

    average_score = sum_scores / num_actions
    #print("Average score was {}".format(average_score))
    if average_score < 0.5:
        success = False
        print("IGNORING THIS RUN B/C ITS BAD")
    
    num_highest_scores_added = 0
    for action_id, scores in actions_to_scores.items():
        if action_id > highest_action_id:
            actions_to_scores[action_id].append(highest_score)
            num_highest_scores_added+=1
                    
    print('\t...had {} actions, filled in {} with highest_score of {}, average was {} '.format(num_actions,num_highest_scores_added,highest_score,average_score))
    if not success:
        return None, success
    else:
        return actions_to_scores, success

num_random_files = 0
num_explore_c3_files = 0
num_explore_c4_files = 0
num_explore_c5_files = 0


for f in files:
    #print("f is {}".format(f))
    if '.csv' in f:
        if 'human' in f: # get the most recent file
            human_file = f
        elif 'random' in f and num_random_files < max_runs_to_use:
            new_actions_to_scores, good_run = process_file(f,random_actions_to_scores)
            if good_run:
                random_actions_to_scores = new_actions_to_scores
                num_random_files+=1
        elif 'explore' in f:
            if 'c3' in f and num_explore_c3_files < max_runs_to_use:
                new_c3_actions_to_scores, good_run = process_file(f,explore_c3_actions_to_scores)
                if good_run:
                    explore_c3_actions_to_scores = new_c3_actions_to_scores
                    num_explore_c3_files+=1
            elif 'c4' in f and num_explore_c4_files < max_runs_to_use:
                new_c4_actions_to_scores, good_run = process_file(f,explore_c4_actions_to_scores)
                if good_run:
                    explore_c4_actions_to_scores = new_c4_actions_to_scores
                    num_explore_c4_files+=1
            elif 'c5' in f and num_explore_c5_files < max_runs_to_use:
                new_c5_actions_to_scores, good_run = process_file(f,explore_c5_actions_to_scores)
                if good_run:
                    explore_c5_actions_to_scores = new_c5_actions_to_scores
                    num_explore_c5_files+=1                

random_action_ids = []
random_averages = []

for action_id in random_actions_to_scores.keys():
    scores_per_action_id = random_actions_to_scores[action_id]
    if len(scores_per_action_id) != num_random_files:
        print("random_actions_to_scores[{}] has len {} but we expected {} b/c thats the number of random files".format(action_id,len(scores_per_action_id),num_random_files))
    single_average = sum(scores_per_action_id) / len(scores_per_action_id)
    random_action_ids.append(action_id)
    random_averages.append(single_average)

explore_c3_action_ids = []
explore_c3_averages = []
for action_id in explore_c3_actions_to_scores.keys():
    scores_per_action_id = explore_c3_actions_to_scores[action_id]
    if len(scores_per_action_id) != num_explore_c3_files:
        print("explore_c3_actions_to_scores[{}] has len {} but we expected {} b/c thats the number of explore files".format(action_id,len(scores_per_action_id),num_explore_c3_files))

    single_average = sum(scores_per_action_id) / len(scores_per_action_id)
    explore_c3_action_ids.append(action_id)
    explore_c3_averages.append(single_average)

explore_c4_action_ids = []
explore_c4_averages = []
for action_id in explore_c4_actions_to_scores.keys():
    scores_per_action_id = explore_c4_actions_to_scores[action_id]
    if len(scores_per_action_id) != num_explore_c4_files:
        print("explore_c4_actions_to_scores[{}] has len {} but we expected {} b/c thats the number of explore files".format(action_id,len(scores_per_action_id),num_explore_c4_files))

    single_average = sum(scores_per_action_id) / len(scores_per_action_id)
    explore_c4_action_ids.append(action_id)
    explore_c4_averages.append(single_average)

explore_c5_action_ids = []
explore_c5_averages = []
for action_id in explore_c5_actions_to_scores.keys():
    scores_per_action_id = explore_c5_actions_to_scores[action_id]
    if len(scores_per_action_id) != num_explore_c5_files:
        print("explore_c5_actions_to_scores[{}] has len {} but we expected {} b/c thats the number of explore files".format(action_id,len(scores_per_action_id),num_explore_c5_files))

    single_average = sum(scores_per_action_id) / len(scores_per_action_id)
    explore_c5_action_ids.append(action_id)
    explore_c5_averages.append(single_average)            

print("Avg'd over {} random, {} explore (c=3), {} explore (c=4), and {} explore (c=5) runs)".format(num_random_files,num_explore_c3_files,num_explore_c4_files,num_explore_c5_files))

matplotlib.rcParams.update({'font.size':28})

#plt.plot(actions_human,scores_human,'-r',label='Human')
plt.plot(random_action_ids,random_averages,'-r',label='Random',linewidth=2.0)
plt.plot(explore_c3_action_ids,explore_c3_averages,'-xb',label='Exploration (C=3)',markevery=5)
plt.plot(explore_c4_action_ids,explore_c4_averages,'-^g',label='Exploration (C=4)',markevery=6)
plt.plot(explore_c5_action_ids,explore_c5_averages,'-om',label='Exploration (C=5)',markevery=4)
plt.legend()

plt.xlabel("Number of Actions Executed")
plt.ylabel("Accuracy")
plt.title("Learning Accuracy per Number of Actions Executed")
plt.show()

