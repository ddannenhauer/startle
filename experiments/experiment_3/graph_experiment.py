import matplotlib.pyplot as plt
from matplotlib import cm
import os
# get the most recent data file

#DATADIR = 'agent_data/'
#DATADIR = 'experiment_run_2/'
#DATADIR = '.'

# get the filenames
files = sorted([f for f in os.listdir(os.getcwd())])

random_actions_to_scores = {} # key is action, val is list of scores
explore_actions_to_scores = {} # key is action, val is list of scores

def process_file(f,actions_to_scores):
    print("-- Processing  "+str(f))
    header = True
    with open(f, 'r') as f:
        for line in f.readlines():
            if header:
                header = False
            else:
                row = line.strip().split(',')
                action_id = int(row[0])
                score = float(row[1])
                if action_id not in actions_to_scores.keys():
                    actions_to_scores[action_id] = [score]
                    #print('\tinserting action_id {}'.format(action_id))
                else:
                    actions_to_scores[action_id].append(score)

    return actions_to_scores

num_runs = 0

for f in files:
    #print("f is {}".format(f))
    if 'human' in f: # get the most recent file
        human_file = f
    elif 'random' in f:
        random_actions_to_scores = process_file(f,random_actions_to_scores)
        num_runs+=1
    elif 'explore' in f:        
        explore_actions_to_scores = process_file(f,explore_actions_to_scores)        

random_action_ids = []
random_averages = []
for action_id in random_actions_to_scores.keys():
    scores_per_action_id = random_actions_to_scores[action_id] 
    single_average = sum(scores_per_action_id) / len(scores_per_action_id)
    random_action_ids.append(action_id)
    random_averages.append(single_average)

explore_action_ids = []
explore_averages = []
for action_id in explore_actions_to_scores.keys():
    scores_per_action_id = explore_actions_to_scores[action_id] 
    single_average = sum(scores_per_action_id) / len(scores_per_action_id)
    explore_action_ids.append(action_id)
    explore_averages.append(single_average)    

        
#plt.plot(actions_human,scores_human,'-r',label='Human')
plt.plot(random_action_ids,random_averages,'-b',label='Avg Random')
plt.plot(explore_action_ids,explore_averages,'-g',label='Avg Exploration')
plt.legend()
plt.xlabel("Number of Actions Executed")
plt.ylabel("Accuracy (Avg'd over {} runs)".format(num_runs))
plt.title("Learning Accuracy per Number of Actions Executed")
plt.show()

