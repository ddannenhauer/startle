import matplotlib.pyplot as plt
from matplotlib import cm
import os
# get the most recent data file

#DATADIR = 'agent_data/'
#DATADIR = 'experiment_run_2/'
#DATADIR = '.'

# get the filenames
files = sorted([f for f in os.listdir(os.getcwd())])

random_actions_to_scores = {} # key is action, val is list of scores
explore_c3_actions_to_scores = {} # key is action, val is list of scores
explore_c4_actions_to_scores = {} # key is action, val is list of scores


def process_file(f,actions_to_scores):
    print("-- Processing  "+str(f))
    header = True
    num_actions = 0

    highest_score = 0
    highest_action_id = 0
    with open(f, 'r') as f:
        for line in f.readlines():
            if header:
                header = False
            else:
                row = line.strip().split(',')
                action_id = int(row[0])
                score = float(row[1])
                num_actions+=1
                if action_id not in actions_to_scores.keys():
                    actions_to_scores[action_id] = [score]
                    #print('\tinserting action_id {}'.format(action_id))
                else:
                    actions_to_scores[action_id].append(score)

                if score > highest_score:
                    highest_score = score
                if action_id > highest_action_id:
                    highest_action_id = action_id

    num_highest_scores_added = 0
    for action_id, scores in actions_to_scores.items():
        if action_id > highest_action_id:
            actions_to_scores[action_id].append(highest_score)
            num_highest_scores_added+=1
                    
    print('...had {} actions, filled in {} with highest_score of {} '.format(num_actions,num_highest_scores_added,highest_score))
    return actions_to_scores

num_random_files = 0
num_explore_c3_files = 0
num_explore_c4_files = 0


for f in files:
    #print("f is {}".format(f))
    if '.csv' in f:
        if 'human' in f: # get the most recent file
            human_file = f
        elif 'random' in f:
            random_actions_to_scores = process_file(f,random_actions_to_scores)
            num_random_files+=1
        elif 'explore' in f:
            if 'c3' in f:
                explore_c3_actions_to_scores = process_file(f,explore_c3_actions_to_scores)
                num_explore_c3_files+=1
            elif 'c4' in f:
                explore_c4_actions_to_scores = process_file(f,explore_c4_actions_to_scores)
                num_explore_c4_files+=1                

random_action_ids = []
random_averages = []

for action_id in random_actions_to_scores.keys():
    scores_per_action_id = random_actions_to_scores[action_id]
    if len(scores_per_action_id) != num_random_files:
        print("random_actions_to_scores[{}] has len {} but we expected {} b/c thats the number of random files".format(action_id,len(scores_per_action_id),num_random_files))
    single_average = sum(scores_per_action_id) / len(scores_per_action_id)
    random_action_ids.append(action_id)
    random_averages.append(single_average)

explore_c3_action_ids = []
explore_c3_averages = []
for action_id in explore_c3_actions_to_scores.keys():
    scores_per_action_id = explore_c3_actions_to_scores[action_id]
    if len(scores_per_action_id) != num_explore_c3_files:
        print("explore_c3_actions_to_scores[{}] has len {} but we expected {} b/c thats the number of explore files".format(action_id,len(scores_per_action_id),num_explore_c3_files))

    single_average = sum(scores_per_action_id) / len(scores_per_action_id)
    explore_c3_action_ids.append(action_id)
    explore_c3_averages.append(single_average)

explore_c4_action_ids = []
explore_c4_averages = []
for action_id in explore_c4_actions_to_scores.keys():
    scores_per_action_id = explore_c4_actions_to_scores[action_id]
    if len(scores_per_action_id) != num_explore_c4_files:
        print("explore_c4_actions_to_scores[{}] has len {} but we expected {} b/c thats the number of explore files".format(action_id,len(scores_per_action_id),num_explore_c4_files))

    single_average = sum(scores_per_action_id) / len(scores_per_action_id)
    explore_c4_action_ids.append(action_id)
    explore_c4_averages.append(single_average)        

        
#plt.plot(actions_human,scores_human,'-r',label='Human')
plt.plot(random_action_ids,random_averages,'-r',label='Avg Random')
plt.plot(explore_c3_action_ids,explore_c3_averages,'-b',label='Avg Exploration (C=3)')
plt.plot(explore_c4_action_ids,explore_c4_averages,'-g',label='Avg Exploration (C=4)')
plt.legend()
plt.xlabel("Number of Actions Executed")
plt.ylabel("Accuracy (Avg'd over {} random and {} explore (c=3) and {} explore (c=4) runs)".format(num_random_files,num_explore_c3_files,num_explore_c4_files))
plt.title("Learning Accuracy per Number of Actions Executed")
plt.show()

