import os, re
import sys
import numpy as np
from LogicHandler import LogicHandler
from itertools import combinations, product
from itertools import product
from itertools import chain
import logging
from term import get_arg_types, clean
from copy import deepcopy
import time
from agent import CrawlAIAgent
import json
from subprocess import Popen, STDOUT, PIPE


class ContextGenerator:

    def __init__(self, game_state, context_size):
        self.context_size = context_size
        self.game_state = game_state
        self.methods = {}
        self.objects = {}
        self.objInstances = {}
        self.actionParameters = []
        self.varFormat = 'v%d'
        self.reVar = re.compile('^v[0-9]+$')
        self.re_num = re.compile('^-?[0-9]+$')
        self.contexts = set()
        self.all_contexts = {}
        self.num_contexts = 0
        self.learning_file_number = 0
        self.hand_contexts = {
            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord0,ycoord1', 'wall,cell1', 'is_one_minus,ycoord0,ycoord1']": None,
            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord0,ycoord1', 'wall,cell1', 'is_one_minus,ycoord1,ycoord0']": None,
            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord1,ycoord0', 'wall,cell1', 'is_one_minus,xcoord0,xcoord1']": None,
            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord1,ycoord0', 'wall,cell1', 'is_one_minus,xcoord1,xcoord0']": None,
            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord1,ycoord1', 'wall,cell1', 'is_one_minus,ycoord0,ycoord1', 'is_one_minus,xcoord0,xcoord1']": None,
            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord1,ycoord1', 'wall,cell1', 'is_one_minus,ycoord0,ycoord1', 'is_one_minus,xcoord1,xcoord0']": None,
            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord1,ycoord1', 'wall,cell1', 'is_one_minus,ycoord1,ycoord0', 'is_one_minus,xcoord0,xcoord1']": None,
            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord1,ycoord1', 'wall,cell1', 'is_one_minus,ycoord1,ycoord0', 'is_one_minus,xcoord1,xcoord0']": None,
            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord0,ycoord1', 'closed_door,cell1', 'is_one_minus,ycoord0,ycoord1']": None,
            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord0,ycoord1', 'closed_door,cell1', 'is_one_minus,ycoord1,ycoord0']": None,
            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord1,ycoord0', 'closed_door,cell1', 'is_one_minus,xcoord0,xcoord1']": None,
            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord1,ycoord0', 'closed_door,cell1', 'is_one_minus,xcoord1,xcoord0']": None,
            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord1,ycoord1', 'closed_door,cell1', 'is_one_minus,ycoord0,ycoord1', 'is_one_minus,xcoord0,xcoord1']": None,
            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord1,ycoord1', 'closed_door,cell1', 'is_one_minus,ycoord0,ycoord1', 'is_one_minus,xcoord1,xcoord0']": None,
            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord1,ycoord1', 'closed_door,cell1', 'is_one_minus,ycoord1,ycoord0', 'is_one_minus,xcoord0,xcoord1']": None,
            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord1,ycoord1', 'closed_door,cell1', 'is_one_minus,ycoord1,ycoord0', 'is_one_minus,xcoord1,xcoord0']": None,
            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord0,ycoord1', 'deepwater,cell1', 'is_one_minus,ycoord0,ycoord1']": None,
            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord0,ycoord1', 'deepwater,cell1', 'is_one_minus,ycoord1,ycoord0']": None,
            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord1,ycoord0', 'deepwater,cell1', 'is_one_minus,xcoord0,xcoord1']": None,
            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord1,ycoord0', 'deepwater,cell1', 'is_one_minus,xcoord1,xcoord0']": None,
            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord1,ycoord1', 'deepwater,cell1', 'is_one_minus,ycoord0,ycoord1', 'is_one_minus,xcoord0,xcoord1']": None,
            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord1,ycoord1', 'deepwater,cell1', 'is_one_minus,ycoord0,ycoord1', 'is_one_minus,xcoord1,xcoord0']": None,
            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord1,ycoord1', 'deepwater,cell1', 'is_one_minus,ycoord1,ycoord0', 'is_one_minus,xcoord0,xcoord1']": None,
            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord1,ycoord1', 'deepwater,cell1', 'is_one_minus,ycoord1,ycoord0', 'is_one_minus,xcoord1,xcoord0']": None,
            "['agentat,cell0','item,cell0,item_type0,quantity0)']": None}

    # take first atom in rule, save all possible values of the variables. weed out variables as we go. sets do matter still.

    # CONTEXT FORMAT#
    # {
    # 'lists':[all combinations of variable mappings for a set of terms],
    # 'heads':{atom_head:{'count':times this head in atom set, 'args': {atom_body_arg:num times in atom set,}},},
    # 'args' :{atom_body_arg: {'count':num times arg in atom set, 'in',list of heads in set arg is in},}
    # }
    def in_context2(self, context, asp_str):
        context = json.loads(context)
        atoms = asp_str.split(".\n")
        for i in range(len(atoms)):
            temp = re.split('\(|\)', atoms[i])
            clean(temp)
            if len(temp) < 2:
                atoms[i] = ''
                continue
            head = temp[0]
            body = temp[1].split(",")
            atoms[i] = (head, body)
        clean(atoms)
        found = []
        first = True
        for c in context:
            items = c.split(",")
            head = items[0]
            body = items[1:]
            for atom in atoms:
                if head == atom[0]:
                    for i in range(len(body)):
                        cset = {'altered': False}
                        if first:
                            cset[body[i]] = atom[1][i]
                            cset['altered'] = True
                            found.append(cset)
                            first = False
                        else:
                            newfound = []
                            for s in found:
                                if body[i] in s and atom[1][i] == s[body[i]]:
                                    s['altered'] = True
                                else:
                                    ns = deepcopy(s)
                                    ns[body[i]] = atom[1][i]
                                    ns['altered'] = True
                                    newfound.append(ns)
                            found += newfound
                    for i in range(len(found)):
                        if not found[i]['altered']:
                            found[i] = ''
                        else:
                            found[i]['altered'] = False
                    clean(found)
        if len(found) > 0:
            return True
        else:
            return False

    def build_context_string(self, context, counter):  # sets up in context set for exploratory actions
        context = json.loads(context)
        vars = {}
        lifted_vars = []
        rule_body = ""
        argc = 0
        for c in context:
            items = c.split(',')
            head = items[0]
            body = items[1:]
            for i in range(len(body)):
                if body[i] in vars:
                    body[i] = "V" + str(vars[body[i]])
                else:
                    v = "V" + str(argc)
                    vars[body[i]] = argc
                    argc += 1
                    body[i] = v
                    lifted_vars.append(v)
            if head == "is_one_minus":
                rule_body += body[0] + " = " + body[1] + " - 1, "
                continue
            add_string = head + str(tuple(body)) + " "
            if len(body) == 1:
                add_string = re.sub(",", "", add_string)

            rule_body += re.sub(" ", "", add_string[:-1]) + ", "
        vars = None
        rule_body = rule_body[:-2]
        rule_head = "context" + str(counter) + str(tuple(lifted_vars))
        rule_head = re.sub(",\)", ")", rule_head)
        rule_head = re.sub("'", "", rule_head)
        rule_body = re.sub("'", "", rule_body)
        rule = rule_head + " :- " + rule_body + "."
        return (rule, lifted_vars)

    def get_context_answer_set(self, asp_str):
        self.learning_file_number += 1
        p = Popen(['clingo'], shell=False, stdout=PIPE, stderr=STDOUT, stdin=PIPE)
        textout, inp = p.communicate(input=asp_str.encode('utf-8'))
        textout = (textout.decode('utf-8'))
        return textout

    def answer_set_list_from_raw_scoring(self, answers):
        answer_set = set()
        for answer in answers.split('\n')[4].split(' '):
            head = self.get_seperated_atom(answer)[0]
            if "_perfect" in head or "_learned" in head:
                answer_set.add(answer)
        return answer_set

    def answer_set_list_from_raw_contexts(self, answers):
        answer_set = set()
        for answer in answers.split('\n')[4].split(' '):
            head = self.get_seperated_atom(answer)[0]
            if "context" in head:
                answer_set.add(head)
        return answer_set

    def get_seperated_atom(self, atom):
        vars = re.split('\(|\)', atom)
        return vars

    def in_context(self, answer_set, id):
        action_name = "context" + str(id)
        if action_name in answer_set:
            return True
        #         vars = self.get_seperated_atom(atom)
        #         vars = vars[1].split(',')
        #         counter = 0
        #         for var in vars:
        #             sub_answers[var] = lifted_vars[counter]
        #             counter = counter + 1
        #         answers.append(sub_answers)
        # if (len(answers))>0:
        #     return True
        return False

    def clean_contexts(self):
        context_str = self.game_state.asp_str + "\n"
        remove = []
        ctr = 0
        for context in self.all_contexts:
            ctr += 1
            n_context_str = context_str + self.build_context_string(context, 0)[0]
            answer_set = self.get_context_answer_set(n_context_str)
            try:
                answer_set = answer_set.split('\n')[4].split(' ')
                if 'unsafe' in answer_set:
                    remove.append(context)
            except:
                remove.append(context)
        for context in remove:
            self.all_contexts.pop(context)

    def generateContexts(self):  # TODO:read in context file if it exists instead of generate
        pddl_str = (self.game_state.get_pddl_str())
        first = True
        for line in pddl_str.split('\n'):
            if first or line == ')':  # ditch init line and last line that closes off
                first = False
                continue
            line = re.split('\(|\)| ', line)
            clean(line)
            head = line[0]
            body = line[1:]
            arg_types = get_arg_types(head)
            for i in range(0, len(body)):
                line[i + 1] = arg_types[i]
            context = ''
            for l in line:
                context += l + ','
            context = context[:-1]
            self.contexts.add(context)
        self.contexts.add("is_one_minus,xcoord,xcoord")
        self.contexts.add("is_one_minus,ycoord,ycoord")
        num_contexts = len(self.contexts)
        if num_contexts <= self.num_contexts:
            print(len(self.all_contexts))
            return
        self.num_contexts = num_contexts
        combo = self.combiner([], self.context_size)
        action_sets = {}
        for action in CrawlAIAgent.all_actions:
            action_sets[action] = 0
        all_contexts = {}
        for c in combo:
            for l in c['lists']:
                action_sets = {}
                for action in CrawlAIAgent.all_actions:
                    action_sets[action] = 0
                all_contexts[json.dumps(l)] = action_sets
        self.all_contexts = all_contexts
        return

    def combiner(self, combos, counter):
        if counter > 1:
            combos = self.combiner(combos, counter - 1)
        new_combos = []
        if combos == []:
            for c in self.contexts:
                base = {}
                terms = c.split(',')
                head = terms[0]
                if head == 'is_one_minus':
                    continue
                body = terms[1:]
                base['heads'] = {}
                base['heads'][head] = {'count': 1, 'args': {}}
                base['args'] = {}
                for b in body:
                    if b not in base['heads'][head]['args']:
                        base['heads'][head]['args'][b] = 0
                    base['heads'][head]['args'][b] = body.count(b)
                    if b not in base['args']:
                        base['args'][b] = {}
                        base['args'][b]['count'] = 0
                        base['args'][b]['in'] = set()
                    base['args'][b]['count'] += 1
                    base['args'][b]['in'].add(head)
                strings = [head + ',']
                for b in body:
                    new_strings = []
                    for i in range(0, base['args'][b]['count']):
                        new_b = b + str(i)
                        for s in strings:
                            if new_b in s:
                                continue
                            new_strings.append(s + new_b + ',')
                    strings = new_strings
                base['lists'] = []
                for s in strings:
                    base['lists'].append([s[:-1]])
                new_combos.append(base)
        else:
            crt = 0
            for con in self.contexts:
                crt += 1
                terms = con.split(',')
                head = terms[0]
                minus = False
                if head == "is_one_minus":
                    minus = True
                body = terms[1:]
                crt2 = 0
                for combo in combos:
                    # print(counter, crt,crt2)
                    crt2 += 1
                    c = deepcopy(combo)
                    if head not in c['heads']:
                        c['heads'][head] = {'count': 0, 'args': {}}
                    c['heads'][head]['count'] += 1
                    count = c['heads'][head]['count']
                    for b in body:
                        c['heads'][head]['args'][b] = body.count(b)

                    flag = False
                    if minus:
                        flag = False
                        for b in body:
                            if b not in c['args']:
                                flag = True
                                break
                            arg_count = count * c['heads'][head]['args'][b]
                            if c['args'][b]['count'] < (arg_count):
                                flag = True
                                break
                        if flag:
                            continue
                    permute = set()
                    for b in body:
                        if b not in c['args']:
                            c['args'][b] = {'count': 0, 'in': set()}
                        c['args'][b]['in'].add(head)
                        arg_count = count * c['heads'][head]['args'][b]
                        if c['args'][b]['count'] < (arg_count):
                            c['args'][b]['count'] = arg_count  # addition to total count of this arg, need to permute.
                            permute.add(b)
                    strings = [head + ',']
                    for b in body:
                        new_strings = []
                        for i in range(0, c['args'][b]['count']):
                            new_b = b + str(i)
                            for s in strings:
                                if new_b in s:
                                    continue
                                new_strings.append(s + new_b + ',')
                        strings = new_strings
                    lists = c['lists']
                    new_list = []
                    for l in lists:
                        for s in strings:
                            new_list.append(l + [s[:-1]])
                    for i in range(0, len(new_list)):
                        count = 0
                        for s in new_list[i]:
                            if "agentat" in s:
                                count += 1
                        if len(new_list[i]) != len(set(new_list[i])) or not count == 1:
                            new_list[i] = []
                        else:
                            new_list[i] = sorted(new_list[i])
                    while [] in new_list:
                        new_list.remove([])
                    # print(permute)#TODO: Permute when adding higher order args
                    c['lists'] = new_list
                    new_combos.append(c)
        return new_combos


if __name__ == "__main__":
    cg = ContextGenerator()
    lh = None
    if len(sys.argv) > 1:
        lh = LogicHandler()
        lh.load_state(sys.argv[1])

    cg.generateContexts(6, 'context6.txt')
    # allPossiblities = cg.load_contexts('context3.txt')
    # if lh is not None:
    #    allPossiblities = cg.filterByState(lh,allPossiblities)
    # cg.save_contexts(allPossiblities,'context3_load.txt')
