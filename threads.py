import time
import threading
import copy

class BaseThread(threading.Thread):
    def __init__(self, name=None, callback=None, callback_args=None, *args, **kwargs):
        target = kwargs.pop('target')
        super(BaseThread, self).__init__(target=self.target_with_callback, *args, **kwargs)
        self.callback = callback
        self.method = target
        self.callback_args = callback_args
        self.name=name
        self._return=None

    def target_with_callback(self,*args):
        self._return=self.method(*args)
        if self.callback is not None:
            self.callback(*self.callback_args)

    def __str__(self):
        return self.name

    def get_return(self):
        self.join()
        return self._return



def my_thread_job():
    # do any things here
    print ("thread start successfully and sleep for 5 seconds")
    time.sleep(5)
    print ("thread ended successfully!")


def cb(param1):
    # this is run after your thread ends
    print(get_active_count())
    print ("Finished learning {}".format(param1))

def get_active_count():
    return threading.activeCount()
# example using BaseThread with callback
# thread = BaseThread(
#     name='test',
#     target=my_thread_job,
#     callback=cb,
#     callback_args=("hello", "world")
# )
