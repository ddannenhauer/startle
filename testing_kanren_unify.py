from kanren import run, eq, membero, var, conde, Relation, fact
from unification import unify
from kanren.assoccomm import commutative, associative
import re

if __name__ == "__main__":

    str1_head = 'southeast(V3)'
    rule1 = 'southeast(V3), at(V0,V1,V2), at(V3,V4,V5), not wall(V0), V4 = V1-1, V5 = V2-1'
    v_v0 = var('v_v0') #c0
    v_v1 = var('v_v1') #x0
    v_v2 = var('v_v2') #y0
    v_v3 = var('v_v3') #c1
    v_v4 = var('v_v4')
    v_v5 = var('v_v5')

    rule2 = 'southeast(C3), at(C0,V1,V2), at(C3,V4,V5), not wall(C0), V4 = V1-1, V5 = V2-1'

    v2_v0 = var('v2_v0')
    v2_v1 = var('v2_v1')
    v2_v2 = var('v2_v2')
    v2_v3 = var('v2_v3')
    v2_v4 = var('v2_v4')
    v2_v5 = var('v2_v5')


    print(unify((('se', v_v3), ('at', v_v0, v_v1, v_v2), ('at', v_v3, v_v4, v_v5), ("notwall", v_v0)),
          (('se', v2_v3), ('at', v2_v0, v2_v1, v2_v2), ('at', v2_v3, v2_v4, v2_v5), ("notwall", v2_v3))))

    sub = 'sub'

    myeq = 'myeq'
    fact(commutative,myeq)
    fact(associative,myeq)

    v_c0 = var('v_c0')
    v_c1 = var('v_c1')
    v_x0 = var('v_x0')
    v_x1 = var('v_x1')
    v_y0 = var('v_y0')
    v_y1 = var('v_y1')

    v2_c0 = var('v2_c0')
    v2_c1 = var('v2_c1')
    v2_x0 = var('v2_x0')
    v2_x1 = var('v2_x1')
    v2_y0 = var('v2_y0')
    v2_y1 = var('v2_y1')

    # this one is a good example
    # NOT using two types of variables
    print("Good - 1 set of variables:")
    print(unify((('s',v_c0),('at',v_c0,v_x0,v_y0),('at',v_c1,v_x0,v_y1),('nw',v_c1),(myeq,v_y0,(sub,v_y1,1))),
                (('s', v_c0), ('at', v_c1, v_x0, v_y0), ('at', v_c0, v_x0, v_y1), ('nw', v_c1),
                 (myeq, v_y1, (sub, v_y0, 1)))))

    # good
    # YES using two types of variables
    print("Good - 2 set of variables:")
    print(unify(
        (('s', v_c0), ('at', v_c0, v_x0, v_y0), ('at', v_c1, v_x0, v_y1), ('nw', v_c1), (myeq, v_y0, (sub, v_y1, 1))),
        (('s', v2_c0), ('at', v2_c1, v2_x0, v2_y0), ('at', v2_c0, v2_x0, v2_y1), ('nw', v2_c1),
         (myeq, v2_y1, (sub, v2_y0, 1)))))

    # bad
    # not using two types of variables
    print("Bad - 1 set of variables:")
    print(unify(
        (('s', v_c0), ('at1', v_c0, v_x0, v_y0), ('at2', v_c1, v_x0, v_y1), ('nw', v_c1), (myeq, v_y0, (sub, v_y1, 1))),
        (('s', v_c0), ('at1', v_c1, v_x0, v_y0), ('at2', v_c0, v_x0, v_y1), ('nw', v_c1),
         (myeq, v_y0, (sub, v_y1, 1)))))

    # bad
    # yes using two types of variables
    print("Bad - 2 set of variables:")
    print(unify(
        (('s', v_c0), ('at1', v_c0, v_x0, v_y0), ('at2', v_c1, v_x0, v_y1), ('nw', v_c1), (myeq, v_y0, (sub, v_y1, 1))),
        (('s', v2_c0), ('at1', v2_c1, v2_x0, v2_y0), ('at2', v2_c0, v2_x0, v2_y1), ('nw', v2_c1),
         (myeq, v2_y0, (sub, v2_y1, 1)))))

    input("Done?")

    rule2 = 'southeast(C3), at(C0,V1,V2), at(C3,V4,V5), not wall(C0), V4 = V1-1, V5 = V2-1'

    vars = []
    for i in range(6):
        vars.append(var('V{0}'.format(i)))

    se_rel = Relation(name='southeast')
    at_rel = Relation(name='at')
    nw_rel = Relation(name='not-wall')
    eeqq_rel = Relation(name='eeqq')

    facts(se_rel, ('V3'))
    facts(at_rel,
          ('V0','V1','V2'),
          ('V3','V4','V5'))



    facts(at_rel,)
    rule1_knrn = ('se')
    eq()


    eq()

    input("Done?")
    rule1_parts = map(lambda s: s.strip(), rule1.split(", "))
    rule2_parts = map(lambda s: s.strip(), rule2.split(", "))

    relations = []

    for term in rule1_parts:
        if '(' in term and ')' in term:
            term_parts = re.split("\(|\)", term)
            term_pred_str = term_parts[0]
            term_all_vars_str = term_parts[1]
            term_vars = term_all_vars_str.split(",")

            new_rel = Relation(name='term_pred_str')



            if term_pred_str == 'at':
                if term_vars[0] not in env.keys():
                    env[term_vars[0]] = 'C{0}'.format(cell_id)
                    cell_id += 1
                if term_vars[1] not in env.keys():
                    env[term_vars[1]] = 'X{0}'.format(x_id)
                    x_id += 1
                if term_vars[2] not in env.keys():
                    env[term_vars[2]] = 'Y{0}'.format(y_id)
                    #print('term_vars[2] is {0} and env is {1}'.format(term_vars[2], env))
                    y_id += 1