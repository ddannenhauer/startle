% ## = wall
%-------------
%  #c1#|  c7
%  c2  |  #c8#
%  c3  |  c9
%  #c4#|  c10
%  c5  |  c11
%  c6  |  #c12#
%--------------

cell(c1).
cell(c2).
cell(c3).
cell(c4).
cell(c5).
cell(c6).
cell(c7).
cell(c8).
cell(c9).
cell(c10).
cell(c11).
cell(c12).

at(c1,1,1).
at(c2,1,2).
at(c3,1,3).
at(c4,1,4).
at(c5,1,5).
at(c6,1,6).
at(c7,2,1).
at(c8,2,2).
at(c9,2,3).
at(c10,2,4).
at(c11,2,5).
at(c12,2,6).

wall(c1).
wall(c4).
wall(c8).
wall(c12).

%above(C1,C2) :- cell(C1), cell(C2), at(C1,X1,Y1), at(C2,X2,Y2), C1 != C2, Y1 = Y2-1.

#pos(p1, {movenortheast(c2),movenortheast(c5),movenortheast(c6)},{movenortheast(c3)}).

#modeh(movenortheast(var(cell))).

#modeb(2,at(var(cell),var(coord),var(coord)), (positive)).
#modeb(2,wall(var(cell))).
%#modeb(2,var(coord)=var(coord)).
#modeb(2,var(coord)=var(coord)-1).
#modeb(2,var(coord)=var(coord)+1).

%#modeb(2,var(number)=var(number)).
%#modeb(2,var(number)=var(number)+const(number)).
%#modeb(1,above(var(cell),var(cell)), (anti_reflexive)).

%#constant(number, 1).
%#constant(number, 2).

#maxv(12).
#disallow_multiple_head_variables.

% IDEAL Rule to be learned
% movenortheast(C) :- at(C,X,Y), at(C2,X2,Y2), X=X2-1, Y2=Y-1, not wall(C2).
