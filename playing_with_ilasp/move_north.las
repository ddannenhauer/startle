cell(c1).
cell(c2).
cell(c3).
cell(c4).
cell(c5).
cell(c6).

at(c1,1,1).
at(c2,1,2).
at(c3,1,3).
at(c4,1,4).
at(c5,1,5).
at(c6,1,6).

wall(c1).
wall(c4).

%above(C1,C2) :- cell(C1), cell(C2), at(C1,X1,Y1), at(C2,X2,Y2), C1 != C2, Y1 = Y2-1.

#pos(p1, {movenorth(c3),movenorth(c6)},{movenorth(c2),movenorth(c5)}).

#modeh(movenorth(var(cell))).

#modeb(2,at(var(cell),var(coord),var(coord)), (positive)).
#modeb(2,wall(var(cell))).
%#modeb(1,above(var(cell),var(cell)), (anti_reflexive)).
%#modeb(2,var(coord)=var(coord)).
#modeb(2,var(coord)=var(coord)-1).
%#modeb(2,var(number)=var(number)).
%#modeb(2,var(number)=var(number)+const(number)).

%#constant(number, 1).
%#constant(number, 2).


#maxv(12).
#disallow_multiple_head_variables.

% IDEAL Rule to be learned
% movenorth(C) :- not wall(C), not wall(C2), above(C2,C).
% OR
% movenorth(C) :- cell(C), cell(C2), at(C,X,Y), at(C2,X2,Y2), X=X2, Y2=Y-1, not wall(C2).
