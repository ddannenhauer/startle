2 ~ movenorth(V0) :- at(V0,V1,V2).
2 ~ movenorth(V0) :- wall(V0).
2 ~ movenorth(V0) :- above(V0,V1).
2 ~ movenorth(V1) :- above(V0,V1).
3 ~ movenorth(V0) :- at(V0,V1,V2), at(V0,V1,V3).
3 ~ movenorth(V0) :- at(V0,V1,V2), at(V0,V3,V2).
3 ~ movenorth(V0) :- at(V0,V1,V2), at(V0,V3,V4).
3 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2).
3 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2).
3 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V4).
3 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V4).
3 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V4,V2).
3 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V4,V2).
3 ~ movenorth(V0) :- at(V0,V1,V2), not wall(V0).
3 ~ movenorth(V0) :- at(V0,V1,V2), wall(V0).
3 ~ movenorth(V0) :- at(V0,V1,V2), wall(V3).
3 ~ movenorth(V3) :- at(V0,V1,V2), wall(V3).
3 ~ movenorth(V0) :- wall(V0), wall(V1).
3 ~ movenorth(V1) :- wall(V0), wall(V1).
3 ~ movenorth(V0) :- at(V0,V1,V2), above(V0,V3).
3 ~ movenorth(V3) :- at(V0,V1,V2), above(V0,V3).
3 ~ movenorth(V0) :- at(V0,V1,V2), above(V3,V0).
3 ~ movenorth(V3) :- at(V0,V1,V2), above(V3,V0).
3 ~ movenorth(V0) :- at(V0,V1,V2), above(V3,V4).
3 ~ movenorth(V3) :- at(V0,V1,V2), above(V3,V4).
3 ~ movenorth(V4) :- at(V0,V1,V2), above(V3,V4).
3 ~ movenorth(V0) :- not wall(V0), above(V0,V1).
3 ~ movenorth(V1) :- not wall(V0), above(V0,V1).
3 ~ movenorth(V0) :- not wall(V1), above(V0,V1).
3 ~ movenorth(V1) :- not wall(V1), above(V0,V1).
3 ~ movenorth(V0) :- wall(V0), above(V0,V1).
3 ~ movenorth(V1) :- wall(V0), above(V0,V1).
3 ~ movenorth(V0) :- wall(V0), above(V1,V0).
3 ~ movenorth(V1) :- wall(V0), above(V1,V0).
3 ~ movenorth(V0) :- wall(V0), above(V1,V2).
3 ~ movenorth(V1) :- wall(V0), above(V1,V2).
3 ~ movenorth(V2) :- wall(V0), above(V1,V2).
4 ~ movenorth(V0) :- at(V0,V1,V2), at(V0,V1,V3), not wall(V0).
4 ~ movenorth(V0) :- at(V0,V1,V2), at(V0,V3,V2), not wall(V0).
4 ~ movenorth(V0) :- at(V0,V1,V2), at(V0,V3,V4), not wall(V0).
4 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V0).
4 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V0).
4 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V3).
4 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V3).
4 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V4), not wall(V0).
4 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V4), not wall(V0).
4 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V4), not wall(V3).
4 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V4), not wall(V3).
4 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V4,V2), not wall(V0).
4 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V4,V2), not wall(V0).
4 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V4,V2), not wall(V3).
4 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V4,V2), not wall(V3).
4 ~ movenorth(V0) :- at(V0,V1,V2), at(V0,V1,V3), wall(V0).
4 ~ movenorth(V0) :- at(V0,V1,V2), at(V0,V1,V3), wall(V4).
4 ~ movenorth(V4) :- at(V0,V1,V2), at(V0,V1,V3), wall(V4).
4 ~ movenorth(V0) :- at(V0,V1,V2), at(V0,V3,V2), wall(V0).
4 ~ movenorth(V0) :- at(V0,V1,V2), at(V0,V3,V2), wall(V4).
4 ~ movenorth(V4) :- at(V0,V1,V2), at(V0,V3,V2), wall(V4).
4 ~ movenorth(V0) :- at(V0,V1,V2), at(V0,V3,V4), wall(V0).
4 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), wall(V0).
4 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), wall(V0).
4 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), wall(V3).
4 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), wall(V3).
4 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), wall(V4).
4 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), wall(V4).
4 ~ movenorth(V4) :- at(V0,V1,V2), at(V3,V1,V2), wall(V4).
4 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V4), wall(V0).
4 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V4), wall(V0).
4 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V4), wall(V3).
4 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V4), wall(V3).
4 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V4,V2), wall(V0).
4 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V4,V2), wall(V0).
4 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V4,V2), wall(V3).
4 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V4,V2), wall(V3).
4 ~ movenorth(V0) :- at(V0,V1,V2), wall(V0), wall(V3).
4 ~ movenorth(V3) :- at(V0,V1,V2), wall(V0), wall(V3).
4 ~ movenorth(V0) :- at(V0,V1,V2), wall(V3), wall(V4).
4 ~ movenorth(V3) :- at(V0,V1,V2), wall(V3), wall(V4).
4 ~ movenorth(V4) :- at(V0,V1,V2), wall(V3), wall(V4).
4 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), not above(V0,V3).
4 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), not above(V0,V3).
4 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), not above(V3,V0).
4 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), not above(V3,V0).
4 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V4), not above(V0,V3).
4 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V4), not above(V0,V3).
4 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V4), not above(V3,V0).
4 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V4), not above(V3,V0).
4 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V4,V2), not above(V0,V3).
4 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V4,V2), not above(V0,V3).
4 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V4,V2), not above(V3,V0).
4 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V4,V2), not above(V3,V0).
4 ~ movenorth(V0) :- at(V0,V1,V2), at(V0,V1,V3), above(V0,V4).
4 ~ movenorth(V4) :- at(V0,V1,V2), at(V0,V1,V3), above(V0,V4).
4 ~ movenorth(V0) :- at(V0,V1,V2), at(V0,V1,V3), above(V4,V0).
4 ~ movenorth(V4) :- at(V0,V1,V2), at(V0,V1,V3), above(V4,V0).
4 ~ movenorth(V0) :- at(V0,V1,V2), at(V0,V3,V2), above(V0,V4).
4 ~ movenorth(V4) :- at(V0,V1,V2), at(V0,V3,V2), above(V0,V4).
4 ~ movenorth(V0) :- at(V0,V1,V2), at(V0,V3,V2), above(V4,V0).
4 ~ movenorth(V4) :- at(V0,V1,V2), at(V0,V3,V2), above(V4,V0).
4 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), above(V0,V3).
4 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), above(V0,V3).
4 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), above(V0,V4).
4 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), above(V0,V4).
4 ~ movenorth(V4) :- at(V0,V1,V2), at(V3,V1,V2), above(V0,V4).
4 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), above(V3,V0).
4 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), above(V3,V0).
4 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), above(V3,V4).
4 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), above(V3,V4).
4 ~ movenorth(V4) :- at(V0,V1,V2), at(V3,V1,V2), above(V3,V4).
4 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), above(V4,V0).
4 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), above(V4,V0).
4 ~ movenorth(V4) :- at(V0,V1,V2), at(V3,V1,V2), above(V4,V0).
4 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), above(V4,V3).
4 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), above(V4,V3).
4 ~ movenorth(V4) :- at(V0,V1,V2), at(V3,V1,V2), above(V4,V3).
4 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V4), above(V0,V3).
4 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V4), above(V0,V3).
4 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V4), above(V3,V0).
4 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V4), above(V3,V0).
4 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V4,V2), above(V0,V3).
4 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V4,V2), above(V0,V3).
4 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V4,V2), above(V3,V0).
4 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V4,V2), above(V3,V0).
4 ~ movenorth(V0) :- at(V0,V1,V2), wall(V3), not above(V0,V3).
4 ~ movenorth(V3) :- at(V0,V1,V2), wall(V3), not above(V0,V3).
4 ~ movenorth(V0) :- at(V0,V1,V2), wall(V3), not above(V3,V0).
4 ~ movenorth(V3) :- at(V0,V1,V2), wall(V3), not above(V3,V0).
4 ~ movenorth(V0) :- at(V0,V1,V2), not wall(V0), above(V0,V3).
4 ~ movenorth(V3) :- at(V0,V1,V2), not wall(V0), above(V0,V3).
4 ~ movenorth(V0) :- at(V0,V1,V2), not wall(V3), above(V0,V3).
4 ~ movenorth(V3) :- at(V0,V1,V2), not wall(V3), above(V0,V3).
4 ~ movenorth(V0) :- at(V0,V1,V2), not wall(V0), above(V3,V0).
4 ~ movenorth(V3) :- at(V0,V1,V2), not wall(V0), above(V3,V0).
4 ~ movenorth(V0) :- at(V0,V1,V2), not wall(V3), above(V3,V0).
4 ~ movenorth(V3) :- at(V0,V1,V2), not wall(V3), above(V3,V0).
4 ~ movenorth(V0) :- at(V0,V1,V2), not wall(V0), above(V3,V4).
4 ~ movenorth(V3) :- at(V0,V1,V2), not wall(V0), above(V3,V4).
4 ~ movenorth(V4) :- at(V0,V1,V2), not wall(V0), above(V3,V4).
4 ~ movenorth(V0) :- at(V0,V1,V2), not wall(V3), above(V3,V4).
4 ~ movenorth(V3) :- at(V0,V1,V2), not wall(V3), above(V3,V4).
4 ~ movenorth(V4) :- at(V0,V1,V2), not wall(V3), above(V3,V4).
4 ~ movenorth(V0) :- at(V0,V1,V2), not wall(V4), above(V3,V4).
4 ~ movenorth(V3) :- at(V0,V1,V2), not wall(V4), above(V3,V4).
4 ~ movenorth(V4) :- at(V0,V1,V2), not wall(V4), above(V3,V4).
4 ~ movenorth(V0) :- at(V0,V1,V2), wall(V0), above(V0,V3).
4 ~ movenorth(V3) :- at(V0,V1,V2), wall(V0), above(V0,V3).
4 ~ movenorth(V0) :- at(V0,V1,V2), wall(V0), above(V3,V0).
4 ~ movenorth(V3) :- at(V0,V1,V2), wall(V0), above(V3,V0).
4 ~ movenorth(V0) :- at(V0,V1,V2), wall(V0), above(V3,V4).
4 ~ movenorth(V3) :- at(V0,V1,V2), wall(V0), above(V3,V4).
4 ~ movenorth(V4) :- at(V0,V1,V2), wall(V0), above(V3,V4).
4 ~ movenorth(V0) :- at(V0,V1,V2), wall(V3), above(V0,V3).
4 ~ movenorth(V3) :- at(V0,V1,V2), wall(V3), above(V0,V3).
4 ~ movenorth(V0) :- at(V0,V1,V2), wall(V3), above(V0,V4).
4 ~ movenorth(V3) :- at(V0,V1,V2), wall(V3), above(V0,V4).
4 ~ movenorth(V4) :- at(V0,V1,V2), wall(V3), above(V0,V4).
4 ~ movenorth(V0) :- at(V0,V1,V2), wall(V3), above(V3,V0).
4 ~ movenorth(V3) :- at(V0,V1,V2), wall(V3), above(V3,V0).
4 ~ movenorth(V0) :- at(V0,V1,V2), wall(V3), above(V3,V4).
4 ~ movenorth(V3) :- at(V0,V1,V2), wall(V3), above(V3,V4).
4 ~ movenorth(V4) :- at(V0,V1,V2), wall(V3), above(V3,V4).
4 ~ movenorth(V0) :- at(V0,V1,V2), wall(V3), above(V4,V0).
4 ~ movenorth(V3) :- at(V0,V1,V2), wall(V3), above(V4,V0).
4 ~ movenorth(V4) :- at(V0,V1,V2), wall(V3), above(V4,V0).
4 ~ movenorth(V0) :- at(V0,V1,V2), wall(V3), above(V4,V3).
4 ~ movenorth(V3) :- at(V0,V1,V2), wall(V3), above(V4,V3).
4 ~ movenorth(V4) :- at(V0,V1,V2), wall(V3), above(V4,V3).
4 ~ movenorth(V0) :- wall(V0), wall(V1), not above(V0,V1).
4 ~ movenorth(V1) :- wall(V0), wall(V1), not above(V0,V1).
4 ~ movenorth(V0) :- wall(V0), wall(V1), not above(V1,V0).
4 ~ movenorth(V1) :- wall(V0), wall(V1), not above(V1,V0).
4 ~ movenorth(V0) :- not wall(V0), not wall(V1), above(V0,V1).
4 ~ movenorth(V1) :- not wall(V0), not wall(V1), above(V0,V1).
4 ~ movenorth(V0) :- wall(V0), not wall(V1), above(V0,V1).
4 ~ movenorth(V1) :- wall(V0), not wall(V1), above(V0,V1).
4 ~ movenorth(V0) :- wall(V0), not wall(V1), above(V1,V0).
4 ~ movenorth(V1) :- wall(V0), not wall(V1), above(V1,V0).
4 ~ movenorth(V0) :- wall(V0), not wall(V1), above(V1,V2).
4 ~ movenorth(V1) :- wall(V0), not wall(V1), above(V1,V2).
4 ~ movenorth(V2) :- wall(V0), not wall(V1), above(V1,V2).
4 ~ movenorth(V0) :- wall(V0), not wall(V2), above(V1,V2).
4 ~ movenorth(V1) :- wall(V0), not wall(V2), above(V1,V2).
4 ~ movenorth(V2) :- wall(V0), not wall(V2), above(V1,V2).
4 ~ movenorth(V0) :- wall(V0), wall(V1), above(V0,V1).
4 ~ movenorth(V1) :- wall(V0), wall(V1), above(V0,V1).
4 ~ movenorth(V0) :- wall(V0), wall(V1), above(V0,V2).
4 ~ movenorth(V1) :- wall(V0), wall(V1), above(V0,V2).
4 ~ movenorth(V2) :- wall(V0), wall(V1), above(V0,V2).
4 ~ movenorth(V0) :- wall(V0), wall(V1), above(V1,V0).
4 ~ movenorth(V1) :- wall(V0), wall(V1), above(V1,V0).
4 ~ movenorth(V0) :- wall(V0), wall(V1), above(V1,V2).
4 ~ movenorth(V1) :- wall(V0), wall(V1), above(V1,V2).
4 ~ movenorth(V2) :- wall(V0), wall(V1), above(V1,V2).
4 ~ movenorth(V0) :- wall(V0), wall(V1), above(V2,V0).
4 ~ movenorth(V1) :- wall(V0), wall(V1), above(V2,V0).
4 ~ movenorth(V2) :- wall(V0), wall(V1), above(V2,V0).
4 ~ movenorth(V0) :- wall(V0), wall(V1), above(V2,V1).
4 ~ movenorth(V1) :- wall(V0), wall(V1), above(V2,V1).
4 ~ movenorth(V2) :- wall(V0), wall(V1), above(V2,V1).
4 ~ movenorth(V0) :- wall(V0), wall(V1), above(V2,V3).
4 ~ movenorth(V1) :- wall(V0), wall(V1), above(V2,V3).
4 ~ movenorth(V2) :- wall(V0), wall(V1), above(V2,V3).
4 ~ movenorth(V3) :- wall(V0), wall(V1), above(V2,V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V0), not wall(V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V0), not wall(V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V4), not wall(V0), not wall(V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V4), not wall(V0), not wall(V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V4,V2), not wall(V0), not wall(V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V4,V2), not wall(V0), not wall(V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), wall(V0), not wall(V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), wall(V0), not wall(V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V4), wall(V0), not wall(V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V4), wall(V0), not wall(V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V4,V2), wall(V0), not wall(V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V4,V2), wall(V0), not wall(V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V0,V1,V3), wall(V0), wall(V4).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V0,V1,V3), wall(V0), wall(V4).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V0,V3,V2), wall(V0), wall(V4).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V0,V3,V2), wall(V0), wall(V4).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), wall(V0), wall(V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), wall(V0), wall(V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), wall(V0), wall(V4).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), wall(V0), wall(V4).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V3,V1,V2), wall(V0), wall(V4).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), wall(V3), wall(V4).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), wall(V3), wall(V4).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V3,V1,V2), wall(V3), wall(V4).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V4), wall(V0), wall(V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V4), wall(V0), wall(V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V4,V2), wall(V0), wall(V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V4,V2), wall(V0), wall(V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V0), not above(V0,V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V0), not above(V0,V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V0), not above(V3,V0).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V0), not above(V3,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V3), not above(V0,V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V3), not above(V0,V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V3), not above(V3,V0).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V3), not above(V3,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V4), not wall(V0), not above(V0,V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V4), not wall(V0), not above(V0,V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V4), not wall(V0), not above(V3,V0).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V4), not wall(V0), not above(V3,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V4), not wall(V3), not above(V0,V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V4), not wall(V3), not above(V0,V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V4), not wall(V3), not above(V3,V0).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V4), not wall(V3), not above(V3,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V4,V2), not wall(V0), not above(V0,V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V4,V2), not wall(V0), not above(V0,V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V4,V2), not wall(V0), not above(V3,V0).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V4,V2), not wall(V0), not above(V3,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V4,V2), not wall(V3), not above(V0,V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V4,V2), not wall(V3), not above(V0,V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V4,V2), not wall(V3), not above(V3,V0).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V4,V2), not wall(V3), not above(V3,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V0,V1,V3), wall(V4), not above(V0,V4).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V0,V1,V3), wall(V4), not above(V0,V4).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V0,V1,V3), wall(V4), not above(V4,V0).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V0,V1,V3), wall(V4), not above(V4,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V0,V3,V2), wall(V4), not above(V0,V4).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V0,V3,V2), wall(V4), not above(V0,V4).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V0,V3,V2), wall(V4), not above(V4,V0).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V0,V3,V2), wall(V4), not above(V4,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), wall(V0), not above(V0,V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), wall(V0), not above(V0,V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), wall(V0), not above(V3,V0).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), wall(V0), not above(V3,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), wall(V3), not above(V0,V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), wall(V3), not above(V0,V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), wall(V3), not above(V3,V0).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), wall(V3), not above(V3,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), wall(V4), not above(V0,V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), wall(V4), not above(V0,V3).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V3,V1,V2), wall(V4), not above(V0,V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), wall(V4), not above(V0,V4).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), wall(V4), not above(V0,V4).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V3,V1,V2), wall(V4), not above(V0,V4).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), wall(V4), not above(V3,V0).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), wall(V4), not above(V3,V0).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V3,V1,V2), wall(V4), not above(V3,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), wall(V4), not above(V3,V4).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), wall(V4), not above(V3,V4).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V3,V1,V2), wall(V4), not above(V3,V4).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), wall(V4), not above(V4,V0).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), wall(V4), not above(V4,V0).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V3,V1,V2), wall(V4), not above(V4,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), wall(V4), not above(V4,V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), wall(V4), not above(V4,V3).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V3,V1,V2), wall(V4), not above(V4,V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V4), wall(V0), not above(V0,V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V4), wall(V0), not above(V0,V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V4), wall(V0), not above(V3,V0).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V4), wall(V0), not above(V3,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V4), wall(V3), not above(V0,V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V4), wall(V3), not above(V0,V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V4), wall(V3), not above(V3,V0).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V4), wall(V3), not above(V3,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V4,V2), wall(V0), not above(V0,V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V4,V2), wall(V0), not above(V0,V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V4,V2), wall(V0), not above(V3,V0).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V4,V2), wall(V0), not above(V3,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V4,V2), wall(V3), not above(V0,V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V4,V2), wall(V3), not above(V0,V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V4,V2), wall(V3), not above(V3,V0).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V4,V2), wall(V3), not above(V3,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V0,V1,V3), not wall(V0), above(V0,V4).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V0,V1,V3), not wall(V0), above(V0,V4).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V0,V1,V3), not wall(V4), above(V0,V4).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V0,V1,V3), not wall(V4), above(V0,V4).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V0,V1,V3), not wall(V0), above(V4,V0).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V0,V1,V3), not wall(V0), above(V4,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V0,V1,V3), not wall(V4), above(V4,V0).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V0,V1,V3), not wall(V4), above(V4,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V0,V3,V2), not wall(V0), above(V0,V4).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V0,V3,V2), not wall(V0), above(V0,V4).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V0,V3,V2), not wall(V4), above(V0,V4).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V0,V3,V2), not wall(V4), above(V0,V4).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V0,V3,V2), not wall(V0), above(V4,V0).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V0,V3,V2), not wall(V0), above(V4,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V0,V3,V2), not wall(V4), above(V4,V0).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V0,V3,V2), not wall(V4), above(V4,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V0), above(V0,V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V0), above(V0,V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V3), above(V0,V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V3), above(V0,V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V0), above(V0,V4).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V0), above(V0,V4).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V0), above(V0,V4).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V3), above(V0,V4).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V3), above(V0,V4).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V3), above(V0,V4).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V4), above(V0,V4).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V4), above(V0,V4).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V4), above(V0,V4).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V0), above(V3,V0).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V0), above(V3,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V3), above(V3,V0).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V3), above(V3,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V0), above(V3,V4).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V0), above(V3,V4).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V0), above(V3,V4).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V3), above(V3,V4).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V3), above(V3,V4).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V3), above(V3,V4).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V4), above(V3,V4).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V4), above(V3,V4).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V4), above(V3,V4).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V0), above(V4,V0).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V0), above(V4,V0).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V0), above(V4,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V3), above(V4,V0).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V3), above(V4,V0).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V3), above(V4,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V4), above(V4,V0).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V4), above(V4,V0).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V4), above(V4,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V0), above(V4,V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V0), above(V4,V3).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V0), above(V4,V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V3), above(V4,V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V3), above(V4,V3).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V3), above(V4,V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V4), above(V4,V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V4), above(V4,V3).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V3,V1,V2), not wall(V4), above(V4,V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V4), not wall(V0), above(V0,V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V4), not wall(V0), above(V0,V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V4), not wall(V3), above(V0,V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V4), not wall(V3), above(V0,V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V4), not wall(V0), above(V3,V0).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V4), not wall(V0), above(V3,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V4), not wall(V3), above(V3,V0).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V4), not wall(V3), above(V3,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V4,V2), not wall(V0), above(V0,V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V4,V2), not wall(V0), above(V0,V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V4,V2), not wall(V3), above(V0,V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V4,V2), not wall(V3), above(V0,V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V4,V2), not wall(V0), above(V3,V0).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V4,V2), not wall(V0), above(V3,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V4,V2), not wall(V3), above(V3,V0).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V4,V2), not wall(V3), above(V3,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V0,V1,V3), wall(V0), above(V0,V4).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V0,V1,V3), wall(V0), above(V0,V4).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V0,V1,V3), wall(V0), above(V4,V0).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V0,V1,V3), wall(V0), above(V4,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V0,V1,V3), wall(V4), above(V0,V4).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V0,V1,V3), wall(V4), above(V0,V4).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V0,V1,V3), wall(V4), above(V4,V0).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V0,V1,V3), wall(V4), above(V4,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V0,V3,V2), wall(V0), above(V0,V4).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V0,V3,V2), wall(V0), above(V0,V4).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V0,V3,V2), wall(V0), above(V4,V0).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V0,V3,V2), wall(V0), above(V4,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V0,V3,V2), wall(V4), above(V0,V4).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V0,V3,V2), wall(V4), above(V0,V4).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V0,V3,V2), wall(V4), above(V4,V0).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V0,V3,V2), wall(V4), above(V4,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), wall(V0), above(V0,V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), wall(V0), above(V0,V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), wall(V0), above(V0,V4).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), wall(V0), above(V0,V4).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V3,V1,V2), wall(V0), above(V0,V4).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), wall(V0), above(V3,V0).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), wall(V0), above(V3,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), wall(V0), above(V3,V4).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), wall(V0), above(V3,V4).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V3,V1,V2), wall(V0), above(V3,V4).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), wall(V0), above(V4,V0).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), wall(V0), above(V4,V0).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V3,V1,V2), wall(V0), above(V4,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), wall(V0), above(V4,V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), wall(V0), above(V4,V3).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V3,V1,V2), wall(V0), above(V4,V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), wall(V3), above(V0,V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), wall(V3), above(V0,V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), wall(V3), above(V0,V4).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), wall(V3), above(V0,V4).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V3,V1,V2), wall(V3), above(V0,V4).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), wall(V3), above(V3,V0).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), wall(V3), above(V3,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), wall(V3), above(V3,V4).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), wall(V3), above(V3,V4).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V3,V1,V2), wall(V3), above(V3,V4).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), wall(V3), above(V4,V0).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), wall(V3), above(V4,V0).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V3,V1,V2), wall(V3), above(V4,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), wall(V3), above(V4,V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), wall(V3), above(V4,V3).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V3,V1,V2), wall(V3), above(V4,V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), wall(V4), above(V0,V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), wall(V4), above(V0,V3).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V3,V1,V2), wall(V4), above(V0,V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), wall(V4), above(V0,V4).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), wall(V4), above(V0,V4).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V3,V1,V2), wall(V4), above(V0,V4).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), wall(V4), above(V3,V0).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), wall(V4), above(V3,V0).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V3,V1,V2), wall(V4), above(V3,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), wall(V4), above(V3,V4).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), wall(V4), above(V3,V4).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V3,V1,V2), wall(V4), above(V3,V4).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), wall(V4), above(V4,V0).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), wall(V4), above(V4,V0).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V3,V1,V2), wall(V4), above(V4,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V2), wall(V4), above(V4,V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V2), wall(V4), above(V4,V3).
5 ~ movenorth(V4) :- at(V0,V1,V2), at(V3,V1,V2), wall(V4), above(V4,V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V4), wall(V0), above(V0,V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V4), wall(V0), above(V0,V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V4), wall(V0), above(V3,V0).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V4), wall(V0), above(V3,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V4), wall(V3), above(V0,V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V4), wall(V3), above(V0,V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V1,V4), wall(V3), above(V3,V0).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V1,V4), wall(V3), above(V3,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V4,V2), wall(V0), above(V0,V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V4,V2), wall(V0), above(V0,V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V4,V2), wall(V0), above(V3,V0).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V4,V2), wall(V0), above(V3,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V4,V2), wall(V3), above(V0,V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V4,V2), wall(V3), above(V0,V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), at(V3,V4,V2), wall(V3), above(V3,V0).
5 ~ movenorth(V3) :- at(V0,V1,V2), at(V3,V4,V2), wall(V3), above(V3,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), wall(V0), wall(V3), not above(V0,V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), wall(V0), wall(V3), not above(V0,V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), wall(V0), wall(V3), not above(V3,V0).
5 ~ movenorth(V3) :- at(V0,V1,V2), wall(V0), wall(V3), not above(V3,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), wall(V3), wall(V4), not above(V0,V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), wall(V3), wall(V4), not above(V0,V3).
5 ~ movenorth(V4) :- at(V0,V1,V2), wall(V3), wall(V4), not above(V0,V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), wall(V3), wall(V4), not above(V0,V4).
5 ~ movenorth(V3) :- at(V0,V1,V2), wall(V3), wall(V4), not above(V0,V4).
5 ~ movenorth(V4) :- at(V0,V1,V2), wall(V3), wall(V4), not above(V0,V4).
5 ~ movenorth(V0) :- at(V0,V1,V2), wall(V3), wall(V4), not above(V3,V0).
5 ~ movenorth(V3) :- at(V0,V1,V2), wall(V3), wall(V4), not above(V3,V0).
5 ~ movenorth(V4) :- at(V0,V1,V2), wall(V3), wall(V4), not above(V3,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), wall(V3), wall(V4), not above(V3,V4).
5 ~ movenorth(V3) :- at(V0,V1,V2), wall(V3), wall(V4), not above(V3,V4).
5 ~ movenorth(V4) :- at(V0,V1,V2), wall(V3), wall(V4), not above(V3,V4).
5 ~ movenorth(V0) :- at(V0,V1,V2), wall(V3), wall(V4), not above(V4,V0).
5 ~ movenorth(V3) :- at(V0,V1,V2), wall(V3), wall(V4), not above(V4,V0).
5 ~ movenorth(V4) :- at(V0,V1,V2), wall(V3), wall(V4), not above(V4,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), wall(V3), wall(V4), not above(V4,V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), wall(V3), wall(V4), not above(V4,V3).
5 ~ movenorth(V4) :- at(V0,V1,V2), wall(V3), wall(V4), not above(V4,V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), not wall(V0), not wall(V3), above(V0,V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), not wall(V0), not wall(V3), above(V0,V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), not wall(V0), not wall(V3), above(V3,V0).
5 ~ movenorth(V3) :- at(V0,V1,V2), not wall(V0), not wall(V3), above(V3,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), not wall(V0), not wall(V3), above(V3,V4).
5 ~ movenorth(V3) :- at(V0,V1,V2), not wall(V0), not wall(V3), above(V3,V4).
5 ~ movenorth(V4) :- at(V0,V1,V2), not wall(V0), not wall(V3), above(V3,V4).
5 ~ movenorth(V0) :- at(V0,V1,V2), not wall(V0), not wall(V4), above(V3,V4).
5 ~ movenorth(V3) :- at(V0,V1,V2), not wall(V0), not wall(V4), above(V3,V4).
5 ~ movenorth(V4) :- at(V0,V1,V2), not wall(V0), not wall(V4), above(V3,V4).
5 ~ movenorth(V0) :- at(V0,V1,V2), not wall(V3), not wall(V4), above(V3,V4).
5 ~ movenorth(V3) :- at(V0,V1,V2), not wall(V3), not wall(V4), above(V3,V4).
5 ~ movenorth(V4) :- at(V0,V1,V2), not wall(V3), not wall(V4), above(V3,V4).
5 ~ movenorth(V0) :- at(V0,V1,V2), wall(V0), not wall(V3), above(V0,V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), wall(V0), not wall(V3), above(V0,V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), wall(V0), not wall(V3), above(V3,V0).
5 ~ movenorth(V3) :- at(V0,V1,V2), wall(V0), not wall(V3), above(V3,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), wall(V0), not wall(V3), above(V3,V4).
5 ~ movenorth(V3) :- at(V0,V1,V2), wall(V0), not wall(V3), above(V3,V4).
5 ~ movenorth(V4) :- at(V0,V1,V2), wall(V0), not wall(V3), above(V3,V4).
5 ~ movenorth(V0) :- at(V0,V1,V2), wall(V0), not wall(V4), above(V3,V4).
5 ~ movenorth(V3) :- at(V0,V1,V2), wall(V0), not wall(V4), above(V3,V4).
5 ~ movenorth(V4) :- at(V0,V1,V2), wall(V0), not wall(V4), above(V3,V4).
5 ~ movenorth(V0) :- at(V0,V1,V2), wall(V3), not wall(V4), above(V0,V4).
5 ~ movenorth(V3) :- at(V0,V1,V2), wall(V3), not wall(V4), above(V0,V4).
5 ~ movenorth(V4) :- at(V0,V1,V2), wall(V3), not wall(V4), above(V0,V4).
5 ~ movenorth(V0) :- at(V0,V1,V2), wall(V3), not wall(V4), above(V3,V4).
5 ~ movenorth(V3) :- at(V0,V1,V2), wall(V3), not wall(V4), above(V3,V4).
5 ~ movenorth(V4) :- at(V0,V1,V2), wall(V3), not wall(V4), above(V3,V4).
5 ~ movenorth(V0) :- at(V0,V1,V2), wall(V3), not wall(V4), above(V4,V0).
5 ~ movenorth(V3) :- at(V0,V1,V2), wall(V3), not wall(V4), above(V4,V0).
5 ~ movenorth(V4) :- at(V0,V1,V2), wall(V3), not wall(V4), above(V4,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), wall(V3), not wall(V4), above(V4,V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), wall(V3), not wall(V4), above(V4,V3).
5 ~ movenorth(V4) :- at(V0,V1,V2), wall(V3), not wall(V4), above(V4,V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), wall(V0), wall(V3), above(V0,V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), wall(V0), wall(V3), above(V0,V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), wall(V0), wall(V3), above(V0,V4).
5 ~ movenorth(V3) :- at(V0,V1,V2), wall(V0), wall(V3), above(V0,V4).
5 ~ movenorth(V4) :- at(V0,V1,V2), wall(V0), wall(V3), above(V0,V4).
5 ~ movenorth(V0) :- at(V0,V1,V2), wall(V0), wall(V3), above(V3,V0).
5 ~ movenorth(V3) :- at(V0,V1,V2), wall(V0), wall(V3), above(V3,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), wall(V0), wall(V3), above(V3,V4).
5 ~ movenorth(V3) :- at(V0,V1,V2), wall(V0), wall(V3), above(V3,V4).
5 ~ movenorth(V4) :- at(V0,V1,V2), wall(V0), wall(V3), above(V3,V4).
5 ~ movenorth(V0) :- at(V0,V1,V2), wall(V0), wall(V3), above(V4,V0).
5 ~ movenorth(V3) :- at(V0,V1,V2), wall(V0), wall(V3), above(V4,V0).
5 ~ movenorth(V4) :- at(V0,V1,V2), wall(V0), wall(V3), above(V4,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), wall(V0), wall(V3), above(V4,V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), wall(V0), wall(V3), above(V4,V3).
5 ~ movenorth(V4) :- at(V0,V1,V2), wall(V0), wall(V3), above(V4,V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), wall(V3), wall(V4), above(V0,V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), wall(V3), wall(V4), above(V0,V3).
5 ~ movenorth(V4) :- at(V0,V1,V2), wall(V3), wall(V4), above(V0,V3).
5 ~ movenorth(V0) :- at(V0,V1,V2), wall(V3), wall(V4), above(V0,V4).
5 ~ movenorth(V3) :- at(V0,V1,V2), wall(V3), wall(V4), above(V0,V4).
5 ~ movenorth(V4) :- at(V0,V1,V2), wall(V3), wall(V4), above(V0,V4).
5 ~ movenorth(V0) :- at(V0,V1,V2), wall(V3), wall(V4), above(V3,V0).
5 ~ movenorth(V3) :- at(V0,V1,V2), wall(V3), wall(V4), above(V3,V0).
5 ~ movenorth(V4) :- at(V0,V1,V2), wall(V3), wall(V4), above(V3,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), wall(V3), wall(V4), above(V3,V4).
5 ~ movenorth(V3) :- at(V0,V1,V2), wall(V3), wall(V4), above(V3,V4).
5 ~ movenorth(V4) :- at(V0,V1,V2), wall(V3), wall(V4), above(V3,V4).
5 ~ movenorth(V0) :- at(V0,V1,V2), wall(V3), wall(V4), above(V4,V0).
5 ~ movenorth(V3) :- at(V0,V1,V2), wall(V3), wall(V4), above(V4,V0).
5 ~ movenorth(V4) :- at(V0,V1,V2), wall(V3), wall(V4), above(V4,V0).
5 ~ movenorth(V0) :- at(V0,V1,V2), wall(V3), wall(V4), above(V4,V3).
5 ~ movenorth(V3) :- at(V0,V1,V2), wall(V3), wall(V4), above(V4,V3).
5 ~ movenorth(V4) :- at(V0,V1,V2), wall(V3), wall(V4), above(V4,V3).
