
def get_arg_types(pred:str):

    pred=pred.split("not ")
    clean(pred)
    pred=pred[0]
    move_list = ["north", "south", "east", "west", "northwest", "northeast", "southeast", "southwest"]
    door_actions = ['open_door_N',
                    'open_door_S',
                    'open_door_E',
                    'open_door_W',
                    'open_door_NW',
                    'open_door_NE',
                    'open_door_SW',
                    'open_door_SE',
                    'close_door_N',
                    'close_door_S',
                    'close_door_E',
                    'close_door_W',
                    'close_door_NW',
                    'close_door_NE',
                    'close_door_SW',
                    'close_door_SE', ]
    arg_type_dict = {}
    for door in door_actions:
        arg_type_dict[door] = ['cell']
    for move in move_list:
        arg_type_dict[move] = ['cell']
    arg_type_dict['cell'] = ['cell']
    arg_type_dict['deepwater'] = ['cell']
    arg_type_dict['get'] = ['cell']
    arg_type_dict['at'] = ['cell', 'xcoord', 'ycoord']
    arg_type_dict['agentat'] = ['cell']
    arg_type_dict['wall'] = ['cell']
    arg_type_dict['='] = ['int', 'int', 'operator', 'constant']
    arg_type_dict['throw'] = ['cell', 'xcoord', 'ycoord']
    arg_type_dict['open_door'] = ['cell']
    arg_type_dict['opened_door'] = ['cell']
    arg_type_dict['close_door'] = ['cell']
    arg_type_dict['closed_door'] = ['cell']
    arg_type_dict['drop'] = ['cell']
    arg_type_dict['wait'] = ['cell']
    arg_type_dict['itemtype'] = ['item_type']
    arg_type_dict['inv_item'] = ['item_type', 'quantity']
    arg_type_dict['item'] = ['cell', 'item_type', 'quantity']
    arg_type_dict['inv_id'] = ['id', 'item_type']
    return arg_type_dict[pred]

def clean(list):
    while '' in list:
        list.remove('')
class Term:

    def __init__(self,predicate_str:str,arg_strs:[str]):
        self.pred_str = predicate_str
        self.args = arg_strs
        self.arg_types = None
        self.human_readable_args = []

        self._set_arg_types(predicate_str)

    def get_pred_str(self):
        return self.pred_str

    def _set_arg_types(self, pred:str):
        #print("pred is {}".format(pred))
        self.arg_types = get_arg_types(pred)

    def set_human_readable_args(self, args:[str]):
        #print("args={}".format(args))
        assert len(args) == len(self.arg_types)
        self.human_readable_args = args

    def substitute_args(self,curr:[str],new:[str]):
        new_args = []
        for a in self.human_readable_args:
            new_args.append(curr.index(a))
        self.human_readable_args = new_args

    def __eq__(self, other):
        return self.pred_str == other.pred_str and self.human_readable_args == other.standardized_args

    def __hash__(self):
        return hash(self.__str__())

    def __str__(self):
        upper_args = map(lambda s: s.upper(), self.args)
        if self.pred_str == '=':
            return '{} = {} {} {}'.format(*upper_args)
        else:
            s = '{}('.format(self.pred_str)
            for a in upper_args:
                s += '{},'.format(a)
            s = s[0:-1] + ')'
            return s

    def __repr__(self):
        return self.__str__()