from tqdm import tqdm
from multiprocessing import Pool
import subprocess
import os
import time
import sys

if len(sys.argv) < 5:
    print("Incorrect number of arguments, correct usage:")
    print("python3 run_ilasp_parallel_with_params.py <folder_name> <period> <timeout> <cores>")
    print("<folder_name> contains the input files to ilasp")
    print("<period> is the number of new examples needed per action before calling learning")
    print("<timeout> is the number of seconds before ILASP is cutoff")
    print("<cores> is the number of cores given to multiprocessing.pool")
else:
    DIR_FOR_INPUT_FILES_TO_ILASP = str(sys.argv[1])
    PERIOD = int(str(sys.argv[2]))
    TIMEOUT = int(str(sys.argv[3]))
    CORES = int(str(sys.argv[4]))

    # Get all the files in the directory
    files = sorted(os.listdir(DIR_FOR_INPUT_FILES_TO_ILASP))
    learned_files = []
    las_files = []
    for f in files:
        if f.endswith("learned_rule.txt"):
            learned_files.append(f)
        elif f.endswith(".las"):
            las_files.append(f)
        else:
            print("Error, unrecognized file: {}".format(f))

    avail_files_to_learn_over = []
    for f in las_files:
        need_to_remove = False
        for f_learned in learned_files:
            if f[0:-4] == f_learned[0:-len('~learned_rule.txt')]:
                need_to_remove = True
        if not need_to_remove:
            avail_files_to_learn_over.append(f)
        #else:
        #    print("removed {} from files to learn over".format(f))
            
    print("Identified {} files that have already been learned over, ignoring those".format(len(learned_files)))

    #actions_to_num_examples = {} # key is string of action name (found in the filename), value is number of times we've seen it
    final_files_to_learn_over = avail_files_to_learn_over

    print('About to run ILASP on {} files with timeout of {}s ...'.format(len(final_files_to_learn_over),TIMEOUT))

    pbar = tqdm(total=len(final_files_to_learn_over))

    def run_ilasp_single_file(filename=''):
        #print('Filename is {}'.format(filename))
        results_file_str = ""
        if len(filename) > 0:
            result_output_str = ''
            try:
                completed_process = subprocess.run(["ILASP","--version=2i","--clingo5","-nc","-ml=5","--max-rule-length=5",DIR_FOR_INPUT_FILES_TO_ILASP+filename], stdout=subprocess.PIPE, timeout=TIMEOUT)
                result_output_str = completed_process.stdout.decode()
            except subprocess.TimeoutExpired:
                result_output_str = 'timeout after {} seconds\n'.format(TIMEOUT)

            result_output_filename = DIR_FOR_INPUT_FILES_TO_ILASP + filename[0:-4]+'~learned_rule.txt'
            #if ':-' in result_output_str:
            #    print("Learned a rule!")
            with open(result_output_filename, 'w') as f:
                f.write(result_output_str)

    def wrapMyFunc(arg):
        return arg, run_ilasp_single_file(arg)
            
    def update(arg):
        pbar.update()

    start_time = time.time()            
    pool = Pool(processes = CORES) 
    for f_i in final_files_to_learn_over:
        pool.apply_async(wrapMyFunc, args=(f_i,), callback=update)
    pool.close()
    pool.join()
    pbar.close()
    print("Took {} seconds to run {} files".format(time.time() - start_time, len(files)))
