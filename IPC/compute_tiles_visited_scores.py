import sys

if len(sys.argv) < 2:
    print("Incorrect number of arguments, correct usage:")
    print("python3 compute_tiles_visited_scores.py <expn_output_file> ")
    print("<expn_output_file> contains the input files to be scored")
    sys.exit()
else:
    EXPN_OUTPUT_FILE = str(sys.argv[1]) # contains both .las and _learned_rule.txt files

    if 'expn' not in EXPN_OUTPUT_FILE:
        print("ERROR - expn not in filename (make sure expn and the corresponding number are in the filename)")
        sys.exit()

    expn_file_parts = EXPN_OUTPUT_FILE.split('_')
    i = 0
    expn_num_index = -1
    for part in expn_file_parts:
        if part == 'expn':
            expn_num_index = i+1
        i+=1

    if expn_num_index < 0:
        print("Couldn't find expn number")
        sys.exit()

    SCORES_OUTPUT_FILE = 'tiles_visited_scores_expn_{}.csv'.format(expn_file_parts[expn_num_index])

    header = 'run,num_actions,num_tiles_visited\n'
    curr_run = None
    curr_num_actions = None
    tiles_visited = []
    with open(SCORES_OUTPUT_FILE, 'w+') as output_file:
        output_file.write(header)

        with open(EXPN_OUTPUT_FILE, 'r') as input_file:
            for line in input_file.readlines():
                if  'dcss_ilasp_data' in line and '_run_' in line:
                    run_num = -1
                    i = 0
                    line_parts = line.split('_')
                    for part in line_parts:
                        if part == 'run':
                            run_num = i+1
                        i+=1
                    curr_run = line_parts[run_num]
                    print("Now at run {}".format(curr_run))

                if '#A' in line and 'iter_time' in line:
                    curr_num_actions = line[2:8].strip()
                    line_vbar_parts = line.split('|')
                    for part in line_vbar_parts:
                        if 'agentat(' in part:
                            if part not in tiles_visited:
                                tiles_visited.append(part)

                if curr_run and curr_num_actions:
                    output_file.write('{0},{1},{2}\n'.format(curr_run,curr_num_actions,len(tiles_visited)))
