'''
This file stores the gamestate class that is used to keep track of
the current state of the dcss game 
'''

import logging
import re
from term import get_arg_types, clean, Term, add_to_arg_types
import hashlib
from threads import BaseThread
import time
from copy import deepcopy


class GameState():
    ID = 0

    def __init__(self, domprob):
        self.domprob = domprob
        # state is just a dictionary of key value pairs
        self.state = []
        self.obj_mapping = {}
        self.obj_types = {}
        for k, v in domprob.problem.objects.items():
            self.obj_types[k] = v
            for val in v:
                self.obj_mapping[val] = k

        for atom in domprob.problem.init:
            self.state.append(Term(str(atom)))

        # only state information we care about
        self.state_keys = []

        self.letter_to_number_lookup = []

        self.last_recorded_movement = ''
        self.inertia_str = ''
        self.generate_inertia_str()
        self.asp_str = ''  # facts that don't change when an action is executed
        self.get_asp_str()

        self.state_hash_already_computed = False
        self.state_hash = None
        self.compute_state_hash() # this overwrites self.state_hash
        self.old_asp_str = ''
        self.asp_comment_str = ''  # comments associated with asp
        self.training_asp_str = ''  # facts that do change
        self.all_asp_cells = None  # number of cell objects
        self.pddl_str = ''  # state to pass to ff/planner
        self.pred_str = ''  # predicate string for ff/planner
        self.obj_str = ''  # object string for ff/planner
        self.messages = {}  # key is turn, value is list of messages received on this turn in order,
        # where first is oldest message

        # intiliaze values of state variables

        self.id = GameState.ID
        GameState.ID += 1

    def get_full_state_str(self):
        full_str = ''
        for term in sorted(self.state, key=str):
            # remove whitespace to help keep these identical
            term_parts = str(term).split()
            term_no_whitespace = "".join(term_parts)
            full_str += term_no_whitespace
            #print(term_no_whitespace)

        return full_str

    def compute_state_hash(self, recompute=False):
        if not recompute and self.state_hash_already_computed:
            return # do nothing

        # make a hash for identifying unique states
        hasher = hashlib.md5()
        hasher.update(self.get_full_state_str().encode('utf-8'))
        self.state_hash = hasher.digest()  # used for identifying unique game states
        self.state_hash_already_computed = True
        #print("Hash is now: {}".format(self.state_hash))

    def get_inertia_str(self):
        return self.inertia_str

    def remove_term_from_state(self, term):
        self.state.remove(term)
        # recalculate hash since state has changed
        self.compute_state_hash(recompute=True)

    def add_term_from_state(self, term):
        self.state.append(term)
        # recalculate hash since state has changed
        self.compute_state_hash(recompute=True)

    def generate_inertia_str(self):
        inertia = '%INERTIA/PREDICATES\n'
        t_str = ", time(T), T<lasttime.\n"
        for pred in self.domprob.domain.predicates:
            predicate = pred.name + '('
            variables = {}
            counter = 0
            for arg in pred.args:
                var = arg.name
                if var not in variables:
                    variables[var] = "V" + str(counter)
                    predicate += variables[var] + ','
                    counter += 1
            predicate_H = predicate + 'T+1)'
            predicate_B = predicate + 'T)'
            n_predicate_B = 'not -' + predicate_H
            inertia += predicate_H + ' :- ' + predicate_B + ', ' + n_predicate_B + t_str
        for type in self.obj_types:
            inertia += (type + '(V0,T+1) :- ' + type + '(V0,T0)' + t_str)
        self.inertia_str = inertia

    def update(self, last_action):

        self.compute_asp_str(last_action)  # TODO:update function
        self.compute_pddl_str()  # TODO:update function
        # self.compute_pred_str() #TODO:update funciton

    def get_pred_str(self):
        return self.pred_str

    def get_obj_str(self):
        return self.obj_str

    def compute_pred_str(self):
        objects = {}
        preds = set()
        for atom in self.asp_str.split(".\n"):
            atom_parts = re.split('\(|\)|,|\n', atom)
            clean(atom_parts)
            if len(atom_parts) == 0:
                continue
            head = atom_parts[0]
            arg_types = get_arg_types(head)
            body = atom_parts[1:]
            for i in range(0, len(body)):
                if arg_types[i] not in objects:
                    objects[arg_types[i]] = set()
                objects[arg_types[i]].add(body[i])
            counter = 0
            for i in range(0, len(arg_types)):
                arg = arg_types[i]
                arg_types[i] = '?' + arg_types[i] + str(counter) + ' '
                counter += 1
                head = head + ' ' + arg_types[i]
            preds.add(head)
        pred_str = '(:predicates\n'
        object_str = '(:objects\n'
        spaces = ' ' * 4 * 1
        for obj in objects:
            object_str += spaces
            for o in objects[obj]:
                try:
                    a = int(o)
                    o = obj[0] + o
                except:
                    pass
                object_str += o + " "
            object_str += "- " + obj + '\n'
        object_str += ')'
        for pred in preds:
            pred_str += spaces + "("
            for p in pred.split(' '):
                pred_str += p + ' '
            pred_str = pred_str[:-1] + ')\n'
        pred_str += ')'
        self.pred_str = pred_str
        self.obj_str = object_str

    def get_pddl_str(self):
        return self.pddl_str

    def compute_pddl_str(self):
        pddl_str = '(:init\n'
        spaces = ' ' * 4 * 1
        for atom in self.asp_str.split(".\n"):
            atom_parts = re.split('\(|\)|,|\n', atom)
            clean(atom_parts)
            if len(atom_parts) <= 1:
                continue
            head = atom_parts[0]
            arg_types = get_arg_types(head)
            pddl_str += spaces + '(' + head + ' '
            for i in range(1, len(atom_parts)):
                part = atom_parts[i]
                try:
                    a = int(part)
                    part = arg_types[i - 1][0] + part
                except:
                    pass
                pddl_str += part + " "
            pddl_str = pddl_str[:-1] + ')\n'
        pddl_str += ')'
        self.pddl_str = pddl_str

    def process_messages(self, data):

        # begin: this is just for html stripping
        from html.parser import HTMLParser
        class MLStripper(HTMLParser):
            def __init__(self):
                self.reset()
                self.strict = False
                self.convert_charrefs = True
                self.fed = []

            def handle_data(self, d):
                self.fed.append(d)

            def get_data(self):
                return ''.join(self.fed)

        def strip_tags(html):
            s = MLStripper()
            s.feed(html)
            return s.get_data()

        # end: html stripping code

        # need to store the message for current location so I can get quanitity of food items and stones for pickup action
        for m in data:
            turn = m['turn']
            message_only = strip_tags(m['text'])
            if turn in self.messages.keys():
                self.messages[turn].append(message_only)
            else:
                self.messages[turn] = [message_only]

            # print("Just added message for turn {}: {}".format(turn,message_only))

    def process_inv(self, data):
        # print("Data is {}".format(data))
        keys = data.keys()
        remove_ids = []
        self.inventory = {}
        for inv_id in keys:
            if 'name' in data[inv_id]:
                self.inventory[inv_id] = [data[inv_id]['name'], data[inv_id]['quantity']]

    def get_x_y_g_cell_data(self, cells):
        only_xyg_cell_data = []
        curr_x = None
        curr_y = None
        num_at_signs = 0
        if cells:
            for i_dict in cells:
                # if (curr_x and ('x' in i_dict.keys()) or (not ('y' in i_dict.keys()) and curr_y == -1):
                #    raise Exception("ERROR: yeah I must be wrong")
                # print("i_dict is ",str(i_dict))
                if 'x' in i_dict.keys() and 'y' in i_dict.keys() and 'g' in i_dict.keys():
                    curr_x = i_dict['x']
                    curr_y = i_dict['y']
                    only_xyg_cell_data.append([i_dict['x'], i_dict['y'], i_dict['g']])
                    # print("x={},y={},g={}".format(str(curr_x),str(curr_y),str(i_dict['g'])))
                elif 'x' in i_dict.keys() and 'y' in i_dict.keys():
                    ''' Sometimes there is only x and y and no g, often at the beginning of the cells list'''
                    curr_x = i_dict['x']
                    curr_y = i_dict['y']
                    # print("x={},y={}".format(str(curr_x), str(curr_y)))
                elif 'g' in i_dict.keys() and len(i_dict['g']) > 0:
                    # print("x,y,g = ", str(curr_x), str(curr_y), str(i_dict['g']))
                    try:
                        curr_x += 1
                        only_xyg_cell_data.append([curr_x, curr_y, i_dict['g']])
                        # print("added it just fine")
                        if '@' in str(i_dict['g']):
                            num_at_signs += 1
                            # print("Just added ({0},{1},{2}) to only_xyg_cell_data".format(curr_x,curr_y,i_dict['g']))
                            if num_at_signs > 1:
                                print("Whoa, too many @ signs, here's the cell data")
                                print(cells)
                        # print("x={},y={},g={}".format(str(curr_x), str(curr_y), str(i_dict['g'])))
                    except:
                        # TODO: test this more robustly
                        #        right now I think that if this triggers, it means
                        #        the player didn't move, so just keep old data and don't
                        #        update

                        logging.warning("Failure with cell data: " + str(i_dict))
                        print("curr_x={0} and curr_y={1}".format(curr_x, curr_y))
                        print("Cells are " + str(cells))
                        input("Press enter to continue")
                        pass

                # else:
                #    raise Exception("ERROR: no \'g\' found in cell data")
            # for i in only_xyg_cell_data:
            #    print(str(i))

            return only_xyg_cell_data

    def _generate_asp_str(self):
        asp_str = ''
        for t in self.state:
            asp_str += str(t) + ".\n"
        for obj, type in self.obj_mapping.items():
            asp_str += str(type) + "(" + obj + ").\n"
        asp_str = asp_str.lower()
        self.asp_str = asp_str

    def get_asp_str(self):
        self._generate_asp_str()
        return self.asp_str

    def get_asp_comment_str(self):
        return self.asp_comment_str

    def get_training_asp_str(self):
        return self.training_asp_str

    def get_player_cell(self):
        return self.player_cell

    def compute_asp_str(self, last_action, filename=None):
        num_asp_cells = []
        asp_comment_str = '%'
        if filename:
            pass  # TODO write to file

        bg_asp_str = ''
        training_asp_str = ''

        # go through all the map data and add cells, xy coordinates, and any special objects
        cell_id = 1
        y = 0
        FOUND_PLAYER = False
        player_cell = None

        for row in self.map_obj:
            x = 0
            for cell in row:
                if cell == ' ' or cell == '':
                    # ignore
                    asp_comment_str += "|      |".format(cell_id)
                    pass

                else:

                    bg_asp_str += 'cell(c{0}).\n'.format(cell_id)
                    bg_asp_str += 'at(c{0},{1},{2}).\n'.format(cell_id, x, y)
                    num_asp_cells.append('c{0}'.format(cell_id))

                    if cell == '@':
                        bg_asp_str += 'agentat(c{0}).\n'.format(cell_id)
                        asp_comment_str += "| c{0:0=3d} |".format(cell_id)
                        FOUND_PLAYER = True
                        format_cell = 'c{0}'.format(cell_id)
                        under_player = (self._under_player(format_cell, last_action))
                        for line in under_player:
                            bg_asp_str += line + '\n'
                        player_cell = 'c{0}'.format(cell_id)
                    if cell == '#':
                        bg_asp_str += 'wall(c{0}).\n'.format(cell_id)
                        asp_comment_str += "|#c{0:0=3d}#|".format(cell_id)
                    elif cell == '.':
                        # do nothing
                        asp_comment_str += "| c{0:0=3d} |".format(cell_id)
                    elif cell == '$':
                        bg_asp_str += 'gold(c{0}).\n'.format(cell_id)
                        asp_comment_str += "|$c{0:0=3d}$|".format(cell_id)
                    elif cell == '(':
                        bg_asp_str += 'item(c{0},javelin,1).\n'.format(
                            cell_id)  # TODO: FIND A WAY TO DETERMINE WHAT THE ITEM IS.....
                        asp_comment_str += "|(c{0:0=3d}(|".format(cell_id)
                    elif cell == '≈':
                        bg_asp_str += 'deepwater(c{0}).\n'.format(cell_id)
                        asp_comment_str += "|~c{0:0=3d}~|".format(cell_id)
                    elif cell == '%':
                        bg_asp_str += 'food(c{0}).\n'.format(cell_id)
                        asp_comment_str += "|%c{0:0=3d}%|".format(cell_id)
                    elif cell == '0':
                        # orb
                        asp_comment_str += "| c{0:0=3d} |".format(cell_id)
                    elif cell == '<':
                        # exit
                        asp_comment_str += "| c{0:0=3d} |".format(cell_id)
                    elif cell == '+':
                        # door
                        bg_asp_str += 'closed_door(c{0}).\n'.format(cell_id)
                        asp_comment_str += "| c{0:0=3d} |".format(cell_id)
                    elif cell == "'":
                        # door
                        bg_asp_str += 'opened_door(c{0}).\n'.format(cell_id)
                        asp_comment_str += "| c{0:0=3d} |".format(cell_id)
                cell_id += 1
                x += 1
            asp_comment_str += '\n%' + '+------+' * len(row) + '\n%'
            y += 1

        # objects agent knows about
        # TODO Put this somewhere else more global - probably ought to consider pddl to asp in a nicer implementation of
        # TODO all this
        agent_known_object_types = ['javelin', 'orbofzot']
        for obj_type in agent_known_object_types:
            bg_asp_str += 'itemtype({}).\n'.format(obj_type)

        # add inventory facts
        object_types = []
        for i in self.inventory.keys():
            name = self.inventory[i][0]
            name = ''.join([i for i in name if not i.isdigit() and not i in ['+', '-']])
            name = name.strip().replace(' ', '')

            # depluralize
            name = name.replace('potions', 'potion').replace('scrolls', 'scroll').replace('stones', 'stone').replace(
                'rations', 'ration')
            quantity = self.inventory[i][1]

            if name not in object_types:
                object_types.append(name)
                bg_asp_str += 'itemtype({}).\n'.format(name)

            bg_asp_str += 'inv_item({0},{1}).\n'.format(name, quantity)
            bg_asp_str += 'inv_id({0},{1}).\n'.format(i, name)

        # add facts about current tile

        # if len(self.messages.keys()) > 0:
        #     max_turn = max(self.messages.keys())
        #     logging.debug("max_turn is {}".format(max_turn))
        #     for m in self.messages[max_turn]:
        #         logging.debug("a message on turn {} is {}".format(max_turn,m))
        #         if 'You see here a ' in m:
        #             for obj_type in agent_known_object_types:
        #                 if obj_type in m:
        #                     bg_asp_str += 'item({},{},{}).\n'.format(player_cell, obj_type, 1)
        #
        #         elif 'You see here ' in m:
        #             nums_in_m = list(map(int, re.findall(r'\d+', m)))
        #             print("nums_in_m is {}".format(nums_in_m))
        #             if len(nums_in_m) > 0:
        #                 quantity = nums_in_m[0]
        #                 for obj_type in agent_known_object_types:
        #                     if obj_type in m:
        #                         bg_asp_str += 'item({},{},{}).\n'.format(player_cell, obj_type, quantity)
        #         else:
        #             pass # just ignore

        self.asp_comment_str = '%Map Picture:\n' + asp_comment_str + '\n'
        self.player_cell = player_cell
        bg_lines = set(bg_asp_str.split('\n'))
        bg_asp_str = ''
        for line in bg_lines:
            bg_asp_str += line + '\n'
        self.asp_str = bg_asp_str
        self.old_asp_str = self.asp_str
        if FOUND_PLAYER:
            self.training_asp_str = training_asp_str

        self.all_asp_cells = set(num_asp_cells)

        # print(self.asp_str)
