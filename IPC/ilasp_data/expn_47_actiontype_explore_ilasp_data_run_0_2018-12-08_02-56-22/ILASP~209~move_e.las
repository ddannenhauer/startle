
#modeh(move_e(var(xcoord),var(ycoord))).

#pos({move_e(x1,y1)}, {},{west(x3, x2).
west(x2, x1).
wall(x6, y2).
wall(x6, y1).
wall(x3, y2).
wall(x5, y4).
north(y4, y3).
wall(x4, y2).
north(y5, y4).
wall(x3, y4).
north(y3, y2).
wall(x6, y3).
wall(x6, y4).
wall(x2, y2).
wall(x4, y4).
west(x6, x5).
west(x8, x7).
west(x9, x8).
west(x4, x3).
north(y2, y1).
west(x7, x6).
wall(x2, y4).
cdoor(x8, y4).
west(x5, x4).
wall(x1, y2).
agentat(x2,y1).
xcoord(x6).
ycoord(y4).
ycoord(y1).
xcoord(x3).
xcoord(x8).
xcoord(x9).
ycoord(y2).
xcoord(x7).
xcoord(x5).
xcoord(x4).
ycoord(y3).
xcoord(x2).
ycoord(y5).
xcoord(x1).
}).

#pos({}, {move_e(x8,y4)},{west(x3, x2).
west(x2, x1).
wall(x6, y2).
wall(x6, y1).
wall(x3, y2).
wall(x5, y4).
north(y4, y3).
wall(x4, y2).
north(y5, y4).
wall(x3, y4).
north(y3, y2).
wall(x6, y3).
wall(x6, y4).
wall(x2, y2).
wall(x4, y4).
west(x6, x5).
west(x8, x7).
agentat(x1, y1).
west(x9, x8).
west(x4, x3).
north(y2, y1).
west(x7, x6).
wall(x2, y4).
cdoor(x8, y4).
west(x5, x4).
wall(x1, y2).
xcoord(x6).
ycoord(y4).
ycoord(y1).
xcoord(x3).
xcoord(x8).
xcoord(x9).
ycoord(y2).
xcoord(x7).
xcoord(x5).
xcoord(x4).
ycoord(y3).
xcoord(x2).
ycoord(y5).
xcoord(x1).
}).

#pos({}, {move_e(x3,y4)},{west(x3, x2).
west(x2, x1).
wall(x6, y2).
wall(x6, y1).
wall(x3, y2).
wall(x5, y4).
north(y4, y3).
wall(x4, y2).
north(y5, y4).
wall(x3, y4).
north(y3, y2).
wall(x6, y3).
wall(x6, y4).
wall(x2, y2).
wall(x4, y4).
west(x6, x5).
west(x8, x7).
agentat(x1, y1).
west(x9, x8).
west(x4, x3).
north(y2, y1).
west(x7, x6).
wall(x2, y4).
cdoor(x8, y4).
west(x5, x4).
wall(x1, y2).
xcoord(x6).
ycoord(y4).
ycoord(y1).
xcoord(x3).
xcoord(x8).
xcoord(x9).
ycoord(y2).
xcoord(x7).
xcoord(x5).
xcoord(x4).
ycoord(y3).
xcoord(x2).
ycoord(y5).
xcoord(x1).
}).

#pos({}, {move_e(x1,y1)},{west(x3, x2).
west(x2, x1).
wall(x6, y2).
wall(x6, y1).
wall(x3, y2).
wall(x5, y4).
north(y4, y3).
wall(x4, y2).
north(y5, y4).
wall(x3, y4).
north(y3, y2).
wall(x6, y3).
wall(x6, y4).
wall(x2, y2).
wall(x4, y4).
west(x6, x5).
west(x8, x7).
agentat(x1, y1).
west(x9, x8).
west(x4, x3).
north(y2, y1).
west(x7, x6).
wall(x2, y4).
cdoor(x8, y4).
west(x5, x4).
wall(x1, y2).
xcoord(x6).
ycoord(y4).
ycoord(y1).
xcoord(x3).
xcoord(x8).
xcoord(x9).
ycoord(y2).
xcoord(x7).
xcoord(x5).
xcoord(x4).
ycoord(y3).
xcoord(x2).
ycoord(y5).
xcoord(x1).
}).

#pos({}, {move_e(x1,y2)},{west(x3, x2).
west(x2, x1).
wall(x6, y2).
wall(x6, y1).
wall(x3, y2).
wall(x5, y4).
north(y4, y3).
wall(x4, y2).
north(y5, y4).
wall(x3, y4).
north(y3, y2).
wall(x6, y3).
wall(x6, y4).
wall(x2, y2).
wall(x4, y4).
west(x6, x5).
west(x8, x7).
agentat(x1, y1).
west(x9, x8).
west(x4, x3).
north(y2, y1).
west(x7, x6).
wall(x2, y4).
cdoor(x8, y4).
west(x5, x4).
wall(x1, y2).
xcoord(x6).
ycoord(y4).
ycoord(y1).
xcoord(x3).
xcoord(x8).
xcoord(x9).
ycoord(y2).
xcoord(x7).
xcoord(x5).
xcoord(x4).
ycoord(y3).
xcoord(x2).
ycoord(y5).
xcoord(x1).
}).

#pos({}, {move_e(x6,y4)},{west(x3, x2).
west(x2, x1).
wall(x6, y2).
wall(x6, y1).
wall(x3, y2).
wall(x5, y4).
north(y4, y3).
wall(x4, y2).
north(y5, y4).
wall(x3, y4).
north(y3, y2).
wall(x6, y3).
wall(x6, y4).
wall(x2, y2).
wall(x4, y4).
west(x6, x5).
west(x8, x7).
agentat(x1, y1).
west(x9, x8).
west(x4, x3).
north(y2, y1).
west(x7, x6).
wall(x2, y4).
cdoor(x8, y4).
west(x5, x4).
wall(x1, y2).
xcoord(x6).
ycoord(y4).
ycoord(y1).
xcoord(x3).
xcoord(x8).
xcoord(x9).
ycoord(y2).
xcoord(x7).
xcoord(x5).
xcoord(x4).
ycoord(y3).
xcoord(x2).
ycoord(y5).
xcoord(x1).
}).

#pos({}, {move_e(x2,y1)},{west(x3, x2).
west(x2, x1).
wall(x6, y2).
wall(x6, y1).
wall(x3, y2).
wall(x5, y4).
north(y4, y3).
wall(x4, y2).
north(y5, y4).
wall(x3, y4).
north(y3, y2).
wall(x6, y3).
wall(x6, y4).
wall(x2, y2).
wall(x4, y4).
west(x6, x5).
west(x8, x7).
agentat(x1, y1).
west(x9, x8).
west(x4, x3).
north(y2, y1).
west(x7, x6).
wall(x2, y4).
cdoor(x8, y4).
west(x5, x4).
wall(x1, y2).
xcoord(x6).
ycoord(y4).
ycoord(y1).
xcoord(x3).
xcoord(x8).
xcoord(x9).
ycoord(y2).
xcoord(x7).
xcoord(x5).
xcoord(x4).
ycoord(y3).
xcoord(x2).
ycoord(y5).
xcoord(x1).
}).

#pos({}, {move_e(x2,y1)},{west(x3, x2).
west(x2, x1).
wall(x6, y2).
wall(x6, y1).
wall(x3, y2).
wall(x5, y4).
north(y4, y3).
wall(x4, y2).
north(y5, y4).
wall(x3, y4).
north(y3, y2).
wall(x6, y3).
wall(x6, y4).
wall(x2, y2).
wall(x4, y4).
west(x6, x5).
west(x8, x7).
west(x9, x8).
west(x4, x3).
north(y2, y1).
west(x7, x6).
wall(x2, y4).
cdoor(x8, y4).
west(x5, x4).
wall(x1, y2).
agentat(x2,y1).
xcoord(x6).
ycoord(y4).
ycoord(y1).
xcoord(x3).
xcoord(x8).
xcoord(x9).
ycoord(y2).
xcoord(x7).
xcoord(x5).
xcoord(x4).
ycoord(y3).
xcoord(x2).
ycoord(y5).
xcoord(x1).
}).

#pos({}, {move_e(x7,y4)},{west(x3, x2).
west(x2, x1).
wall(x6, y2).
wall(x6, y1).
wall(x3, y2).
wall(x5, y4).
north(y4, y3).
wall(x4, y2).
north(y5, y4).
wall(x3, y4).
north(y3, y2).
wall(x6, y3).
wall(x6, y4).
wall(x2, y2).
wall(x4, y4).
west(x6, x5).
west(x8, x7).
west(x9, x8).
west(x4, x3).
north(y2, y1).
west(x7, x6).
wall(x2, y4).
cdoor(x8, y4).
west(x5, x4).
wall(x1, y2).
agentat(x2,y1).
xcoord(x6).
ycoord(y4).
ycoord(y1).
xcoord(x3).
xcoord(x8).
xcoord(x9).
ycoord(y2).
xcoord(x7).
xcoord(x5).
xcoord(x4).
ycoord(y3).
xcoord(x2).
ycoord(y5).
xcoord(x1).
}).

#modeb(1,cdoor(var(xcoord),var(ycoord))).
#modeb(1,wall(var(xcoord),var(ycoord))).
#modeb(1,agentat(var(xcoord),var(ycoord))).
#modeb(1,west(var(xcoord),var(xcoord))).
#modeb(1,ycoord(var(ycoord))).
#modeb(1,odoor(var(xcoord),var(ycoord))).
#modeb(1,xcoord(var(xcoord))).
#modeb(1,north(var(ycoord),var(ycoord))).
#maxv(6).
