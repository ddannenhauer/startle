
#modeh(move_w(var(xcoord),var(ycoord))).

#pos({move_w(x2,y1)}, {},{west(x5, x4).
west(x3, x2).
agentat(x1, y1).
wall(x1, y2).
wall(x5, y4).
north(y2, y1).
north(y5, y4).
wall(x6, y1).
wall(x2, y2).
wall(x4, y2).
west(x4, x3).
north(y4, y3).
north(y3, y2).
wall(x3, y4).
wall(x3, y2).
cdoor(x8, y4).
wall(x6, y3).
west(x8, x7).
wall(x6, y2).
west(x6, x5).
west(x7, x6).
wall(x6, y4).
west(x9, x8).
west(x2, x1).
wall(x2, y4).
wall(x4, y4).
ycoord(y3).
ycoord(y1).
ycoord(y4).
xcoord(x5).
ycoord(y2).
xcoord(x7).
xcoord(x8).
xcoord(x6).
xcoord(x1).
xcoord(x9).
xcoord(x2).
xcoord(x4).
xcoord(x3).
ycoord(y5).
}).

#pos({}, {move_w(x9,y4)},{west(x5, x4).
west(x3, x2).
agentat(x1, y1).
wall(x1, y2).
wall(x5, y4).
north(y2, y1).
north(y5, y4).
wall(x6, y1).
wall(x2, y2).
wall(x4, y2).
west(x4, x3).
north(y4, y3).
north(y3, y2).
wall(x3, y4).
wall(x3, y2).
cdoor(x8, y4).
wall(x6, y3).
west(x8, x7).
wall(x6, y2).
west(x6, x5).
west(x7, x6).
wall(x6, y4).
west(x9, x8).
west(x2, x1).
wall(x2, y4).
wall(x4, y4).
ycoord(y3).
ycoord(y1).
ycoord(y4).
xcoord(x5).
ycoord(y2).
xcoord(x7).
xcoord(x8).
xcoord(x6).
xcoord(x1).
xcoord(x9).
xcoord(x2).
xcoord(x4).
xcoord(x3).
ycoord(y5).
}).

#pos({}, {move_w(x5,y3)},{west(x5, x4).
west(x3, x2).
agentat(x1, y1).
wall(x1, y2).
wall(x5, y4).
north(y2, y1).
north(y5, y4).
wall(x6, y1).
wall(x2, y2).
wall(x4, y2).
west(x4, x3).
north(y4, y3).
north(y3, y2).
wall(x3, y4).
wall(x3, y2).
cdoor(x8, y4).
wall(x6, y3).
west(x8, x7).
wall(x6, y2).
west(x6, x5).
west(x7, x6).
wall(x6, y4).
west(x9, x8).
west(x2, x1).
wall(x2, y4).
wall(x4, y4).
ycoord(y3).
ycoord(y1).
ycoord(y4).
xcoord(x5).
ycoord(y2).
xcoord(x7).
xcoord(x8).
xcoord(x6).
xcoord(x1).
xcoord(x9).
xcoord(x2).
xcoord(x4).
xcoord(x3).
ycoord(y5).
}).

#pos({}, {move_w(x6,y5)},{west(x5, x4).
west(x3, x2).
agentat(x1, y1).
wall(x1, y2).
wall(x5, y4).
north(y2, y1).
north(y5, y4).
wall(x6, y1).
wall(x2, y2).
wall(x4, y2).
west(x4, x3).
north(y4, y3).
north(y3, y2).
wall(x3, y4).
wall(x3, y2).
cdoor(x8, y4).
wall(x6, y3).
west(x8, x7).
wall(x6, y2).
west(x6, x5).
west(x7, x6).
wall(x6, y4).
west(x9, x8).
west(x2, x1).
wall(x2, y4).
wall(x4, y4).
ycoord(y3).
ycoord(y1).
ycoord(y4).
xcoord(x5).
ycoord(y2).
xcoord(x7).
xcoord(x8).
xcoord(x6).
xcoord(x1).
xcoord(x9).
xcoord(x2).
xcoord(x4).
xcoord(x3).
ycoord(y5).
}).

#pos({}, {move_w(x3,y5)},{west(x5, x4).
west(x3, x2).
agentat(x1, y1).
wall(x1, y2).
wall(x5, y4).
north(y2, y1).
north(y5, y4).
wall(x6, y1).
wall(x2, y2).
wall(x4, y2).
west(x4, x3).
north(y4, y3).
north(y3, y2).
wall(x3, y4).
wall(x3, y2).
cdoor(x8, y4).
wall(x6, y3).
west(x8, x7).
wall(x6, y2).
west(x6, x5).
west(x7, x6).
wall(x6, y4).
west(x9, x8).
west(x2, x1).
wall(x2, y4).
wall(x4, y4).
ycoord(y3).
ycoord(y1).
ycoord(y4).
xcoord(x5).
ycoord(y2).
xcoord(x7).
xcoord(x8).
xcoord(x6).
xcoord(x1).
xcoord(x9).
xcoord(x2).
xcoord(x4).
xcoord(x3).
ycoord(y5).
}).

#pos({}, {move_w(x9,y3)},{west(x5, x4).
west(x3, x2).
agentat(x1, y1).
wall(x1, y2).
wall(x5, y4).
north(y2, y1).
north(y5, y4).
wall(x6, y1).
wall(x2, y2).
wall(x4, y2).
west(x4, x3).
north(y4, y3).
north(y3, y2).
wall(x3, y4).
wall(x3, y2).
cdoor(x8, y4).
wall(x6, y3).
west(x8, x7).
wall(x6, y2).
west(x6, x5).
west(x7, x6).
wall(x6, y4).
west(x9, x8).
west(x2, x1).
wall(x2, y4).
wall(x4, y4).
ycoord(y3).
ycoord(y1).
ycoord(y4).
xcoord(x5).
ycoord(y2).
xcoord(x7).
xcoord(x8).
xcoord(x6).
xcoord(x1).
xcoord(x9).
xcoord(x2).
xcoord(x4).
xcoord(x3).
ycoord(y5).
}).

#pos({}, {move_w(x9,y2)},{west(x5, x4).
west(x3, x2).
agentat(x1, y1).
wall(x1, y2).
wall(x5, y4).
north(y2, y1).
north(y5, y4).
wall(x6, y1).
wall(x2, y2).
wall(x4, y2).
west(x4, x3).
north(y4, y3).
north(y3, y2).
wall(x3, y4).
wall(x3, y2).
cdoor(x8, y4).
wall(x6, y3).
west(x8, x7).
wall(x6, y2).
west(x6, x5).
west(x7, x6).
wall(x6, y4).
west(x9, x8).
west(x2, x1).
wall(x2, y4).
wall(x4, y4).
ycoord(y3).
ycoord(y1).
ycoord(y4).
xcoord(x5).
ycoord(y2).
xcoord(x7).
xcoord(x8).
xcoord(x6).
xcoord(x1).
xcoord(x9).
xcoord(x2).
xcoord(x4).
xcoord(x3).
ycoord(y5).
}).

#pos({}, {move_w(x1,y5)},{west(x5, x4).
west(x3, x2).
agentat(x1, y1).
wall(x1, y2).
wall(x5, y4).
north(y2, y1).
north(y5, y4).
wall(x6, y1).
wall(x2, y2).
wall(x4, y2).
west(x4, x3).
north(y4, y3).
north(y3, y2).
wall(x3, y4).
wall(x3, y2).
cdoor(x8, y4).
wall(x6, y3).
west(x8, x7).
wall(x6, y2).
west(x6, x5).
west(x7, x6).
wall(x6, y4).
west(x9, x8).
west(x2, x1).
wall(x2, y4).
wall(x4, y4).
ycoord(y3).
ycoord(y1).
ycoord(y4).
xcoord(x5).
ycoord(y2).
xcoord(x7).
xcoord(x8).
xcoord(x6).
xcoord(x1).
xcoord(x9).
xcoord(x2).
xcoord(x4).
xcoord(x3).
ycoord(y5).
}).

#pos({}, {move_w(x6,y3)},{west(x5, x4).
west(x3, x2).
agentat(x1, y1).
wall(x1, y2).
wall(x5, y4).
north(y2, y1).
north(y5, y4).
wall(x6, y1).
wall(x2, y2).
wall(x4, y2).
west(x4, x3).
north(y4, y3).
north(y3, y2).
wall(x3, y4).
wall(x3, y2).
cdoor(x8, y4).
wall(x6, y3).
west(x8, x7).
wall(x6, y2).
west(x6, x5).
west(x7, x6).
wall(x6, y4).
west(x9, x8).
west(x2, x1).
wall(x2, y4).
wall(x4, y4).
ycoord(y3).
ycoord(y1).
ycoord(y4).
xcoord(x5).
ycoord(y2).
xcoord(x7).
xcoord(x8).
xcoord(x6).
xcoord(x1).
xcoord(x9).
xcoord(x2).
xcoord(x4).
xcoord(x3).
ycoord(y5).
}).

#pos({}, {move_w(x3,y1)},{west(x5, x4).
west(x3, x2).
agentat(x1, y1).
wall(x1, y2).
wall(x5, y4).
north(y2, y1).
north(y5, y4).
wall(x6, y1).
wall(x2, y2).
wall(x4, y2).
west(x4, x3).
north(y4, y3).
north(y3, y2).
wall(x3, y4).
wall(x3, y2).
cdoor(x8, y4).
wall(x6, y3).
west(x8, x7).
wall(x6, y2).
west(x6, x5).
west(x7, x6).
wall(x6, y4).
west(x9, x8).
west(x2, x1).
wall(x2, y4).
wall(x4, y4).
ycoord(y3).
ycoord(y1).
ycoord(y4).
xcoord(x5).
ycoord(y2).
xcoord(x7).
xcoord(x8).
xcoord(x6).
xcoord(x1).
xcoord(x9).
xcoord(x2).
xcoord(x4).
xcoord(x3).
ycoord(y5).
}).

#pos({}, {move_w(x7,y1)},{west(x5, x4).
west(x3, x2).
agentat(x1, y1).
wall(x1, y2).
wall(x5, y4).
north(y2, y1).
north(y5, y4).
wall(x6, y1).
wall(x2, y2).
wall(x4, y2).
west(x4, x3).
north(y4, y3).
north(y3, y2).
wall(x3, y4).
wall(x3, y2).
cdoor(x8, y4).
wall(x6, y3).
west(x8, x7).
wall(x6, y2).
west(x6, x5).
west(x7, x6).
wall(x6, y4).
west(x9, x8).
west(x2, x1).
wall(x2, y4).
wall(x4, y4).
ycoord(y3).
ycoord(y1).
ycoord(y4).
xcoord(x5).
ycoord(y2).
xcoord(x7).
xcoord(x8).
xcoord(x6).
xcoord(x1).
xcoord(x9).
xcoord(x2).
xcoord(x4).
xcoord(x3).
ycoord(y5).
}).

#pos({}, {move_w(x6,y1)},{west(x5, x4).
west(x3, x2).
agentat(x1, y1).
wall(x1, y2).
wall(x5, y4).
north(y2, y1).
north(y5, y4).
wall(x6, y1).
wall(x2, y2).
wall(x4, y2).
west(x4, x3).
north(y4, y3).
north(y3, y2).
wall(x3, y4).
wall(x3, y2).
cdoor(x8, y4).
wall(x6, y3).
west(x8, x7).
wall(x6, y2).
west(x6, x5).
west(x7, x6).
wall(x6, y4).
west(x9, x8).
west(x2, x1).
wall(x2, y4).
wall(x4, y4).
ycoord(y3).
ycoord(y1).
ycoord(y4).
xcoord(x5).
ycoord(y2).
xcoord(x7).
xcoord(x8).
xcoord(x6).
xcoord(x1).
xcoord(x9).
xcoord(x2).
xcoord(x4).
xcoord(x3).
ycoord(y5).
}).

#pos({}, {move_w(x8,y5)},{west(x5, x4).
west(x3, x2).
agentat(x1, y1).
wall(x1, y2).
wall(x5, y4).
north(y2, y1).
north(y5, y4).
wall(x6, y1).
wall(x2, y2).
wall(x4, y2).
west(x4, x3).
north(y4, y3).
north(y3, y2).
wall(x3, y4).
wall(x3, y2).
cdoor(x8, y4).
wall(x6, y3).
west(x8, x7).
wall(x6, y2).
west(x6, x5).
west(x7, x6).
wall(x6, y4).
west(x9, x8).
west(x2, x1).
wall(x2, y4).
wall(x4, y4).
ycoord(y3).
ycoord(y1).
ycoord(y4).
xcoord(x5).
ycoord(y2).
xcoord(x7).
xcoord(x8).
xcoord(x6).
xcoord(x1).
xcoord(x9).
xcoord(x2).
xcoord(x4).
xcoord(x3).
ycoord(y5).
}).

#pos({}, {move_w(x2,y2)},{west(x5, x4).
west(x3, x2).
agentat(x1, y1).
wall(x1, y2).
wall(x5, y4).
north(y2, y1).
north(y5, y4).
wall(x6, y1).
wall(x2, y2).
wall(x4, y2).
west(x4, x3).
north(y4, y3).
north(y3, y2).
wall(x3, y4).
wall(x3, y2).
cdoor(x8, y4).
wall(x6, y3).
west(x8, x7).
wall(x6, y2).
west(x6, x5).
west(x7, x6).
wall(x6, y4).
west(x9, x8).
west(x2, x1).
wall(x2, y4).
wall(x4, y4).
ycoord(y3).
ycoord(y1).
ycoord(y4).
xcoord(x5).
ycoord(y2).
xcoord(x7).
xcoord(x8).
xcoord(x6).
xcoord(x1).
xcoord(x9).
xcoord(x2).
xcoord(x4).
xcoord(x3).
ycoord(y5).
}).

#pos({}, {move_w(x9,y5)},{west(x5, x4).
west(x3, x2).
agentat(x1, y1).
wall(x1, y2).
wall(x5, y4).
north(y2, y1).
north(y5, y4).
wall(x6, y1).
wall(x2, y2).
wall(x4, y2).
west(x4, x3).
north(y4, y3).
north(y3, y2).
wall(x3, y4).
wall(x3, y2).
cdoor(x8, y4).
wall(x6, y3).
west(x8, x7).
wall(x6, y2).
west(x6, x5).
west(x7, x6).
wall(x6, y4).
west(x9, x8).
west(x2, x1).
wall(x2, y4).
wall(x4, y4).
ycoord(y3).
ycoord(y1).
ycoord(y4).
xcoord(x5).
ycoord(y2).
xcoord(x7).
xcoord(x8).
xcoord(x6).
xcoord(x1).
xcoord(x9).
xcoord(x2).
xcoord(x4).
xcoord(x3).
ycoord(y5).
}).

#pos({}, {move_w(x7,y2)},{west(x5, x4).
west(x3, x2).
agentat(x1, y1).
wall(x1, y2).
wall(x5, y4).
north(y2, y1).
north(y5, y4).
wall(x6, y1).
wall(x2, y2).
wall(x4, y2).
west(x4, x3).
north(y4, y3).
north(y3, y2).
wall(x3, y4).
wall(x3, y2).
cdoor(x8, y4).
wall(x6, y3).
west(x8, x7).
wall(x6, y2).
west(x6, x5).
west(x7, x6).
wall(x6, y4).
west(x9, x8).
west(x2, x1).
wall(x2, y4).
wall(x4, y4).
ycoord(y3).
ycoord(y1).
ycoord(y4).
xcoord(x5).
ycoord(y2).
xcoord(x7).
xcoord(x8).
xcoord(x6).
xcoord(x1).
xcoord(x9).
xcoord(x2).
xcoord(x4).
xcoord(x3).
ycoord(y5).
}).

#pos({}, {move_w(x3,y3)},{west(x5, x4).
west(x3, x2).
agentat(x1, y1).
wall(x1, y2).
wall(x5, y4).
north(y2, y1).
north(y5, y4).
wall(x6, y1).
wall(x2, y2).
wall(x4, y2).
west(x4, x3).
north(y4, y3).
north(y3, y2).
wall(x3, y4).
wall(x3, y2).
cdoor(x8, y4).
wall(x6, y3).
west(x8, x7).
wall(x6, y2).
west(x6, x5).
west(x7, x6).
wall(x6, y4).
west(x9, x8).
west(x2, x1).
wall(x2, y4).
wall(x4, y4).
ycoord(y3).
ycoord(y1).
ycoord(y4).
xcoord(x5).
ycoord(y2).
xcoord(x7).
xcoord(x8).
xcoord(x6).
xcoord(x1).
xcoord(x9).
xcoord(x2).
xcoord(x4).
xcoord(x3).
ycoord(y5).
}).

#pos({}, {move_w(x7,y5)},{west(x5, x4).
west(x3, x2).
agentat(x1, y1).
wall(x1, y2).
wall(x5, y4).
north(y2, y1).
north(y5, y4).
wall(x6, y1).
wall(x2, y2).
wall(x4, y2).
west(x4, x3).
north(y4, y3).
north(y3, y2).
wall(x3, y4).
wall(x3, y2).
cdoor(x8, y4).
wall(x6, y3).
west(x8, x7).
wall(x6, y2).
west(x6, x5).
west(x7, x6).
wall(x6, y4).
west(x9, x8).
west(x2, x1).
wall(x2, y4).
wall(x4, y4).
ycoord(y3).
ycoord(y1).
ycoord(y4).
xcoord(x5).
ycoord(y2).
xcoord(x7).
xcoord(x8).
xcoord(x6).
xcoord(x1).
xcoord(x9).
xcoord(x2).
xcoord(x4).
xcoord(x3).
ycoord(y5).
}).

#pos({}, {move_w(x7,y3)},{west(x5, x4).
west(x3, x2).
agentat(x1, y1).
wall(x1, y2).
wall(x5, y4).
north(y2, y1).
north(y5, y4).
wall(x6, y1).
wall(x2, y2).
wall(x4, y2).
west(x4, x3).
north(y4, y3).
north(y3, y2).
wall(x3, y4).
wall(x3, y2).
cdoor(x8, y4).
wall(x6, y3).
west(x8, x7).
wall(x6, y2).
west(x6, x5).
west(x7, x6).
wall(x6, y4).
west(x9, x8).
west(x2, x1).
wall(x2, y4).
wall(x4, y4).
ycoord(y3).
ycoord(y1).
ycoord(y4).
xcoord(x5).
ycoord(y2).
xcoord(x7).
xcoord(x8).
xcoord(x6).
xcoord(x1).
xcoord(x9).
xcoord(x2).
xcoord(x4).
xcoord(x3).
ycoord(y5).
}).

#pos({}, {move_w(x8,y1)},{west(x5, x4).
west(x3, x2).
agentat(x1, y1).
wall(x1, y2).
wall(x5, y4).
north(y2, y1).
north(y5, y4).
wall(x6, y1).
wall(x2, y2).
wall(x4, y2).
west(x4, x3).
north(y4, y3).
north(y3, y2).
wall(x3, y4).
wall(x3, y2).
cdoor(x8, y4).
wall(x6, y3).
west(x8, x7).
wall(x6, y2).
west(x6, x5).
west(x7, x6).
wall(x6, y4).
west(x9, x8).
west(x2, x1).
wall(x2, y4).
wall(x4, y4).
ycoord(y3).
ycoord(y1).
ycoord(y4).
xcoord(x5).
ycoord(y2).
xcoord(x7).
xcoord(x8).
xcoord(x6).
xcoord(x1).
xcoord(x9).
xcoord(x2).
xcoord(x4).
xcoord(x3).
ycoord(y5).
}).

#pos({}, {move_w(x3,y2)},{west(x5, x4).
west(x3, x2).
agentat(x1, y1).
wall(x1, y2).
wall(x5, y4).
north(y2, y1).
north(y5, y4).
wall(x6, y1).
wall(x2, y2).
wall(x4, y2).
west(x4, x3).
north(y4, y3).
north(y3, y2).
wall(x3, y4).
wall(x3, y2).
cdoor(x8, y4).
wall(x6, y3).
west(x8, x7).
wall(x6, y2).
west(x6, x5).
west(x7, x6).
wall(x6, y4).
west(x9, x8).
west(x2, x1).
wall(x2, y4).
wall(x4, y4).
ycoord(y3).
ycoord(y1).
ycoord(y4).
xcoord(x5).
ycoord(y2).
xcoord(x7).
xcoord(x8).
xcoord(x6).
xcoord(x1).
xcoord(x9).
xcoord(x2).
xcoord(x4).
xcoord(x3).
ycoord(y5).
}).

#pos({}, {move_w(x9,y1)},{west(x5, x4).
west(x3, x2).
agentat(x1, y1).
wall(x1, y2).
wall(x5, y4).
north(y2, y1).
north(y5, y4).
wall(x6, y1).
wall(x2, y2).
wall(x4, y2).
west(x4, x3).
north(y4, y3).
north(y3, y2).
wall(x3, y4).
wall(x3, y2).
cdoor(x8, y4).
wall(x6, y3).
west(x8, x7).
wall(x6, y2).
west(x6, x5).
west(x7, x6).
wall(x6, y4).
west(x9, x8).
west(x2, x1).
wall(x2, y4).
wall(x4, y4).
ycoord(y3).
ycoord(y1).
ycoord(y4).
xcoord(x5).
ycoord(y2).
xcoord(x7).
xcoord(x8).
xcoord(x6).
xcoord(x1).
xcoord(x9).
xcoord(x2).
xcoord(x4).
xcoord(x3).
ycoord(y5).
}).

#pos({}, {move_w(x8,y2)},{west(x5, x4).
west(x3, x2).
agentat(x1, y1).
wall(x1, y2).
wall(x5, y4).
north(y2, y1).
north(y5, y4).
wall(x6, y1).
wall(x2, y2).
wall(x4, y2).
west(x4, x3).
north(y4, y3).
north(y3, y2).
wall(x3, y4).
wall(x3, y2).
cdoor(x8, y4).
wall(x6, y3).
west(x8, x7).
wall(x6, y2).
west(x6, x5).
west(x7, x6).
wall(x6, y4).
west(x9, x8).
west(x2, x1).
wall(x2, y4).
wall(x4, y4).
ycoord(y3).
ycoord(y1).
ycoord(y4).
xcoord(x5).
ycoord(y2).
xcoord(x7).
xcoord(x8).
xcoord(x6).
xcoord(x1).
xcoord(x9).
xcoord(x2).
xcoord(x4).
xcoord(x3).
ycoord(y5).
}).

#pos({}, {move_w(x2,y5)},{west(x5, x4).
west(x3, x2).
agentat(x1, y1).
wall(x1, y2).
wall(x5, y4).
north(y2, y1).
north(y5, y4).
wall(x6, y1).
wall(x2, y2).
wall(x4, y2).
west(x4, x3).
north(y4, y3).
north(y3, y2).
wall(x3, y4).
wall(x3, y2).
cdoor(x8, y4).
wall(x6, y3).
west(x8, x7).
wall(x6, y2).
west(x6, x5).
west(x7, x6).
wall(x6, y4).
west(x9, x8).
west(x2, x1).
wall(x2, y4).
wall(x4, y4).
ycoord(y3).
ycoord(y1).
ycoord(y4).
xcoord(x5).
ycoord(y2).
xcoord(x7).
xcoord(x8).
xcoord(x6).
xcoord(x1).
xcoord(x9).
xcoord(x2).
xcoord(x4).
xcoord(x3).
ycoord(y5).
}).

#pos({}, {move_w(x5,y4)},{west(x5, x4).
west(x3, x2).
agentat(x1, y1).
wall(x1, y2).
wall(x5, y4).
north(y2, y1).
north(y5, y4).
wall(x6, y1).
wall(x2, y2).
wall(x4, y2).
west(x4, x3).
north(y4, y3).
north(y3, y2).
wall(x3, y4).
wall(x3, y2).
cdoor(x8, y4).
wall(x6, y3).
west(x8, x7).
wall(x6, y2).
west(x6, x5).
west(x7, x6).
wall(x6, y4).
west(x9, x8).
west(x2, x1).
wall(x2, y4).
wall(x4, y4).
ycoord(y3).
ycoord(y1).
ycoord(y4).
xcoord(x5).
ycoord(y2).
xcoord(x7).
xcoord(x8).
xcoord(x6).
xcoord(x1).
xcoord(x9).
xcoord(x2).
xcoord(x4).
xcoord(x3).
ycoord(y5).
}).

#pos({}, {move_w(x4,y3)},{west(x5, x4).
west(x3, x2).
agentat(x1, y1).
wall(x1, y2).
wall(x5, y4).
north(y2, y1).
north(y5, y4).
wall(x6, y1).
wall(x2, y2).
wall(x4, y2).
west(x4, x3).
north(y4, y3).
north(y3, y2).
wall(x3, y4).
wall(x3, y2).
cdoor(x8, y4).
wall(x6, y3).
west(x8, x7).
wall(x6, y2).
west(x6, x5).
west(x7, x6).
wall(x6, y4).
west(x9, x8).
west(x2, x1).
wall(x2, y4).
wall(x4, y4).
ycoord(y3).
ycoord(y1).
ycoord(y4).
xcoord(x5).
ycoord(y2).
xcoord(x7).
xcoord(x8).
xcoord(x6).
xcoord(x1).
xcoord(x9).
xcoord(x2).
xcoord(x4).
xcoord(x3).
ycoord(y5).
}).

#modeb(1,ycoord(var(ycoord))).
#modeb(1,odoor(var(xcoord),var(ycoord))).
#modeb(1,agentat(var(xcoord),var(ycoord))).
#modeb(1,wall(var(xcoord),var(ycoord))).
#modeb(1,cdoor(var(xcoord),var(ycoord))).
#modeb(1,xcoord(var(xcoord))).
#modeb(1,north(var(ycoord),var(ycoord))).
#modeb(1,west(var(xcoord),var(xcoord))).
#maxv(6).
