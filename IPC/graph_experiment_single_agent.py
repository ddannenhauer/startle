import copy
import matplotlib.pyplot as plt
from matplotlib import cm
import os
import sys
# get the most recent data file

if len(sys.argv) < 2:
    print("Incorrect number of arguments, correct usage:")
    print("python3 graph_experiment_single_agent.py <csv filename>")
    print("<csv filename> results from single agent run of data from compute scores in the form of a .csv file")
    sys.exit()
else:
    csvfilename=sys.argv[1]

    scores = {} # key is action, value is

    header = True
    with open(csvfilename, 'r') as f:
        for line in f.readlines():
            if header:
                header = False
            else:
                row = line.strip().split(',')
                action = (int(row[0]))
                if action not in scores:
                    scores[action]=[(float(row[1]))]
                else:
                    scores[action].append(float(row[1]))


    actions_as_x_vals=[]
    scores_as_y_vals=[]
    for key,val in scores.items():
        actions_as_x_vals.append(key)
        total_score = 0
        for score in val:
            total_score+=score
        total_score/=len(val)
        scores_as_y_vals.append(total_score)

    actions_exploratory3 = []
    score_exploratory3 = []
    for key, val in scores_exploratory3.items():
        actions_exploratory3.append(key)
        total_score = 0
        for score in val:
            total_score += score
        total_score /= len(val)
        score_exploratory3.append(total_score)

    actions_exploratory4 = []
    score_exploratory4 = []
    for key, val in scores_exploratory4.items():
        actions_exploratory4.append(key)
        total_score = 0
        for score in val:
            total_score += score
        total_score /= len(val)
        score_exploratory4.append(total_score)

    actions_exploratory5 = []
    score_exploratory5 = []
    for key, val in scores_exploratory5.items():
        actions_exploratory5.append(key)
        total_score = 0
        for score in val:
            total_score += score
        total_score /= len(val)
        score_exploratory5.append(total_score)


    plt.plot(actions_random,score_random,'-b',label='Random')
    plt.plot(actions_exploratory3,score_exploratory3,'-r',label='Exploration3')
    plt.plot(actions_exploratory4,score_exploratory4,'-g',label='Exploration4')
    plt.plot(actions_exploratory5,score_exploratory5,'-y',label='Exploration5')
    plt.legend()
    plt.xlabel("Number of Actions Executed")
    plt.ylabel("Accuracy")
    plt.title("Learning Accuracy per Number of Actions Executed")
    plt.savefig(domain+'_graph.png')

