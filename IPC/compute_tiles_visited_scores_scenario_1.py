import sys

if len(sys.argv) > 1:
    print("Incorrect number of arguments, this is a script with hardcoded filenames and does not take args")
    sys.exit()
else:
    random_expn_output_file = 'experiment_logs/output_expn_35_random_scenario_1.txt'
    explore_expn_output_file = 'experiment_logs/output_expn_46_explore_only_scenario_1.txt'
    planning_expn_output_files = ['experiment_logs/output_expn_33_scenario_1_only_run_0.txt',
                                  'experiment_logs/output_expn_209_planning_scenario_1_only_run_0.txt',
                                  'experiment_logs/output_expn_42_planning_scenario_1_only_run_0.txt']

    scores_output_file = 'tiles_visited_scores_scenario_1.csv'

    max_num_runs = 3

    header = 'agent_type,run,num_actions,num_tiles_visited\n'

    with open(scores_output_file, 'w+') as output_file:
        output_file.write(header)

        curr_run = None
        curr_num_actions = None
        tiles_visited = []
        print("Processing random expn output file...")
        with open(random_expn_output_file, 'r') as input_file:
            for line in input_file.readlines():
                if  'dcss_ilasp_data' in line and '_run_' in line:
                    run_num = -1
                    i = 0
                    line_parts = line.split('_')
                    for part in line_parts:
                        if part == 'run':
                            run_num = i+1
                        i+=1
                    curr_run = line_parts[run_num]
                    print("Now at run {}".format(curr_run))
                    curr_num_actions = None
                    tiles_visited = [] # very important to reset this!

                if '#A' in line and 'iter_time' in line:
                    curr_num_actions = line[2:8].strip()
                    line_vbar_parts = line.split('|')
                    for part in line_vbar_parts:
                        if 'agentat(' in part:
                            if part not in tiles_visited:
                                tiles_visited.append(part)

                if curr_run and curr_num_actions and int(curr_run) < max_num_runs:
                    output_file.write('{},{},{},{}\n'.format('random',curr_run,curr_num_actions,len(tiles_visited)))

        curr_run = None
        curr_num_actions = None
        tiles_visited = []  # very important to reset this!
        print("Processing explore expn output file...")
        with open(explore_expn_output_file, 'r') as input_file:
            for line in input_file.readlines():
                if  'dcss_ilasp_data' in line and '_run_' in line:
                    run_num = -1
                    i = 0
                    line_parts = line.split('_')
                    for part in line_parts:
                        if part == 'run':
                            run_num = i+1
                        i+=1
                    curr_run = line_parts[run_num]
                    print("Now at run {}".format(curr_run))
                    curr_num_actions = None
                    tiles_visited = []  # very important to reset this!

                if '#A' in line and 'iter_time' in line:
                    curr_num_actions = line[2:8].strip()
                    line_vbar_parts = line.split('|')
                    for part in line_vbar_parts:
                        if 'agentat(' in part:
                            if part not in tiles_visited:
                                tiles_visited.append(part)

                if curr_run and curr_num_actions and int(curr_run) < max_num_runs:
                    output_file.write('{},{},{},{}\n'.format('explore',curr_run,curr_num_actions,len(tiles_visited)))

        # TODO add planning later

        print("Processing planning expn output file...")
        global_run = 0 # need this for these, because runs are spread across files, and we are only using run 0 for each file
        for planning_expn_output_file in planning_expn_output_files:
            curr_run = None
            curr_num_actions = None
            tiles_visited = []  # very important to reset this!
            with open(planning_expn_output_file, 'r') as input_file:
                for line in input_file.readlines():
                    if 'dcss_ilasp_data' in line and '_run_' in line:
                        run_num = -1
                        i = 0
                        line_parts = line.split('_')
                        for part in line_parts:
                            if part == 'run':
                                run_num = i + 1
                            i += 1
                        curr_run = line_parts[run_num]
                        print("Now at run {}".format(curr_run))
                        tiles_visited = []  # very important to reset this!

                    if '#A' in line and 'iter_time' in line:
                        curr_num_actions = line[2:8].strip()
                        line_vbar_parts = line.split('|')
                        for part in line_vbar_parts:
                            if 'agentat(' in part:
                                if part not in tiles_visited:
                                    tiles_visited.append(part)

                    if curr_run and int(curr_run) == 0 and curr_num_actions and global_run < max_num_runs:
                        output_file.write('{},{},{},{}\n'.format('planning', global_run, curr_num_actions, len(tiles_visited)))
            global_run+=1