import copy
import matplotlib.pyplot as plt
from matplotlib import cm
import os
import sys
# get the most recent data file

if len(sys.argv) < 2:
    print("Incorrect number of arguments, correct usage:")
    print("python3 graph_experiment.py <domain identifier>")
    print("<domain> identifier (example: dcss)")
    sys.exit()
else:
    domain=sys.argv[1]
    DATADIR = domain+'_agent_data/'
    #DATADIR = 'experiment_run_2/'


    # get the filenames
    files = sorted([f for f in os.listdir(DATADIR)])
    datafiles = []
    random_files = []
    exploratory_files3 = []
    exploratory_files4 = []
    exploratory_files5 =  []
    for f in files:
        if 'random' in f:
            random_files.append(DATADIR + f)
        elif 'explore-context_size_3' in f: # get the most recent file
            exploratory_files3.append(DATADIR + f)
        elif 'explore-context_size_4' in f:
            exploratory_files4.append(DATADIR + f)
        elif 'explore-context_size_5' in f:
            exploratory_files5.append(DATADIR + f)


    print(str(len(random_files)))
    print(str(len(exploratory_files3)))
    print(str(len(exploratory_files4)))
    print(str(len(exploratory_files5)))

    print("Processing Random")
    scores_random = {}
    for random_file in random_files:
        print("-- About to graph data from "+str(random_file))
        header = True
        with open(random_file, 'r') as f:
            for line in f.readlines():
                if header:
                    header = False
                else:
                    row = line.strip().split(',')
                    action = (int(row[0]))
                    if action not in scores_random:
                        scores_random[action]=[(float(row[1]))]
                    else:
                        scores_random[action].append(float(row[1]))
    # now get the second most recent filename
    print("Processing Exploratory3")
    scores_exploratory3 = {}
    for exploratory_file3 in exploratory_files3:
        print("-- About to graph data from "+str(exploratory_file3))
        header = True
        with open(exploratory_file3, 'r') as f:
            for line in f.readlines():
                if header:
                    header = False
                else:
                    row = line.strip().split(',')
                    action=(int(row[0]))
                    if action not in scores_exploratory3:
                        scores_exploratory3[action]=[float(row[1])]
                    else:
                        scores_exploratory3[action].append(float(row[1]))

    # now get the second most recent filename
    print("Processing Exploratory4")
    scores_exploratory4 = {}
    for exploratory_file4 in exploratory_files4:
        print("-- About to graph data from "+str(exploratory_file4))
        header = True
        with open(exploratory_file4, 'r') as f:
            for line in f.readlines():
                if header:
                    header = False
                else:
                    row = line.strip().split(',')
                    action=(int(row[0]))
                    if action not in scores_exploratory4:
                        scores_exploratory4[action]=[float(row[1])]
                    else:
                        scores_exploratory4[action].append(float(row[1]))
    # now get the second most recent filename
    print("Processing Exploratory5")
    scores_exploratory5 = {}
    for exploratory_file5 in exploratory_files5:
        print("-- About to graph data from "+str(exploratory_file5))
        header = True
        with open(exploratory_file5, 'r') as f:
            for line in f.readlines():
                if header:
                    header = False
                else:
                    row = line.strip().split(',')
                    action=(int(row[0]))
                    if action not in scores_exploratory5:
                        scores_exploratory5[action]=[float(row[1])]
                    else:
                        scores_exploratory5[action].append(float(row[1]))
    actions_random=[]
    score_random=[]
    for key,val in scores_random.items():
        actions_random.append(key)
        total_score = 0
        for score in val:
            total_score+=score
        total_score/=len(val)
        score_random.append(total_score)

    actions_exploratory3 = []
    score_exploratory3 = []
    for key, val in scores_exploratory3.items():
        actions_exploratory3.append(key)
        total_score = 0
        for score in val:
            total_score += score
        total_score /= len(val)
        score_exploratory3.append(total_score)

    actions_exploratory4 = []
    score_exploratory4 = []
    for key, val in scores_exploratory4.items():
        actions_exploratory4.append(key)
        total_score = 0
        for score in val:
            total_score += score
        total_score /= len(val)
        score_exploratory4.append(total_score)

    actions_exploratory5 = []
    score_exploratory5 = []
    for key, val in scores_exploratory5.items():
        actions_exploratory5.append(key)
        total_score = 0
        for score in val:
            total_score += score
        total_score /= len(val)
        score_exploratory5.append(total_score)


    plt.plot(actions_random,score_random,'-b',label='Random')
    plt.plot(actions_exploratory3,score_exploratory3,'-r',label='Exploration3')
    plt.plot(actions_exploratory4,score_exploratory4,'-g',label='Exploration4')
    plt.plot(actions_exploratory5,score_exploratory5,'-y',label='Exploration5')
    plt.legend()
    plt.xlabel("Number of Actions Executed")
    plt.ylabel("Accuracy")
    plt.title("Learning Accuracy per Number of Actions Executed")
    plt.savefig(domain+'_graph.png')

