from tqdm import tqdm
import subprocess
import os
import time
import sys
import copy
import multiprocessing

def filename_action_count(f_tuple=''):
    """ returns the action count of the filename """
    f_parts = f_tuple[0].split('~')
    return int(f_parts[-1].split('.')[0])

if len(sys.argv) < 2:
    print()
else:
    DATA_FILES = str(sys.argv[1]) # contains both .las and _learned_rule.txt files
    las_files = sorted([f for f in os.listdir(DATA_FILES) if f.endswith(".las")])
    learned_rule_files = sorted([f for f in os.listdir(DATA_FILES) if f.endswith("_learned_rule.txt")])

    # build up files per run and agent type
    assert len(las_files) == len(learned_rule_files)
    print("Collecting {} files for scoring...".format(len(las_files)+len(learned_rule_files)))
    
    runs = {} # key is run_id, val is dict {agent:run_files}
    
    for i in range(len(las_files)):
        f_trimmed = las_files[i][0:-4] # removes the .las ending
        f_parts = f_trimmed.split('~')
        run_id = int(f_parts[0])
        agent_name = str(f_parts[1])

        if run_id in runs.keys():
            if agent_name in runs[run_id].keys():
                # add learned_rule file for the agent
                runs[run_id][agent_name].append((las_files[i],learned_rule_files[i]))
            else:
                runs[run_id][agent_name] = [(las_files[i],learned_rule_files[i])]
        else:
            runs[run_id] = {} # key is agent_name, val is all of their files
            runs[run_id][agent_name] = [(las_files[i],learned_rule_files[i])]

    last_action_example_files = {} # key is action_name, val is list of files for each action
            
    # sort, get last ilasp files per action, and print out a summary
    for r in runs.keys():
        print('  run {}'.format(r))
        for agentname in runs[r].keys():
            last_action_files = {} # key is action name, val is last file of this action
            runs[r][agentname] = sorted(runs[r][agentname],key=filename_action_count)
            for f_tuple in runs[r][agentname]:
                f_parts = f_tuple[0].split('~')
                f_action_name = str(f_parts[2])
                last_action_files[f_action_name] = [f_tuple] # since they are sorted, the last one will be the maximal one
            print('    {} has {} learned_rule.txt files and has {} last action files'.format(agentname, len(runs[r][agentname]),len(last_action_files.keys())))
            for action_name in last_action_files.keys():
                if action_name in last_action_example_files.keys():
                    last_action_example_files[action_name]+=last_action_files[action_name]
                else:
                    last_action_example_files[action_name]=last_action_files[action_name]
            #for a,f in last_action_files.items():
            #    print('      {}:{}'.format(a,f))
            #if r==7:
            #    for f in runs[r][agentname]:
            #        print('    {}'.format(f))

    #print("Last_action_files keys are {}".format(last_action_files.keys()))
    #time.sleep(3)
    # now that we have the last action files, we can turn all the
    # examples into individual asp files, and run clingo against them
    # with the learned rules

    state_strs_per_action = {} # key is an action_name, val is a list
                               # of strings, where each string is ASP
                               # background knowledge, that only needs
                               # the learned rule to be appended and
                               # then can be run against clingo to
                               # verify if the rule was correct or not
    for a in last_action_example_files.keys():
        #print("-*- action {} has {} ilasp files from all the runs -*-".format(a,len(last_action_example_files[a])))
        for f_tuple in last_action_example_files[a]:
            #print('    files are {}'.format(f_tuple))
            # open the file
            during_parsing_example=False
            end_of_pos_example = '}).'
            asp_strs_whole_file = []
            asp_str = ''
            with open(DATA_FILES+f_tuple[0],'r') as ilasp_examples_f:
                lines = ilasp_examples_f.readlines()
                for line in lines:
                    #print('line is {}'.format(line))
                    if '#pos({' in line:
                        asp_str = ''                
                        during_parsing_example = True
                        line_parts = line.split('}, {')
                        line_parts = line_parts[0:-1] + line_parts[-1].split('},{') # eww i know
                        asp_str += line_parts[2].strip()+'\n'
                        #print('line_parts are {}'.format(line_parts))
                    elif end_of_pos_example in line:
                        during_parsing_example = False
                        # save this asp str
                        if a in state_strs_per_action.keys():
                            state_strs_per_action[a]+=[copy.copy(asp_str)]                                    
                        else:
                            state_strs_per_action[a]=[copy.copy(asp_str)]                                    
                    elif during_parsing_example:
                        asp_str +=line.strip() + '\n'

    #print("state_strs_per_action keys are {}".format(state_strs_per_action.keys()))
    #time.sleep(3)
                        
    print("Getting states to use for scoring from last action .las files...")
    all_unique_states = []
    for a in state_strs_per_action.keys():
        state_strs_per_action[a] = set(state_strs_per_action[a])     # remove any duplicates
        all_unique_states+=state_strs_per_action[a]
        #print('  acquired {} examples for action {}'.format(len(state_strs_per_action[a]),a))
        #if 'move_w' in a:
        #    for i in range(3):
        #        print('asp_str {} for action {} is...'.format(i,a))
        #        print(list(state_strs_per_action[a])[i])
        #        print('...done w asp str')

    all_unique_states = set(all_unique_states)
    # for kicks, lets also create a set of all the unique states from all the runs of all the actions
    

        
    # Get perfect rules
    print("Obtaining perfect rules from PDDL domain files and converting to ASP...")
    actions_perfect_rules_pddl = {} # key is action, val is pddl text of perfect rule (from domain.pddl file)
    domain_pddl_filename = 'pddl_domains/'+DATA_FILES.split('_')[0]+'_domain.pddl'
    with open(domain_pddl_filename,'r') as domain_pddl_f:
        last_action = None # serves as both a flag and storing the last action name seen
        processed_parameters = False
        found_preconditions = False
        head = ''
        i = 0
        for line in domain_pddl_f.readlines():
            #print('{}: {}'.format(i,line))
            i+=1
            if '(:action' in line:
                found_preconditions = False
                processed_parameters = False
                for a in state_strs_per_action.keys():
                    line_action_name = line.split(' ')[-1].strip()
                    #print("a = *{}* | line_action_name = *{}* | line is *{}*".format(a,line_action_name,line))
                    if a == line_action_name:
                        last_action = a

            if ':parameters' in line:
                if last_action and not processed_parameters: # assumes parameters are on a single line
                    line_parts = line.split('?') # split on the variables
                    line_parts = line_parts[1:] # drop the first item because its just the ':parameter' string
                    head = last_action+'('
                    for param in line_parts:
                        param_var = param.split('-')[0].strip()
                        head+='?'+param_var+','
                    #print("head is {}".format(head))
                    head = head[0:-1] # remove the last comma
                    head+=')'
                    processed_parameters = True
                    
            if ':precondition' in line: # big assumption that preconditions are all on the same line!                    
                if last_action and processed_parameters and not found_preconditions: # check if we are at preconditions line
                    actions_perfect_rules_pddl[copy.copy(last_action)] = (copy.copy(head),line[15:])
                    #print('  Recorded action {} with perfect preconditions {}'.format(last_action, actions_perfect_rules_pddl[last_action]))
                    found_preconditions = True
                    last_action = None
                    head = ''

    #print("actions_perfect_rules_pddl.keys() are {}".format(actions_perfect_rules_pddl.keys()))
    #time.sleep(3)
                    
    print("Converting perfect PDDL rules into ASP rules")
    actions_perfect_rules_asp = {} # key is action, val is asp text of perfect rule
    var_translate = {}
    var_counter = 0
    # now convert pddl rules to asp text
    for a in actions_perfect_rules_pddl.keys():
        pddl_rule_str_head, pddl_rule_str_body = actions_perfect_rules_pddl[a]
        pddl_rule_str_body = pddl_rule_str_body[4:] # remove: (and
        pddl_rule_str_body = pddl_rule_str_body[:-1] # remove: )
        terms = pddl_rule_str_body.split(')(')
        terms[0] = terms[0].replace('(','') # remove the starting paren on the first term
        terms[-1] = terms[-1].replace(')','') # remove closing parens
        asp_rule = ''
        for i in range(len(terms)):
            has_not = False
            #print('  processing term {}'.format(terms[i]))
            if 'not' in terms[i]:
                has_not = True
                terms[i] = terms[i][4:].replace('(','').replace(')','') # remove the not
                #print('    terms[{}] is now {}'.format(i,terms[i]))
            terms[i] = terms[i].strip()
            term_parts = terms[i].split(' ')
            new_term_head = term_parts[0]
            new_term_vars = term_parts[1:]
            new_term_str = new_term_head+'('
            #print('action {} | new_term_vars are {}'.format(a,new_term_vars))
            #input("Press anything to continue")
            #print("new_term_vars are {}".format(new_term_vars))
            for tv in new_term_vars:
                if tv not in var_translate.keys():
                    #print("here1")
                    var_translate[tv] = 'V'+str(var_counter)
                    var_counter+=1
                    new_term_str+=var_translate[tv]+','
                else:
                    #print("here2")                    
                    new_term_str+=var_translate[tv]+','
                
            new_term_str = new_term_str[:-1] # remove last comma
            new_term_str+=')'
            #input("New term str is {} for action {} (press to continue)".format(new_term_str,a))
            if has_not:
                terms[i] = 'not '+new_term_str
            else:
                terms[i] = new_term_str

        # replace vars in head
        head_str_easier = pddl_rule_str_head.replace('(',' ').replace(')',' ').replace(',',' ').strip()
        head_parts = head_str_easier.split(' ')
        head_pred = head_parts[0]
        head_vars = head_parts[1:]

        # add head to asp str
        asp_rule = head_pred+'('
        for hv in head_vars:
            if hv in var_translate.keys():
                asp_rule+=var_translate[hv]+','
            else:
                # do nothing, because its in the effects - and we are only learning preconditions
                pass
        asp_rule = asp_rule[:-1] # remove the last comma
        asp_rule+='):-'
        
        for t in terms:
            asp_rule+=t+','
        asp_rule = asp_rule[:-1] # remove last comma
        asp_rule += '.' # add period for asp ending
        #print("just added action {}".format(a))
        actions_perfect_rules_asp[a] = asp_rule
            
        # now change to asp format
        
        #print("For {} perfect pddl rule is {}".format(a,actions_perfect_rules_pddl[a]))
        #print("  For {}, perfect asp rule is {}".format(a,actions_perfect_rules_asp[a]))
        #print("For {} have pddl term parts {}".format(a,terms))

        
    ##### About to do scoring by calling clingo #####
    # Now generate temp files for calling clingo
    
    temp_dir = DATA_FILES[0:-1]+'_temp_clingo_files/'

    def run_single_eval(learned_file,DATA_FILES,all_unique_states,temp_dir,actions_perfect_rules_asp):
        #print("here")
        #print('{}'.format(learned_file))
        #print('{}'.format(DATA_FILES))
        #print('{}'.format(len(all_unique_states)))
        #print('{}'.format(temp_dir))
        #print('{}'.format(len(actions_perfect_rules_asp.keys())))
        
        #print("I'm getting args\n{}\n{}\n{}\n{}\n{}\n{}".format(learned_file,len(DATA_FILES),len(all_unique_states),temp_dir,len(actions_perfect_rules_asp)))
        f_parts = learned_file.split('~')
        run_id = int(f_parts[0])
        agent_name = str(f_parts[1])
        action_name = str(f_parts[2])
        action_count = int(f_parts[3].split('_')[0])

        # first check to see if a rule was learned (if not learned, then score = 0)
        learned_rule_str = None
        with open(DATA_FILES+learned_file,'r') as learned_f:
            for line in learned_f.readlines():
                if ':-' in line:
                    learned_rule_str = line

        learn = False
        if learned_rule_str:
            learn = True

        # we're going to check against all states for this action
        eval_count = 0
        precision_values_this_action = []
        recall_values_this_action = []
        true_pos_len_acc = 0
        false_neg_len_acc = 0
        false_pos_len_acc = 0
        #total_score_this_action = 0
        #total_possible_score_this_action = 0
        for eval_state in all_unique_states: #state_strs_per_action[action_name]:
            # write the file for clingo
            eval_count+=1
            curr_eval_filename = temp_dir+str(run_id)+'_'+str(agent_name)+'_'+str(action_name)+'_'+str(action_count)+'_'+str(eval_count)
            with open(curr_eval_filename,'w') as es_f:
                es_f.write(eval_state+'\n')
                perfect_rule = 'perfect_'+actions_perfect_rules_asp[action_name]
                #print("Perfect_rule is {}".format(perfect_rule))
                if learn: learned_rule = 'learned_'+learned_rule_str
                es_f.write(perfect_rule+'\n')
                if learn: es_f.write(learned_rule+'\n')
                num_perfect_params = perfect_rule.split(':-')[0].count(',') + 1
                if learn: num_learned_params = learned_rule.split(':-')[0].count(',') + 1
                perfect_rule_pred = perfect_rule.split('(')[0]
                if learn: learned_rule_pred = learned_rule.split('(')[0]
                es_f.write('#show '+perfect_rule_pred+'/'+str(num_perfect_params)+'.\n')
                if learn: es_f.write('#show '+learned_rule_pred+'/'+str(num_learned_params)+'.\n')                       
                #print('check on the file: {}'.format(curr_eval_filename))

            # now run clingo on the file
            null_f = open(os.devnull, 'w')
            completed_process = subprocess.run(["clingo", curr_eval_filename], stdout=subprocess.PIPE,stderr=null_f)
            result_output_str = completed_process.stdout.decode()
            null_f.close()
            started_getting_results = False
            finished_getting_results = False
            learned_args = []
            perfect_args = []
            #print("result_output_str is {}".format(result_output_str))
            #if learn:
                #print("learned_rule_pred = {}".format(learned_rule_pred))
            for line in result_output_str.split('\n'):
                if started_getting_results and not finished_getting_results:
                    for res in line.split(' '):
                        # get the args and store them as a unique str
                        if learn and learned_rule_pred in res:
                            learned_args.append(res[res.find("(")+1:res.find(")")])
                        if perfect_rule_pred in res:
                            perfect_args.append(res[res.find("(")+1:res.find(")")])

                    #print(' results: {}'.format(line))
                    #print('    perfect args are {}'.format(perfect_args))
                    #print('    learned args are {}'.format(learned_args))
                    ls = set(learned_args)
                    ps = set(perfect_args)

                    true_pos = ls & ps
                    false_neg = ps - ls
                    false_pos = ls - ps

                    true_pos_len_acc += len(true_pos)
                    false_neg_len_acc += len(false_neg)
                    false_pos_len_acc += len(false_pos)                        

                    precision = 0                        
                    if len(false_pos) == 0:
                        precision = 1
                    else:
                        precision = 1.0*len(true_pos) / 1.0* (len(true_pos) + len(false_pos))                            

                    recall = 0
                    if len(false_neg) == 0:
                        recall = 1
                    else:
                        recall = 1.0*len(true_pos) / 1.0 * (len(true_pos) + len(false_neg))

                    # special case when both ls and ps are empty:
                    if len(ls) == 0 and len(ps) == 0:
                        precision = 1
                        recall = 1

                    precision_values_this_action.append(precision)
                    recall_values_this_action.append(recall)        

                    #total_score_this_action += score
                    #total_possible_score_this_action += len(ps)

                if 'Answer' in line:
                    started_getting_results = True
                    finished_getting_results = False
                if 'SATISFIABLE' in line:
                    finished_getting_results = True
                    started_getting_results = False
            # exit for loop processing this individual file's results 
            #print("*****DONE PROCESSING FROM CLINGO****")
        # exit for loop going over all eval files

        # these are over all eval states
        global_precision = 1.0*true_pos_len_acc / 1.0*(true_pos_len_acc + false_pos_len_acc)
        global_recall = 1.0*true_pos_len_acc / 1.0*(true_pos_len_acc + false_neg_len_acc) 

        if len(precision_values_this_action) == 0:
            print("ERROR: no precision values obtained for action {} after {} eval states".format(action_name,len(all_unique_states)))
        avg_precision = sum(precision_values_this_action) / len(precision_values_this_action)
        if len(recall_values_this_action) == 0:
            print("ERROR: no recall values obtained for action {} after {} eval states".format(action_name,len(all_unique_states)))                
        avg_recall = sum(recall_values_this_action) / len(recall_values_this_action)
        return '{},{},{},{},{},{},{},{}\n'.format(run_id,agent_name,action_name,action_count,avg_precision,avg_recall,global_precision,global_recall)

    pbar = tqdm(total=len(learned_rule_files))

    results = [None] * len(learned_rule_files)
    
    def wrapMyFunc(i,learned_rule_files,DATA_FILES,all_unique_states,temp_dir,actions_perfect_rules_asp):
        #print("i is {}".format(i))
        single_result = ''
        try:
            learned_f = learned_rule_files[i]
            #print("learned_f is {}".format(learned_f))
            single_result = run_single_eval(learned_rule_files[i],DATA_FILES,all_unique_states,temp_dir,actions_perfect_rules_asp)
        except:
            #print("Something happened")
            #print('{}'.format(sys.exc_info()))
            raise
        return i, single_result

    def update(args):
        # note: input comes from async 'wrapMyFunc'
        #print("args are {}".format(args))
        i = args[0]
        ans = args[1]
        results[i] = ans
        pbar.update()
        
    print("Calling clingo for scoring over {} files...".format(len(learned_rule_files)))
    pool = multiprocessing.Pool(20) # 20 processes
    for i in range(len(learned_rule_files)):
        pool.apply_async(wrapMyFunc, args=(i,copy.deepcopy(learned_rule_files),copy.deepcopy(DATA_FILES),copy.deepcopy(all_unique_states),copy.deepcopy(temp_dir),copy.deepcopy(actions_perfect_rules_asp),), callback=update)

    pool.close()
    pool.join()
    pbar.close()

    data_output_file = DATA_FILES[0:-1]+'_scores.csv'    
    with open(data_output_file,'w') as data_f:
        # header is: run, agentname, action_count, score
        data_f.write('run,agent_name,action_name,action_count,avg_precision,avg_recall,global_precision,global_recall\n')    
        for i in results:
            #print("writing: {}".format(i))
            if i:
                data_f.write(str(i))
            else:
                #data_f.write('bad data\n')
                pass

    
            
        
        
