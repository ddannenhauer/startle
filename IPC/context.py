from term import clean, get_arg_types, Term
class Context:

    def __init__(self, terms:[Term]=[]):

        self.terms = terms
        self.size = len(terms)

    def all_terms_same(self):
        return len(set(self.terms)) == 1

    def all_lowercase_simple_str(self):
        pass

    def add_term(self, term):
        self.terms.append(term)
        self.size+=1

    def get_terms(self):
        return self.terms

    def json_str(self):
        context_str = '['
        for term in self.terms:
            if isinstance(term,Term):
                context_str += '\"'+term.get_pred_str().lower() + ","
                for arg in term.get_args():
                    context_str += str(arg).lower() + ','
                context_str = context_str[0:-1]  # remove trailing comma
                context_str += '\",'
            else:
                context_str += '\"'+ term + '\",'

        context_str = context_str[0:-1] # remove trailing comma
        context_str += ']'
        return context_str.replace('\r','')

    def __str__(self):
        context_str = ''
        for term in self.terms:
            context_str += str(term).lower() + ','
        return context_str