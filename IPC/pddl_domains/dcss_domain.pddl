(define (domain dcss)
(:requirements :typing :equality)
    (:types
	xcoord
	ycoord
    )
    (:predicates
	(agentat ?x - xcoord ?y - ycoord)
	(wall ?x - xcoord ?y - ycoord)
	(north ?y1 - ycoord ?y2 - ycoord)
	(west ?x1 - xcoord ?x2 - xcoord)
	(cdoor ?x - xcoord ?y - ycoord)
	(odoor ?x - xcoord ?y - ycoord)
    )
    (:action move_n
        :parameters (?x - xcoord ?y - ycoord)
	:precondition (and (agentat ?x ?iy)(north ?y ?iy)(not (wall ?x ?y))(not (cdoor ?x ?y)))
	:effect (and (agentat ?x ?y)(not (agentat ?x ?iy)))
    )
    (:action move_s
        :parameters (?x - xcoord ?y - ycoord)
	:precondition (and (agentat ?x ?iy)(north ?iy ?y)(not (wall ?x ?y))(not (cdoor ?x ?y)))
	:effect (and (agentat ?x ?y)(not (agentat ?x ?iy)))
    )
    (:action move_w
        :parameters (?x - xcoord ?y - ycoord)
	:precondition (and (agentat ?ix ?y)(west ?x ?ix)(not (wall ?x ?y))(not (cdoor ?x ?y)))
	:effect (and (agentat ?x ?y)(not (agentat ?ix ?y)))
    )
    (:action move_e
        :parameters (?x - xcoord ?y - ycoord)
	:precondition (and (agentat ?ix ?y)(west ?ix ?x)(not (wall ?x ?y))(not (cdoor ?x ?y)))
	:effect (and (agentat ?x ?y)(not (agentat ?ix ?y)))
    )
    (:action move_nw
        :parameters (?x - xcoord ?y - ycoord)
	:precondition (and (agentat ?ix ?iy)(north ?y ?iy)(west ?x ?ix)(not (wall ?x ?y))(not (cdoor ?x ?y)))
	:effect (and (agentat ?x ?y)(not (agentat ?ix ?iy)))
    )
    (:action move_ne
        :parameters (?x - xcoord ?y - ycoord)
	:precondition (and (agentat ?ix ?iy)(north ?y ?iy)(west ?ix ?x)(not (wall ?x ?y))(not (cdoor ?x ?y)))
	:effect (and (agentat ?x ?y)(not (agentat ?ix ?iy)))
    )
    (:action move_sw
        :parameters (?x - xcoord ?y - ycoord)
	:precondition (and (agentat ?ix ?iy)(north ?iy ?y)(west ?x ?ix)(not (wall ?x ?y))(not (cdoor ?x ?y)))
	:effect (and (agentat ?x ?y)(not (agentat ?ix ?iy)))
    )
    (:action move_se
        :parameters (?x - xcoord ?y - ycoord)
	:precondition (and (agentat ?ix ?iy)(north ?iy ?y)(west ?ix ?x)(not (wall ?x ?y))(not (cdoor ?x ?y)))
	:effect (and (agentat ?x ?y)(not (agentat ?ix ?iy)))
    )
    (:action open_door_n
        :parameters (?x - xcoord ?y - ycoord)
	:precondition (and (agentat ?x ?iy)(north ?y ?iy)(cdoor ?x ?y))
	:effect (and (odoor ?x ?y)(not (cdoor ?x ?y)))
    )
    (:action open_door_s
        :parameters (?x - xcoord ?y - ycoord)
	:precondition (and (agentat ?x ?iy)(north ?iy ?y)(cdoor ?x ?y))
	:effect (and (odoor ?x ?y)(not (cdoor ?x ?y)))
    )
    (:action open_door_w
        :parameters (?x - xcoord ?y - ycoord)
	:precondition (and (agentat ?ix ?y)(west ?x ?ix)(cdoor ?x ?y))
	:effect (and (odoor ?x ?y)(not (cdoor ?x ?y)))
    )
    (:action open_door_e
        :parameters (?x - xcoord ?y - ycoord)
	:precondition (and (agentat ?ix ?y)(west ?ix ?x)(cdoor ?x ?y))
	:effect (and (odoor ?x ?y)(not (cdoor ?x ?y)))
    )
    (:action open_door_nw
        :parameters (?x - xcoord ?y - ycoord)
	:precondition (and (agentat ?ix ?iy)(north ?y ?iy)(west ?x ?ix)(cdoor ?x ?y))
	:effect (and (odoor ?x ?y)(not (cdoor ?x ?y)))
    )
    (:action open_door_ne
        :parameters (?x - xcoord ?y - ycoord)
	:precondition (and (agentat ?ix ?iy)(north ?y ?iy)(west ?ix ?x)(cdoor ?x ?y))
	:effect (and (odoor ?x ?y)(not (cdoor ?x ?y)))
    )
    (:action open_door_sw
        :parameters (?x - xcoord ?y - ycoord)
	:precondition (and (agentat ?ix ?iy)(north ?iy ?y)(west ?x ?ix)(cdoor ?x ?y))
	:effect (and (odoor ?x ?y)(not (cdoor ?x ?y)))
    )
    (:action open_door_se
        :parameters (?x - xcoord ?y - ycoord)
	:precondition (and (agentat ?ix ?iy)(north ?iy ?y)(west ?ix ?x)(cdoor ?x ?y))
	:effect (and (odoor ?x ?y)(not (cdoor ?x ?y)))
    )
    (:action close_door_n
        :parameters (?x - xcoord ?y - ycoord)
	:precondition (and (agentat ?x ?iy)(north ?y ?iy)(odoor ?x ?y))
	:effect (and (cdoor ?x ?y)(not (odoor ?x ?y)))
    )
    (:action close_door_s
        :parameters (?x - xcoord ?y - ycoord)
	:precondition (and (agentat ?x ?iy)(north ?iy ?y)(odoor ?x ?y))
	:effect (and (cdoor ?x ?y)(not (odoor ?x ?y)))
    )
    (:action close_door_w
        :parameters (?x - xcoord ?y - ycoord)
	:precondition (and (agentat ?ix ?y)(west ?x ?ix)(odoor ?x ?y))
	:effect (and (cdoor ?x ?y)(not (odoor ?x ?y)))
    )
    (:action close_door_e
        :parameters (?x - xcoord ?y - ycoord)
	:precondition (and (agentat ?ix ?y)(west ?ix ?x)(odoor ?x ?y))
	:effect (and (cdoor ?x ?y)(not (odoor ?x ?y)))
    )
    (:action close_door_nw
        :parameters (?x - xcoord ?y - ycoord)
	:precondition (and (agentat ?ix ?iy)(north ?y ?iy)(west ?x ?ix)(odoor ?x ?y))
	:effect (and (cdoor ?x ?y)(not (odoor ?x ?y)))
    )
    (:action close_door_ne
        :parameters (?x - xcoord ?y - ycoord)
	:precondition (and (agentat ?ix ?iy)(north ?y ?iy)(west ?ix ?x)(odoor ?x ?y))
	:effect (and (cdoor ?x ?y)(not (odoor ?x ?y)))
    )
    (:action close_door_sw
        :parameters (?x - xcoord ?y - ycoord)
	:precondition (and (agentat ?ix ?iy)(north ?iy ?y)(west ?x ?ix)(odoor ?x ?y))
	:effect (and (cdoor ?x ?y)(not (odoor ?x ?y)))
    )
    (:action close_door_se
        :parameters (?x - xcoord ?y - ycoord)
	:precondition (and (agentat ?ix ?iy)(north ?iy ?y)(west ?ix ?x)(odoor ?x ?y))
	:effect (and (cdoor ?x ?y)(not (odoor ?x ?y)))
    )
)
