import os,re
import sys
import numpy as np
from LogicHandler import LogicHandler
from term import get_arg_types, clean, get_all_arg_types, Term
from copy import deepcopy
import time
from agent import CrawlAIAgent
import json
from subprocess import Popen,STDOUT,PIPE
from permute import permute_position_unique, valid
from threading import Timer
import itertools
import collections
from threads import BaseThread
import random
from context import Context
import logging

class ContextGenerator:

    def __init__(self,game_state,context_size):

        self.include_nots=False ###SET FOR INCLUDING NOTS IN CONTEXTS
        self.context_size=context_size
        self.game_state=game_state
        self.methods={}
        self.objects={}
        self.all_contexts_list=[]
        self.objInstances={}
        self.actionParameters=[]
        self.varFormat='v%d'
        self.reVar=re.compile('^v[0-9]+$')
        self.re_num=re.compile('^-?[0-9]+$')
        self.contexts=set()
        self.all_contexts={}
        self.num_contexts=0
        self.learning_file_number=0
        self.hand_contexts={"['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord0,ycoord1', 'wall,cell1', 'is_one_minus,ycoord0,ycoord1']": None,
                            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord0,ycoord1', 'wall,cell1', 'is_one_minus,ycoord1,ycoord0']": None,
                            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord1,ycoord0', 'wall,cell1', 'is_one_minus,xcoord0,xcoord1']": None,
                            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord1,ycoord0', 'wall,cell1', 'is_one_minus,xcoord1,xcoord0']": None,
                            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord1,ycoord1', 'wall,cell1', 'is_one_minus,ycoord0,ycoord1', 'is_one_minus,xcoord0,xcoord1']": None,
                            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord1,ycoord1', 'wall,cell1', 'is_one_minus,ycoord0,ycoord1', 'is_one_minus,xcoord1,xcoord0']": None,
                            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord1,ycoord1', 'wall,cell1', 'is_one_minus,ycoord1,ycoord0', 'is_one_minus,xcoord0,xcoord1']": None,
                            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord1,ycoord1', 'wall,cell1', 'is_one_minus,ycoord1,ycoord0', 'is_one_minus,xcoord1,xcoord0']": None,
                            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord0,ycoord1', 'closed_door,cell1', 'is_one_minus,ycoord0,ycoord1']": None,
                            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord0,ycoord1', 'closed_door,cell1', 'is_one_minus,ycoord1,ycoord0']": None,
                            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord1,ycoord0', 'closed_door,cell1', 'is_one_minus,xcoord0,xcoord1']": None,
                            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord1,ycoord0', 'closed_door,cell1', 'is_one_minus,xcoord1,xcoord0']": None,
                            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord1,ycoord1', 'closed_door,cell1', 'is_one_minus,ycoord0,ycoord1', 'is_one_minus,xcoord0,xcoord1']": None,
                            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord1,ycoord1', 'closed_door,cell1', 'is_one_minus,ycoord0,ycoord1', 'is_one_minus,xcoord1,xcoord0']": None,
                            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord1,ycoord1', 'closed_door,cell1', 'is_one_minus,ycoord1,ycoord0', 'is_one_minus,xcoord0,xcoord1']": None,
                            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord1,ycoord1', 'closed_door,cell1', 'is_one_minus,ycoord1,ycoord0', 'is_one_minus,xcoord1,xcoord0']": None,
                            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord0,ycoord1', 'deepwater,cell1', 'is_one_minus,ycoord0,ycoord1']": None,
                            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord0,ycoord1', 'deepwater,cell1', 'is_one_minus,ycoord1,ycoord0']": None,
                            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord1,ycoord0', 'deepwater,cell1', 'is_one_minus,xcoord0,xcoord1']": None,
                            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord1,ycoord0', 'deepwater,cell1', 'is_one_minus,xcoord1,xcoord0']": None,
                            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord1,ycoord1', 'deepwater,cell1', 'is_one_minus,ycoord0,ycoord1', 'is_one_minus,xcoord0,xcoord1']": None,
                            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord1,ycoord1', 'deepwater,cell1', 'is_one_minus,ycoord0,ycoord1', 'is_one_minus,xcoord1,xcoord0']": None,
                            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord1,ycoord1', 'deepwater,cell1', 'is_one_minus,ycoord1,ycoord0', 'is_one_minus,xcoord0,xcoord1']": None,
                            "['agentat,cell0','at,cell0,xcoord0,ycoord0','at,cell1,xcoord1,ycoord1', 'deepwater,cell1', 'is_one_minus,ycoord1,ycoord0', 'is_one_minus,xcoord1,xcoord0']": None,
                            "['agentat,cell0','item,cell0,item_type0,quantity0)']": None}



#take first atom in rule, save all possible values of the variables. weed out variables as we go. sets do matter still.



#CONTEXT FORMAT#
#{
# 'lists':[all combinations of variable mappings for a set of terms],
# 'heads':{atom_head:{'count':times this head in atom set, 'args': {atom_body_arg:num times in atom set,}},},
# 'args' :{atom_body_arg: {'count':num times arg in atom set, 'in',list of heads in set arg is in},}
#}
    def in_context2(self, context, asp_str):
        context=json.loads(context)
        atoms=asp_str.split(".\n")
        for i in range(len(atoms)):
            temp=re.split('\(|\)', atoms[i])
            clean(temp)
            if len(temp)<2:
                atoms[i]=''
                continue
            head=temp[0]
            body=temp[1].split(",")
            atoms[i]=(head,body)
        clean(atoms)
        found=[]
        first=True
        for c in context:
            items=c.split(",")
            head=items[0]
            body=items[1:]
            for atom in atoms:
                if head==atom[0]:
                    for i in range(len(body)):
                        cset={'altered':False}
                        if first:
                            cset[body[i]]=atom[1][i]
                            cset['altered']=True
                            found.append(cset)
                            first=False
                        else:
                            newfound=[]
                            for s in found:
                                if body[i] in s and atom[1][i]==s[body[i]]:
                                    s['altered']=True
                                else:
                                    ns=deepcopy(s)
                                    ns[body[i]]=atom[1][i]
                                    ns['altered']=True
                                    newfound.append(ns)
                            found+=newfound
                    for i in range(len(found)):
                        if not found[i]['altered']:
                            found[i]=''
                        else:
                            found[i]['altered']=False
                    clean(found)
        if len(found)>0:
            return True
        else:
            return False


    def build_context_string(self, context,counter):#sets up in context set for exploratory actions
        context=json.loads(context)
        vars={}
        lifted_vars=[]
        rule_body=""
        argc=0
        for c in context:
            items=c.split(',')
            head=items[0]
            body=items[1:]
            for i in range(len(body)):
                if body[i] in vars:
                    body[i]="V"+str(vars[body[i]])
                else:
                    v="V"+str(argc)
                    vars[body[i]]=str(argc)
                    argc+=1
                    body[i]=v
                    lifted_vars.append(v)
            if head=="is_one_minus":
                rule_body+=body[0] + " = " + body[1] + " - 1, "
                continue
            if len(body)>0:
                if not counter==None:
                    add_string = head + str(tuple(body)) + " "
                else:
                    add_string = head + str(tuple(body))
                    if len(body)==1:
                        add_string = add_string[:-1]+' T) '
                    elif len(body)>1:
                        add_string = add_string[:-1]+', T) '
            else:
                if not counter==None:
                    add_string = head+" "
                else:
                    if "=" not in head:
                        add_string = head+"(T) "
                    else:
                        add_string = head +" "
            if len(body)==1:
                if not counter==None:
                    add_string=re.sub(",","",add_string)

            rule_body+=add_string[:-1]+", "
        for var in vars:
            sub = "V" + str(vars[var])
            rule_body = re.sub(var, sub, rule_body)
        rule_body=rule_body[:-2]
        var_str="("
        for var in lifted_vars:
            var_str+=var+', '
        var_str=var_str[:-2]+')'
        if not counter == None:
            rule_head = "context" + str(counter) + var_str
        else:
            rule_head = "context" + var_str
            if var_str==')':
                rule_head = rule_head[:-1]+'(T)'
            else:
                rule_head = rule_head[:-1]+', T)'
        rule_head=re.sub(",\)",")",rule_head)
        rule_head=re.sub("'","",rule_head)
        rule_body = re.sub("'", "", rule_body)
        rule=rule_head+ " :- "+rule_body+"."
        return (rule,lifted_vars,vars)

    def get_context_answer_set(self, asp_str):
        p = Popen(['clingo'], shell=False, stdout=PIPE, stderr=STDOUT, stdin=PIPE)
        textout, inp = p.communicate(input=asp_str.encode('utf-8'))
        textout=(textout.decode('utf-8'))
        #print("textout is {}".format(textout))
        return textout

    def answer_set_list_from_raw_scoring(self, answers):
        answer_set = set()
        t_answers=answers.split('\n')
        for idx,answer in enumerate(t_answers):
            if "Answer:" in answer:
                answers=t_answers[idx+1]
                break
        for answer in answers.split(' '):
            head = self.get_seperated_atom(answer)[0]
            if "_perfect" in head or "_learned" in head:
                answer_set.add(answer)
        return answer_set

    def answer_set_list_from_raw(self, answers):
        answer_set = set()
        t_answers=answers.split('\n')
        flag=False
        for idx,answer in enumerate(t_answers):
            if "Answer:" in answer:
                flag=True
                answers=t_answers[idx+1]
        if not flag:
            return answer_set
        for answer in answers.split(' '):
            term_list=self.get_seperated_atom(answer)
            head=term_list[0].strip()
            if len(head) > 0:
                body = term_list[1]
                answer_set.add((head,body))
        return answer_set

    def answer_set_list_from_raw_contexts(self, answers):
        answer_set = {}
        #print(answers)
        t_answers=answers.split('\n')
        for idx, answer in enumerate(t_answers):
            if "Answer:" in answer:
                answers=t_answers[idx+1]
                break
        for answer in answers.split(' '):
            head = self.get_seperated_atom(answer)[0]
            if "context" in head:
                body = self.get_seperated_atom(answer)[1]
                if head not in answer_set:
                    answer_set[head]=[]
                answer_set[head].append(body)
        return answer_set

    def get_seperated_atom(self,atom):
        vars = re.split('\(|\)', atom)
        return vars

    def in_context(self,answer_set,id):
        action_name="context"+str(id)
        if action_name in answer_set:
            return True
        #         vars = self.get_seperated_atom(atom)
        #         vars = vars[1].split(',')
        #         counter = 0
        #         for var in vars:
        #             sub_answers[var] = lifted_vars[counter]
        #             counter = counter + 1
        #         answers.append(sub_answers)
        # if (len(answers))>0:
        #     return True
        return False

    def choose_context(self,tried):
        max_count=0
        chosen_contexts=[]
        finished_contexts=[]
        for context in self.all_contexts:
            if context in tried:
                continue
            count=0
            skip_list=['id','vars']
            for action_set in self.all_contexts[context]:
                if action_set in skip_list:
                    continue
                if self.all_contexts[context][action_set]==0:
                    count+=1
            if count==0:
                finished_contexts.append(context)
            if count>max_count:
                chosen_contexts=[context]
                max_count=count
            elif count==max_count:
                chosen_contexts.append(context)
        if max_count==0:
            return False,finished_contexts
        return chosen_contexts,finished_contexts

    def planning_for_context_as_goal(self, context, agent, lasttimebase=10):
        # print(context)
        # print(context[:-1])
        # context=context[:-1]+', "on,block1,block0"]'
        # print(context)
        time_str="time(0..lasttime).\n\n\n"
        initial_state_t="%INITIAL STATE\n" + agent.game_state.get_asp_str()
        initial_state=''
        for line in initial_state_t.split('\n'):
            if len(line)==0:
                continue
            if line[-1]=='.':
                if ')' in line:
                    line=line[:-2]+',0).\n'
                else:
                    line=line[:-1]+'(0).\n'
            else:
                line=line+'\n'
            initial_state+=line
        initial_state+='\n\n'
        inertial_str=agent.game_state.get_inertia_str()+'\n\n'
        action_str=agent.get_asp_action_str()
        if lasttimebase!=10:
            action_str=agent.get_asp_action_str(perfect=True)
        returnval=self.build_context_string(context,None)
        goal_str=(returnval[0])+'\n'
        context_head=goal_str.split(':-')[0].strip()
        context_head=context_head[:-2]+'lasttime)'
        goal_str+=':- {'+context_head+' : '
        vars = returnval[2]
        for v in vars:
            goal_str+=v[:-1]+'(V'+vars[v]+', lasttime), '
        for v in vars:
            base=int(vars[v])
            for i in range(base+1,len(vars)):
                goal_str+='not V'+str(base)+'=V'+str(i)+', '
        goal_str=goal_str[:-2]+'}0.\n\n'
        planning_str=time_str+initial_state+inertial_str+action_str+goal_str
        planning_str=planning_str.encode('utf-8')
        lasttime='lasttime='
        lasttime_num=lasttimebase
        answers=[]
        saved_answers=[]
        while len(answers)>0 and lasttime_num>1 or lasttime_num==lasttimebase:
            lasttime_str=lasttime+str(lasttime_num)
            saved_answers=deepcopy(answers)
            p = Popen(['clingo','-c',lasttime_str], shell=False, stdout=PIPE, stderr=STDOUT, stdin=PIPE)
            t=Timer(100,p.kill)
            try:
                t.start()
                textout, inp = p.communicate(input=planning_str)
                textout = (textout.decode('utf-8'))
                answers=self.answer_set_list_from_raw(textout)
            finally:
                t.cancel()
            if lasttimebase!=10:
                if "\nSATISFIABLE" in textout:
                    return True
                else:
                    return False
            lasttime_num-=1
        answers=saved_answers
        if len(answers)==0:
            actions=[]
        else:
            actions=[]
            actions=sorted(answers, key=lambda action: action[1].split(',')[0])
            for idx,action in enumerate(actions):
                head_t=agent.action_models[action[0]][0].head[0]
                args=self.get_seperated_atom(str(head_t))[1]
                args=args.split(',')
                args=[arg[1:] for arg in args]
                while '' in args:
                    args.remove('')
                action_args=action[1].split(',')[1:]
                new_action_args=[]
                for arg in args:
                    new_action_args.append(action_args[int(arg)])
                actions[idx]=[action[0]]+new_action_args
            # lasttime_num=len(answers)
            # for x in range(lasttime_num):
            #     actions.append(None)
            # for answer in answers:
            #     time_stmp=answer[1].split(",")[-1]
            #     action=(tuple([answer[0]]+answer[1].split(',')[:-1]))
            #     actions[int(time_stmp)]=action
        pass
        return (list(reversed(actions)))

    def clean_contexts(self):
        context_str=self.game_state.asp_str+"\n"
        remove=[]
        ctr=0
        for context in self.all_contexts:
            ctr+=1
            n_context_str=context_str+self.build_context_string(context,0)[0]
            answer_set=self.get_context_answer_set(n_context_str)
            try:
                answer_set=answer_set.split('\n')[4].split(' ')
                if 'unsafe' in answer_set:
                    remove.append(context)
            except:
                remove.append(context)
        for context in remove:
            self.all_contexts.pop(context)


    def filter_contexts(self,t_combos):
        new_combo=[]
        seen=set()
        for combos in t_combos:
            for combo in combos:
                term_list=[]
                for head in combo['heads']:
                    term=(head,combo['heads'][head]['count'])
                    term_list.append(term)
                t = (tuple(sorted(term_list)))
                if t not in seen:
                    seen.add(t)
                    new_combo.append(combo)
        return(new_combo)

    def generate_action_sets(self,agent):
        action_sets = {}
        for action in agent.all_actions:
            # print("len of all_contexts is {}".format(len(all_contexts)))
            args = get_arg_types(action.name)
            action_sets[action] = args
            obj_types = []
            for arg in args:
                obj_types.append(self.game_state.obj_types[arg])

        return action_sets

    def generateContexts(self, agent):
        '''
        Generates permutations of all possible relations up to a certain size where both the relations and the arguments
        are varied.
        '''
        all_contexts = {}

        actions_to_relations = (get_all_arg_types())

        # remove any types from being considered
        types = set([item for sublist in actions_to_relations.values() for item in sublist])
        actions_to_relations_no_types = [i for i in actions_to_relations.keys() if i not in types]
        relations = [i for i in actions_to_relations_no_types if not agent.get_action_from_str(i)]

        # generate all permutations of relations up to a certain size
        ungrounded_contexts_without_vars = []
        for i in range(1,agent.context_size+1):
            ungrounded_contexts_without_vars += list(itertools.combinations(relations, agent.context_size))

        print("ungrounded_contexts_without_vars = {}".format(len(ungrounded_contexts_without_vars)))

        total_contexts_generated = []

        for ungrounded_context in ungrounded_contexts_without_vars:
            arg_type_counts = {} # key is a type, value is the number of occurences of that type as an arg in the context
            context_arg_types_ordered = [] # a list of each arg type as it is encountered (order preservation matters)
            for term in ungrounded_context:
                # get the arg types
                term_types_ordered = get_arg_types(term)
                for type in term_types_ordered:
                    if type in arg_type_counts.keys():
                        arg_type_counts[type]+=1
                    else:
                        arg_type_counts[type]=1
                    context_arg_types_ordered.append(type)

            # generate all possible mappings
            options_per_arg = [] # list of lists where inner lists are variables for the arg of the index of the list
            # i.e. the context: agentat xcoord ycoord , cdoor xcoord ycoord , north ycoord ycoord
            # would have a list like [[0,1][0,1,2,3],[0,1],[0,1,2,3],[0,1,2,3],[0,1,2,3]] because ycoord has 4 options
            # and xcoords has 2 options

            for arg_type in context_arg_types_ordered:
                options_per_arg.append(range(arg_type_counts[arg_type]))

            # now do a permutation to get every possible arrangement
            all_permutations_of_variable_bindings = list(itertools.product(*options_per_arg))

            # now filter out ones that have all unique bindings (because we assume that all contexts will have at least
            # one related variable

            def good_permutation(p):
                types_to_vars = {}
                for i in range(len(p)):
                    var_type = context_arg_types_ordered[i]
                    var_val = p[i]
                    if var_type in types_to_vars.keys():
                        if var_val in types_to_vars[var_type]:
                            return True
                        else:
                            types_to_vars[var_type].append(var_val)
                    else:
                        types_to_vars[var_type] = [var_val]
                return False

            all_related_permutations = [p for p in all_permutations_of_variable_bindings if good_permutation(p)]

            for args in all_related_permutations:
                context_terms = []
                for term_pred in ungrounded_context:
                    term_args = get_arg_types(term_pred)
                    corresponding_args = args[0:len(term_args)]
                    term_args_vars = [str(t)+str(v) for t,v in zip(term_args,corresponding_args)]
                    context_terms.append(Term(term_pred,term_args_vars))
                    args = args[len(term_args):]  # remove for next time
                new_context =  Context(terms=context_terms)
                total_contexts_generated.append(new_context)

        print("There are {} contexts".format(len(set(total_contexts_generated))))

        action_sets = self.generate_action_sets(agent)

        for context in total_contexts_generated:

            context_args = {}
            for term in context.get_terms():
                head = term.get_pred_str()
                args = term.get_args()

                for arg in args:
                    match = re.match(r"([a-z_A-Z]+)([0-9]+)", arg, re.I)
                    ltype, num = match.groups()
                    if ltype not in context_args:
                        context_args[ltype]=[]
                    if num not in context_args[ltype]:
                        context_args[ltype].append(num)

            flag_bad_context_args = False
            for arg_type in context_args:
                if len(context_args[arg_type]) > len(self.game_state.obj_types[arg_type]):
                    flag_bad_context_args = True
                    logging.warning("Problem with context_args[{}] having more objects that it should".format(arg_type))
                    break
            if flag_bad_context_args:
                continue

            for arg_type in context_args:
                for arg in context_args[arg_type]:
                    #add_string=arg_type+","+arg_type+arg
                    context.add_term(Term(arg_type,[arg_type+arg]))
            for var in context_args:
                for idx,v in enumerate(context_args[var]):
                    for i in range(idx+1,len(context_args[var])):
                        add_string='not '+var+v+"="+var+str(context_args[var][i])
                        context.add_term(add_string)

            context_action_set = {}

            for action in action_sets:
                arg_sets = [[]]
                flag = True
                counter = 0
                for ltype in action_sets[action]:
                    try:
                        # if ltype in context_args:#remove to force all args in context
                        arg_sets = [arg_set + [ltype + arg_map] for arg_map in context_args[ltype] for arg_set in
                                    arg_sets]
                        # counter+=1
                    # else:#see if line
                    # arg_sets = [arg_set + [arg_map] for arg_map in self.game_state.obj_types[ltype] for arg_set in arg_sets]
                    except Exception as e:
                        flag = False
                        counter += 1
                        break
                #print("Counter is {}".format(counter))
                # if counter==0:
                #     print(counter)
                #     flag=False
                if not flag:
                    continue
                for arg_set in arg_sets:
                    key=tuple([str(action)]+arg_set)
                    context_action_set[key]=0
            #print("context_action_set has len {} and is \n".format(len(context_action_set)))#,context_action_set))
            if context_action_set == {}: ##### ARE ALL ARGUMENTS NECESSARY IN CONTEXT FOR ACTION TO BE TAKEN
                pass
            else:
                all_contexts[context.json_str()]=context_action_set

        print("all_contexts {}".format(len(all_contexts)))
        self.all_contexts = all_contexts


    def generateContextsOld(self,agent):#TODO:read in context file if it exists instead of generate
        #print('make')
        state=(self.game_state.state)
        #print("Here",self.context_size)
        actions_to_arg_types=(get_all_arg_types())
        arg_types=[i for i in actions_to_arg_types if not agent.get_action_from_str(i)]
        # for types in arg_types:
        #     if not agent.get_action_from_str(types):
        #         print(types)
        #         print(arg_types[types])
        #         print(type(types))
        for term in arg_types:
            head=term
            body=get_arg_types(head)
            context=head+","
            for b in body:
                context += b + ','
            context=context[:-1]
            self.contexts.add(context)
            #self.contexts.add('not '+context)### HERE WE ADD NOT'S
        num_contexts=len(self.contexts)
        self.num_contexts=num_contexts
        combo=self.combiner2([],self.context_size)#HERE IS COMBINER CHOICE
        print("Combo lengths are {}".format((len(combo[0]),len(combo[1]))))
        print("Combo 0 is\n {}")
        for x in combo[0]:
            print("\t{}".format(x))
        print("Combo 1 is\n {}")
        for x in combo[1]:
            print("\t{}".format(x))
        contexts=[]
        for c in combo:
            contexts+=self.build(c)
        #combo = self.combiner([], self.context_size)###HERE IS OLD VERSION, on down
        action_sets = {}
        for action in CrawlAIAgent.all_actions:
            action_sets[action] = 0
        all_contexts={}
        print("len of all_contexts is {}".format(len(all_contexts)))
        action_sets = {}
        num_insertions_debugging_only = 0
        num_collisions = 0
        for action in agent.all_actions:
            #print("len of all_contexts is {}".format(len(all_contexts)))
            args = get_arg_types(action.name)
            action_sets[action]=args
            obj_types = []
            for arg in args:
                obj_types.append(self.game_state.obj_types[arg])
            # arg_sets = [[]]
            # for mapping in obj_types:
            #     arg_sets = [arg_set + [arg_map] for arg_map in mapping for arg_set in arg_sets]
            # for arg_set in arg_sets:
            #     key = tuple([str(action)] + arg_set)
            #     action_sets[key] = 0
        print("Deterministic until here! Currently there are {} contexts".format(len(contexts)))

        # sort each context
        contexts = [sorted(c, key=str) for c in contexts]
        contexts_as_strs = sorted(list([str(c) for c in set(sorted([str(t) for t in contexts], key=str))]), key=str)
        print("There are {} unique contexts".format(len(contexts_as_strs)))

        # with open("pre_contexts_sorted_{}".format(random.choice(range(100))), 'w') as f:
        #     f.write("Pre contexts:\n")
        #     for pre_context in contexts_as_strs:
        #         f.write(str(pre_context)+'\n')

        for l in contexts:
            l=list(l)
            #for l in c['lists']:
            context_args={}
            for pred in l:
                args = pred.split(',')[1:]
                for arg in args:
                    match = re.match(r"([a-z_A-Z]+)([0-9]+)", arg, re.I)
                    ltype, num = match.groups()
                    if ltype not in context_args:
                        context_args[ltype]=[]
                    if num not in context_args[ltype]:
                        context_args[ltype].append(num)
            flag=False
            for arg_type in context_args:
                if len(context_args[arg_type])>len(self.game_state.obj_types[arg_type]):
                    flag=True
                    break
            if flag:
                continue

            for arg_type in context_args:
                for arg in context_args[arg_type]:
                    add_string=arg_type+","+arg_type+arg
                    l.append(add_string)
            for var in context_args:
                for idx,v in enumerate(context_args[var]):
                    for i in range(idx+1,len(context_args[var])):
                        add_string='not '+var+v+"="+var+str(context_args[var][i])
                        l.append(add_string)
            context_action_set={}
            #print("There are {} action_sets for context {}".format(len(action_sets),l))
            for action in action_sets:
                arg_sets=[[]]
                flag=True
                counter=0
                for ltype in action_sets[action]:
                    try:
                        #if ltype in context_args:#remove to force all args in context
                        arg_sets = [arg_set + [ltype+arg_map] for arg_map in context_args[ltype] for arg_set in arg_sets]
                            #counter+=1
                       # else:#see if line
                            #arg_sets = [arg_set + [arg_map] for arg_map in self.game_state.obj_types[ltype] for arg_set in arg_sets]
                    except Exception as e:
                        flag=False
                        counter+=1
                        break
                #print("Counter is {}".format(counter))
                # if counter==0:
                #     print(counter)
                #     flag=False
                if not flag:
                    continue
                for arg_set in arg_sets:
                    key=tuple([str(action)]+arg_set)
                    context_action_set[key]=0
            #print("context_action_set has len {} and is \n".format(len(context_action_set)))#,context_action_set))
            if context_action_set == {}: ##### ARE ALL ARGUMENTS NECESSARY IN CONTEXT FOR ACTION TO BE TAKEN
                pass
            else:
                num_insertions_debugging_only += 1
                context_str = json.dumps(l)
                if context_str in all_contexts.keys():
                    num_collisions +=1
                    if len(context_action_set) != len(all_contexts[context_str]):
                        print("We have a collision, replacing context_action_set of len {} with one of length {}".format(len(all_contexts[context_str]),len(context_action_set)))
                    if set(context_action_set) != set(all_contexts[context_str]):
                        print("context_action_sets are not equal under set comparison")
                    if context_action_set != all_contexts[context_str]:
                        print("context_action_sets are not equal under direct comparison")
                    if str(context_action_set) != str(all_contexts[context_str]):
                        print("context_action_sets are not equal under str comparison")

                all_contexts[json.dumps(l)]=context_action_set
                #print("len of all_contexts is {}".format(len(all_contexts)))
        print("num insertions = {}".format(num_insertions_debugging_only))
        print("num collisions = {}".format(num_collisions))
        # new_contexts = {}
        # l = len(all_contexts)
        # sets={}
        # for c in all_contexts:
        #     print(l)
        #     bt=BaseThread(name=str(l),target=self.context_as_goal,args=(c,agent,20,))
        #     sets[c]=bt
        #     bt.start()
        #     l -= 1
        #     #tmp_set = (self.context_as_goal(c, agent, lasttimebase=15))
        # for c in sets:
        #     retval=sets[c].get_return()
        #     if retval:
        #         new_contexts[c]=all_contexts[c]
        # all_contexts=new_contexts
        print("all_contexts {}".format(len(all_contexts)))
        set_len_all_contexts = len(set(all_contexts.keys()))
        print("len(set(all_contexts.keys())) = {}".format(len(set(all_contexts.keys()))))

        # with open("all_contexts_when_{}".format(set_len_all_contexts), 'w') as f:
        #     f.write("Pre contexts:\n")
        #     for pre_context in contexts_as_strs:
        #         f.write(str(pre_context)+'\n')
        #     f.write("\nPost contexts:\n")
        #     for context in sorted(all_contexts.keys(), key=str):
        #         f.write(str(context)+'\n')

        self.all_contexts=all_contexts
        # write these to a file
        #with open("all_contexts_output.txt", 'w') as f:
        #    for c in self.all_contexts:
        #        f.write(c + '\n')
        # now wait for user input
        #input("Just wrote out all contexts. Press enter to continue")
        return

    def valid_cb(self,c,bool):
        if bool:
            self.all_contexts_list.append(c)

    def build(self,combo):
        contexts=[]
        for c in combo:
            li=[]
            count_map={}
            for arg in c['args']:
                count_map[arg]=[]
            c=(c['heads'])
            for pred in c:
                map = {}
                arg_types = deepcopy(get_arg_types(pred))
                seen=[]
                for idx, arg in enumerate(arg_types):
                    if arg not in map:
                        map[arg] = 0
                    arg_types[idx] += str(map[arg])
                    map[arg] += 1
                for i in range(c[pred]['count']):
                    li.append(([pred] + arg_types))
                for i in range(c[pred]['count']):
                    for arg in c[pred]['args']:
                        count_map[arg].append(c[pred]['args'][arg])
                        seen.append(arg)
                    for arg in list(set(count_map.keys())-set(seen)):
                        count_map[arg].append(0)
            for arg in count_map:
                count_map[arg]=permute_position_unique(count_map[arg])
            li=[li]
            new_li=[]
            regex=re.compile(r"([a-z_A-Z]+)([0-9]+)")
            for arg_type in count_map:
                if not new_li == []:
                    li=deepcopy(new_li)
                    new_li=[]
                for permute in count_map[arg_type]:
                    for context in li:
                        context=deepcopy(context)
                        for idx,term in enumerate(context):
                            for idy,arg in enumerate(term[1:]):
                                match=re.match(regex,arg)
                                term_arg_type,num=match.groups()
                                num=int(num)
                                if arg_type==term_arg_type:
                                    term[idy+1]=term_arg_type+str(permute[idx][num])
                        new_li.append(context)
            if count_map=={}:
                for l in li:
                    new_li.append(l)
            for li in new_li:
                if valid(li):
                    contexts.append(tuple(li))
        new_contexts=[]
        for context in contexts:
            new_context=[]
            for c in context:
                s=""
                for term in c:
                    s+=term+","
                new_context.append(s[:-1])
            new_contexts.append(new_context)
        return new_contexts

    def combiner2(self,combos,counter):
        t_combos=[]
        if counter>1:
            t_combos=self.combiner2(deepcopy(combos),counter-1)
            combos=deepcopy(t_combos[counter-2])
        new_combos=[]
        if combos==[]:#First iterations, create base_case
            for c in self.contexts:
                base={}
                terms=c.split(',')

                print("self.game_state.obj_types = {}".format(self.game_state.obj_types))
                if terms[0] in self.game_state.obj_types:
                    continue
                head=terms[0]
                body=terms[1:]
                base['heads'] = {}
                base['heads'][head] = {'count': 1, 'args': {}}
                base['args'] = {}
                for b in body:
                    if b not in base['heads'][head]['args']:
                        base['heads'][head]['args'][b]=0
                    base['heads'][head]['args'][b]=body.count(b)
                    if b not in base['args']:
                        base['args'][b]={}
                        base['args'][b]['count']=0
                        base['args'][b]['in']=set()
                    base['args'][b]['count']+=1
                    base['args'][b]['in'].add(head)
                new_combos.append(base)
        else:
            for c in self.contexts:
                terms = c.split(',')
                if terms[0] in self.game_state.obj_types:
                    continue
                head = terms[0]
                body = terms[1:]
                crt2 = 0
                for combo in combos:
                    # print(counter, crt,crt2)
                    crt2 += 1
                    c = deepcopy(combo)
                    if head not in c['heads']:
                        c['heads'][head] = {'count': 0, 'args': {}}
                    c['heads'][head]['count'] += 1
                    count = c['heads'][head]['count']
                    for b in body:
                        c['heads'][head]['args'][b] = body.count(b)
                    #permute = set()
                    for b in body:
                        if b not in c['args']:
                            c['args'][b] = {'count': 0, 'in': set()}
                        c['args'][b]['in'].add(head)
                        arg_count = count * c['heads'][head]['args'][b]
                        if c['args'][b]['count'] < (arg_count):
                            c['args'][b]['count'] = arg_count  # addition to total count of this arg, need to permute.
                            #permute.add(b)
                    new_combos.append(c)
        return t_combos+[new_combos]

    def combiner(self,combos,counter):
        if counter>1:
            combos=self.combiner(combos,counter-1)
        new_combos=[]
        if combos==[]:
            for c in self.contexts:
                base={}
                terms=c.split(',')
                if terms[0] in self.game_state.obj_types:
                    continue
                head=terms[0]
                body=terms[1:]
                base['heads'] = {}
                base['heads'][head] = {'count': 1, 'args': {}}
                base['args'] = {}
                for b in body:
                    if b not in base['heads'][head]['args']:
                        base['heads'][head]['args'][b]=0
                    base['heads'][head]['args'][b]=body.count(b)
                    if b not in base['args']:
                        base['args'][b]={}
                        base['args'][b]['count']=0
                        base['args'][b]['in']=set()
                    base['args'][b]['count']+=1
                    base['args'][b]['in'].add(head)
                strings=[head+',']
                for b in body:
                    new_strings = []
                    for i in range(0,base['args'][b]['count']):
                        new_b = b + str(i)
                        for s in strings:
                            if new_b in s:
                                continue
                            new_strings.append(s+new_b+',')
                    strings=new_strings
                base['lists']=[]
                for s in strings:
                    base['lists'].append([s[:-1]])
                new_combos.append(base)
            for c in self.contexts:
                if not self.include_nots:
                    break
                base={}
                terms=c.split(',')
                if terms[0] in self.game_state.obj_types:
                    continue
                head="not "+ terms[0]
                body=terms[1:]
                base['heads']={}
                base['heads'][head]={'count':1,'args':{}}
                base['args']={}
                for b in body:
                    if b not in base['heads'][head]['args']:
                        base['heads'][head]['args'][b]=0
                    base['heads'][head]['args'][b]=body.count(b)
                    if b not in base['args']:
                        base['args'][b]={}
                        base['args'][b]['count']=0
                        base['args'][b]['in']=set()
                    base['args'][b]['count']+=1
                    base['args'][b]['in'].add(head)
                strings=[head+',']
                for b in body:
                    new_strings = []
                    for i in range(0,base['args'][b]['count']):
                        new_b = b + str(i)
                        for s in strings:
                            if new_b in s:
                                continue
                            new_strings.append(s+new_b+',')
                    strings=new_strings
                base['lists']=[]
                for s in strings:
                    base['lists'].append([s[:-1]])
                new_combos.append(base)
        else:
            crt=0
            for con in self.contexts:
                crt+=1
                terms=con.split(',')
                if terms[0] in self.game_state.obj_types:
                    continue
                head=terms[0]
                body=terms[1:]
                crt2=0
                for combo in combos:
                    #print(counter, crt,crt2)
                    crt2+=1
                    c=deepcopy(combo)
                    if head not in c['heads']:
                        c['heads'][head]={'count':0,'args':{}}
                    c['heads'][head]['count']+=1
                    count=c['heads'][head]['count']
                    for b in body:
                        c['heads'][head]['args'][b] = body.count(b)
                    permute=set()
                    for b in body:
                        if b not in c['args']:
                            c['args'][b] = {'count': 0, 'in': set()}
                        c['args'][b]['in'].add(head)
                        arg_count = count * c['heads'][head]['args'][b]
                        if c['args'][b]['count'] < (arg_count):
                            c['args'][b]['count'] = arg_count  # addition to total count of this arg, need to permute.
                            permute.add(b)
                    strings = [head + ',']
                    for b in body:
                        new_strings = []
                        for i in range(0, c['args'][b]['count']):
                            new_b = b + str(i)
                            for s in strings:
                                if new_b in s:
                                    continue
                                new_strings.append(s + new_b + ',')
                        strings = new_strings
                    lists=c['lists']
                    new_list=[]
                    for l in lists:
                        for s in strings:
                            new_list.append(l+[s[:-1]])
                    for i in range(0,len(new_list)):
                        if len(new_list[i])!=len(set(new_list[i])):
                            new_list[i]=[]
                        else:
                            new_list[i]=sorted(new_list[i])
                    while [] in new_list:
                        new_list.remove([])
                    # print(combo)#TODO: Permute when adding higher order args
                    for idx,list in enumerate(new_list):
                        length=len(list)-1
                        arg_set=set()
                        for idy,atom in enumerate(list):
                            if idy==length:
                                flag=True
                                if len(atom.split(",")[1:])<=1:
                                    flag=False
                                else:
                                    for arg in atom.split(",")[1:]:
                                        if arg in arg_set:
                                            flag=False
                                if flag:
                                    new_list[idx]=[]
                            else:
                                for arg in atom.split(",")[1:]:
                                    arg_set.add(arg)
                    while [] in new_list:
                        new_list.remove([])
                    c['lists']=new_list
                    new_combos.append(c)
            for con in self.contexts:
                if not self.include_nots:
                    break
                crt+=1
                terms=con.split(',')
                if terms[0] in self.game_state.obj_types:
                    continue
                head="not "+terms[0]
                body=terms[1:]
                crt2=0
                for combo in combos:
                    #print(counter, crt,crt2)
                    crt2+=1
                    c=deepcopy(combo)
                    if head not in c['heads']:
                        c['heads'][head]={'count':0,'args':{}}
                    c['heads'][head]['count']+=1
                    count=c['heads'][head]['count']
                    for b in body:
                        c['heads'][head]['args'][b] = body.count(b)
                    permute=set()
                    for b in body:
                        if b not in c['args']:
                            c['args'][b] = {'count': 0, 'in': set()}
                        c['args'][b]['in'].add(head)
                        arg_count = count * c['heads'][head]['args'][b]
                        if c['args'][b]['count'] < (arg_count):
                            c['args'][b]['count'] = arg_count  # addition to total count of this arg, need to permute.
                            permute.add(b)
                    strings = [head + ',']
                    for b in body:
                        new_strings = []
                        for i in range(0, c['args'][b]['count']):
                            new_b = b + str(i)
                            for s in strings:
                                if new_b in s:
                                    continue
                                new_strings.append(s + new_b + ',')
                        strings = new_strings
                    lists=c['lists']
                    new_list=[]
                    for l in lists:
                        for s in strings:
                            new_list.append(l+[s[:-1]])
                    for i in range(0,len(new_list)):
                        if len(new_list[i])!=len(set(new_list[i])):
                            new_list[i]=[]
                        else:
                            new_list[i]=sorted(new_list[i])
                    while [] in new_list:
                        new_list.remove([])
                    #print(permute)#TODO: Permute when adding higher order args
                    for idx,list in enumerate(new_list):
                        length=len(list)-1
                        arg_set=set()
                        for idy,atom in enumerate(list):
                            if idy==length:
                                flag=True
                                for arg in atom.split(",")[1:]:
                                    if arg in arg_set:
                                        flag=False
                                if flag:
                                    new_list[idx]=[]
                            else:
                                for arg in atom.split(",")[1:]:
                                    arg_set.add(arg)
                    while [] in new_list:
                        new_list.remove([])
                    c['lists']=new_list
                    new_combos.append(c)
        return new_combos
if __name__ == "__main__":
    cg = ContextGenerator()
    lh = None
    if len(sys.argv) > 1:
        lh = LogicHandler()
        lh.load_state( sys.argv[1] )

    cg.generateContexts(6,'context6.txt')
    #allPossiblities = cg.load_contexts('context3.txt')
    #if lh is not None:
    #    allPossiblities = cg.filterByState(lh,allPossiblities)
    #cg.save_contexts(allPossiblities,'context3_load.txt')


