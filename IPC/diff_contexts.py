

file1 = 'pre_contexts_sorted_97'
file2 = 'pre_contexts_sorted_3'
with open(file1, 'r') as f1:
    with open(file2, 'r') as f2:
        f1_lines = f1.readlines()
        f2_lines = f2.readlines()
        for f1_line in f1_lines:
            if f1_line not in f2_lines:
                print("In F1, NOT in F2: {}".format(f1_line))

        for f2_line in f2_lines:
            if f2_line not in f1_lines:
                print("In F2, NOT in F1: {}".format(f2_line))



