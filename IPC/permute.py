import itertools
import copy
import time

def filter(result,len_):
    s=set()
    for l in result:
        if not len(l)==len_:
            continue
        mapping={}
        count=0
        new_list=[]
        for idx,num in enumerate(l):
            if num not in mapping:
                mapping[num]=count
                count+=1
            new_list.append(mapping[num])
        s.add(tuple(new_list))
    li=(list(s))
    if () in li:
        li.remove(())
    return [list(x) for x in li]

def product(*args, **kwds):
    # product('ABCD', 'xy') --> Ax Ay Bx By Cx Cy Dx Dy
    # product(range(2), repeat=3) --> 000 001 010 011 100 101 110 111
    m=list(map(tuple,args))
    k=kwds.get('repeat',1)
    pools=m*k
    result=[[]]
    for idx,pool in enumerate(pools):
        new_result=copy.deepcopy(result)
        for y in pool:
            for x in new_result:
                result+=[x+[y]]
        result=filter(result,idx+1)
    for prod in result:
        yield tuple(prod)

def permute_position_unique(n):
    count=0
    save=copy.deepcopy(n)
    for num in n:#NEED TO PERMUTE FULL SET< THEN FILTER
        count+=num
    n=count
    base=[]
    for i in range(n):
        base.append(i)
    li=list(product(base,repeat=n))
    send=[[]]
    count=0
    for s in save:
        l=[]
        for i in range(count,s+count):
            l.append(i)
        send.append(l)
        count+=s
    send.remove([])
    new_lists=[]
    for l in li:
        new_send=copy.deepcopy(send)
        for se in new_send:
            for idx,s in enumerate(se):
                se[idx]=l[s]
        #if valid(new_send):
        new_lists.append(new_send)
    return new_lists

def valid(li):
    seen=set()
    for idx,l in enumerate(li):
        tmp=tuple(l)
        if tmp in seen:
            return False
        else:
            seen.add(tmp)
        flag=False
        tried=False
        for n in set(l[1:]):
            for idy,l2 in enumerate(li):
               if idx==idy:
                   continue
               for n2 in set(l2[1:]):
                   tried=True
                   if n==n2:
                       flag=True
                       break
               if flag:
                   break
            if flag:
                break
        if not tried:
            continue
        if not flag:
            if len(l)>2:#### WE LET SINGLE ARG 'FACTS' THROUGH
                return False
    return True
