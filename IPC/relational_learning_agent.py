'''
An agent that takes users commands to move, and records interactions <s_i, a, s_i+1> to then be used by a transition
model learner.
'''
import gamestate
import random
import re, os, sys
import agent
from copy import deepcopy
import subprocess
from time import time, sleep
import datetime
import itertools
import logging
import matplotlib.pyplot as plt
import numpy as np
from pathlib import Path
from term import Term, get_arg_types, add_to_arg_types, get_all_arg_types
from action_preconditions_rule import ActionPreconditionsRule, permute_based_asp_distance
from inspect import getfullargspec
from temp_simple_planning_agent import SimplePlanningAgent
from threads import BaseThread, get_active_count, cb
import copy
import json


class RelationalLearningAgent(agent.CrawlAIAgent):

    def __init__(self, action_type_str='random', context_size=4, domprob=None, plan='No', run_num=None,
                 action_limit=10000, expn=-1):

        self.expn = expn  # this is the experiment identifier
        self.update = set()
        self.learned_new_models = False
        self.run_num = run_num
        self.name = ""
        self.score_file = "scores.txt"
        self.learning_sets = {}
        if plan == 'Yes':
            self.do_planning = True
        else:
            self.do_planning = False
        self.context_size = context_size
        self.domprob = domprob
        self.game_state = gamestate.GameState(self.domprob)
        self.perfect_action_models = {}
        self.all_actions = []
        self.all_actions_by_name = {}
        self.initiate_actions()
        self.counts_by_action = {}
        self.actions_between_plan_learning = 5
        for action in self.perfect_action_models:
            self.counts_by_action[action] = [-1 * self.actions_between_plan_learning, 0]
        self.perform_learning = True
        self.ask_before_learning = True
        self.actions_between_learning = 1
        self.last_learned = -10
        self.total_actions_in_experiment = action_limit
        self.previous_game_state = None
        self.positive_states_per_action = []
        self.negative_state_per_action = []
        self.action_history = []
        self.game_state_history = []
        self.score = 0
        self.planning_agent = SimplePlanningAgent("../ff", self.game_state, self, context_size)
        self.goal_context = None
        self.data_filename = None
        self.data = {'scores': [], 'actions': []}

        self.data_already_saved = False
        self.finished = False
        self.available_moves = set(self.all_actions)

        self._next_action_func = self._next_action_random  # default to random

        self.action_selection_type_str = action_type_str

        self.action_selection_type_str = action_type_str
        if self.action_selection_type_str:
            if self.action_selection_type_str == 'random':
                self._next_action_func = self._next_action_random
            elif self.action_selection_type_str == 'explore':
                self._next_action_func = self._next_action_exploratory
            elif self.action_selection_type_str == 'human':
                self._next_action_func = self._next_action_human_input
            else:
                logging.warning(
                    "Attempt to set unknown next_action() function: available types are: {},{},{}".format('random',
                                                                                                          'explore',
                                                                                                          'human'))

        self.interaction_history = {}  # keys are actions,
        #  values are lists of tuples, where tuple(0) is the previous game_state
        # and tuple(1) is the post game_state

        self.action_counter = 0

        for mv in self.available_moves:
            self.interaction_history[str(mv)] = []

        self.learning_file_number = 1

        # key is action, val is list of numbers corresponding to
        # the number of examples - basically this builds up a list of
        # when ilasp was called recording the number of total examples at that time
        # for example, the first time the 'west' action has 20 examples, call ilasp, then at 40, etc
        self.calls_to_ilasp_per_action = {}

        self.action_models = {}  # key is action, model is string

        self.pos_dict = {}

        self.action_set_length = 0  # Used for debugging
        self.action_set_score = 0  # Used for debugging

        # self.perfect_action_models = {
        #     'east'      : ActionPreconditionsRule('east(V0) :- agentat(V0), at(V0,V1,V2), at(V3,V4,V2), not wall(V3), V1 = V4-1.'), # subeqone(v1,v4)
        #     'west'      : ActionPreconditionsRule('west(V3) :- agentat(V3), at(V0,V1,V2), at(V3,V4,V2), not wall(V0), V1 = V4-1.'),
        #     'north'     : ActionPreconditionsRule('north(V3) :- agentat(V3), at(V0,V1,V2), at(V3,V1,V4), not wall(V0), V2 = V4-1.'),
        #     'south'     : ActionPreconditionsRule('south(V0) :- agentat(V0), at(V0,V1,V2), at(V3,V1,V4), not wall(V3), V2 = V4-1.'),
        #     'southeast' : ActionPreconditionsRule('southeast(V3) :- agentat(V3), at(V0,V1,V2), at(V3,V4,V5), not wall(V0), V4 = V1-1, V5 = V2-1.'),
        #     'northwest' : ActionPreconditionsRule('northwest(V0) :- agentat(V0), at(V0,V1,V2), at(V3,V4,V5), not wall(V3), V4 = V1-1, V5 = V2-1.'),
        #     'northeast' : ActionPreconditionsRule('northeast(V3) :- agentat(V3), at(V0,V1,V2), at(V3,V4,V5), not wall(V0), V4 = V1-1, V2 = V5-1.'),
        #     'southwest' : ActionPreconditionsRule('southwest(V0) :- agentat(V0), at(V0,V1,V2), at(V3,V4,V5), not wall(V3), V4 = V1-1, V2 = V5-1.'),
        #     'get'       : ActionPreconditionsRule('get(V0) :- agentat(V0).'),
        #     'open_door' : ActionPreconditionsRule('open_door(V3) :- at(V0,V1,V2), at(V3,V4,V2), closed_door(V0), V1 = V4-1.'),
        #     'close_door': ActionPreconditionsRule('close_door(V3) :- at(V0,V1,V2), at(V3,V4,V2), opened_door(V0), V1 = V4-1.'),
        #
        # }
        # 'east': [ActionPreconditionsRule('east(V0) :- at(V0,V1,V2), at(V3,V4,V2), not wall(V3), not closed_door(V3), V1 = V4-1.')]
        # }
        # self.action_models=copy.deepcopy(self.perfect_action_models)
        self.successes = []
        self.did_learning = False

        # For exploration agent
        st = time()
        self.pddl_writer = self.planning_agent.pddlWriter
        # self.lh = LogicHandler()
        # self.cg = ContextGenerator()

        self.curr_possiblities = []
        # self.pddl_writer.set_logic_handler(self.lh)
        self.context_counts = {a: [{}, 0] for a in self.available_moves}

        self.plan_action_queue = []

        self.anySeen = False
        self.last_action = None

        self.ready_to_delete = False
        self.active_contexts = []

        self.prev_actions_tried = 0  # number of previous actions tried, to know if we recently
        self.actions_tried = 0  # number of actions tried, where the args were appropriate

        self.goal_contexts_tried_this_state = [] # resets when the agent changes states
        self.no_more_goal_contexts_this_state = False # set to true when goal contexts have been exhausted

        self.remaining_actions_for_current_state = {}  # key is gamestate hash, value is list of random actions left to try
        self.all_possible_grounded_actions = None

        # populate with starting state here
        local_all_possible_grounded_actions = self._get_all_possible_grounded_actions()
        random.shuffle(local_all_possible_grounded_actions)
        self.remaining_actions_for_current_state[self.game_state.state_hash] = local_all_possible_grounded_actions

        # data directory filenames
        self.start_timestamp = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        self.domain_ilasp_data_dir_str = '{0}_ilasp_data/expn_{1}_actiontype_{2}_run_{3}_csize_{4}_ilasp_data_{5}/'.format(
            self.domprob.domain.name, self.expn, self.action_selection_type_str, self.run_num, self.context_size, self.start_timestamp)
        self.general_ilasp_data_dir_str = 'ilasp_data/expn_{0}_actiontype_{1}_ilasp_data_run_{2}_csize_{3}_{4}/'.format(self.expn,
                                                                                                              self.action_selection_type_str,
                                                                                                              self.run_num, self.context_size,
                                                                                                              self.start_timestamp)
        self.general_asp_data_dir_str = 'asp_data/expn_{0}_actiontype_{1}_asp_data_run_{2}_csize_{3}_{4}/'.format(self.expn,
                                                                                                        self.action_selection_type_str,
                                                                                                        self.run_num,
                                                                                                                  self.context_size,
                                                                                                        self.start_timestamp)

        # Now create these directories
        os.mkdir(os.getcwd() + '/' + self.domain_ilasp_data_dir_str)
        os.mkdir(os.getcwd() + '/' + self.general_ilasp_data_dir_str)
        os.mkdir(os.getcwd() + '/' + self.general_asp_data_dir_str)
        print("Created data directories:\n\t{}\n\t{}\n\t{}".format(self.domain_ilasp_data_dir_str,
                                                                   self.general_ilasp_data_dir_str,
                                                                   self.general_asp_data_dir_str))
        # for exploratory agent
        # self.exploratory_agent = ExploratoryPlanningAgent()

    # def save_figure_context_data(self, file_name):
    #     max_contexts = 0
    #     for action,context_data in self.context_counts.items():
    #         num_contexts = len(context_data[0])
    #         if num_contexts > max_contexts:
    #             max_contexts = num_contexts
    #
    #     fig, ax = plt.subplots()
    #     ind = np.arange(len(self.context_counts))
    #     width = .1
    #     offset = 0
    #
    #     for action,context_data in self.context_counts.items():
    #         rects1 = ax.bar(offset + ind * width, tuple(context_data[0].values()), width, label=action)
    #         offset += width * max_contexts
    #
    #     ax.legend()
    #     plt.savefig(file_name)

    def generate_asp_action_str(self, perfect):
        t_str = ", time(T), T<lasttime.\n"
        preconditions = '%PRECONDITIONS\n'
        effects = '%EFFECTS\n'
        restrict = '%RESCTRICT TO ONE ACTION PER TURN\n1{no_op(T) ; '
        show = '%SHOW ONLY ACTIONS IN SOLUTION\n#show.\n'
        action_model = self.action_models
        if perfect:
            action_model = self.perfect_action_models
        for model in action_model:
            if (len(action_model[model])) == 0:
                continue
            rule = action_model[model][0]
            arg_set = {}
            for b in rule.body:
                types = get_arg_types(b.pred_str)
                for idx, arg in enumerate(b.args):
                    arg_set[arg] = types[idx]
            arg_set["T"] = None
            arg_set_t = sorted(arg_set)
            rule_str = rule.head[0].pred_str + "("
            for arg in (arg_set_t):
                rule_str += arg + ","
            rule_str = rule_str[:-1] + ')'
            show += '#show ' + str(rule.head[0].pred_str) + '/' + (str(len(arg_set))) + '.\n'
            pre_start = ':- ' + rule_str + ', '
            restrict += rule_str + ': '
            for arg in (arg_set):
                if arg == "T":
                    continue
                restrict += arg_set[arg] + '(' + arg + ',T), '
            restrict = restrict[:-2] + ' ; '
            for b in rule.body:
                if 'not ' == str(b)[:4]:
                    if len(b.args) > 0:
                        pre = pre_start + str(b)[4:-1] + ',T)'
                    else:
                        pre = pre_start + str(b)[4:] + '(T)'
                else:
                    if len(b.args) > 0:
                        pre = pre_start + "not " + str(b)[:-1] + ',T)'
                    else:
                        pre = pre_start + "not " + str(b) + '(T)'
                pre += t_str
                preconditions += pre
            for e in rule.effects:
                flag = len(e.args) > 0
                e = str(e)
                if 'not ' == e[:4]:
                    if flag:
                        e = '-' + e[4:-1] + ',T+1)'
                    else:
                        e = '-' + e[4:] + '(T+1)'
                else:
                    if flag:
                        e = e[:-1] + ',T+1)'
                    else:
                        e = e + '(T+1)'
                eff = e + ' :- ' + rule_str + t_str
                effects += eff
        restrict = restrict[:-2] + '}1 :-' + t_str[1:]
        actions = preconditions + '\n\n' + effects + '\n\n' + restrict + '\n\n' + show + '\n\n'
        return actions

    def get_asp_action_str(self, perfect=False):
        return self.generate_asp_action_str(perfect)

    def save_figure_context_data(self, file_name, graph_dir, info_dir):
        total_context_counts = {}
        for action, context_data in self.context_counts.items():
            for context, count in context_data[0].items():
                if context not in total_context_counts:
                    total_context_counts[context] = count
                else:
                    total_context_counts[context] += count

        fig, ax = plt.subplots()
        ind = np.arange(len(total_context_counts))
        width = .175
        padded_width = .2

        fig.set_size_inches(18, 6)

        graph_path = Path(graph_dir)
        info_path = Path(info_dir)

        if not graph_path.exists():
            graph_path.mkdir()

        if not info_path.exists():
            info_path.mkdir()

        graph_path /= file_name + '.png'
        info_path /= file_name + '.txt'

        with info_path.open('w') as f:
            f.write('ID,context,count\n')
            i = 0
            for context, count in total_context_counts.items():
                i += 1
                f.write('%d,%s,%d\n' % (i, context, count))

        rects1 = ax.bar(ind * padded_width, tuple(total_context_counts.values()), width, linewidth=2)

        for rect in rects1:
            height = rect.get_height()
            ax.text(rect.get_x() + rect.get_width() / 2., .95 * height,
                    '%d' % int(height),
                    ha='center', va='top', color='white', fontsize=6)

        ax.set_ylabel('Number of Actions')
        ax.set_title('Actions Taken Per Context')

        ax.set_xticks(ind * padded_width)
        ax.set_xticklabels(range(1, len(ind) + 1), minor=False, fontsize=6)

        fig = plt.gcf()
        fig.set_size_inches(20, 11)
        plt.savefig(str(graph_path))
        plt.close()

    def initiate_actions(self):
        domprob = self.domprob
        global arg_type_dict

        for o in domprob.domain.operators:
            o.variable_list = {}
            counter = 0
            variables = {}
            arg_types = []
            head_pred_args = []
            for var in o.params:
                arg_types.append(var.type)
                var = var.name
                if var not in variables:
                    variables[var] = "V" + str(counter)
                    head_pred_args.append("V" + str(counter))
                    counter += 1
            for var in variables:
                o.variable_list[variables[var]] = var
            action = agent.Action(o.name, {}, arg_types)
            head_pred = Term(str(action), head_pred_args)
            body_preds = []
            for var in o.precond:
                for v in var.predicate.args:
                    if v not in variables:
                        variables[v] = "V" + str(counter)
                        counter += 1
            for var in variables:
                o.variable_list[variables[var]] = var
            for atom in o.precond:
                t = str(atom)
                for key, val in o.variable_list.items():
                    val = '\\' + val
                    try:
                        t = re.sub(val, key, t)
                    except Exception as e:
                        print(e)
                        pass
                body_preds.append(Term(t))
            ilasp_str = str(head_pred) + " :- "
            for pred in body_preds:
                ilasp_str += str(pred) + ", "
            ilasp_str = ilasp_str[:-2]
            apr = ActionPreconditionsRule(ilasp_str)
            effects = []
            for atom in o.effects:
                t = str(atom)
                for key, val in o.variable_list.items():
                    val = '\\' + val
                    try:
                        t = re.sub(val, key, t)
                    except Exception as e:
                        print(e)
                        pass
                effects.append(Term(t))
            apr.effects = effects
            if action not in self.perfect_action_models:
                self.perfect_action_models[action] = []
            self.perfect_action_models[action].append(apr)
            action.msg = {"apr": self.perfect_action_models[action], "name": action.name}
            action.apr = self.perfect_action_models[action]
            self.all_actions.append(action)

        # for op in domprob.operators():
        #     o = domprob.domain.operators[op]
        #     o.operator_name=re.sub("-","_",o.operator_name)
        #     action=agent.Action(o.name,{},list(o.variable_list.values()))
        #     self.all_actions.append(action)
        #     variables={}
        #     counter=0
        #     for var in o.params:
        #         if var not in variables:
        #             variables[var]="V"+str(counter)
        #             counter+=1
        #     for var in variables:
        #         o.variable_list[variables[var]]=o.variable_list.pop(var)
        #         for key,val in vars(o).items():
        #             if key=='operator_name' or key=='variable_list':
        #                 continue
        #             for atom in (val):
        #                 for idx, pred in enumerate(atom.predicate):
        #                     if pred == var:
        #                         atom.predicate[idx] = variables[var]
        #
        #     def reset_to_ground(l, map):
        #         for idx, val in enumerate(l):
        #             try:
        #                 l[idx] = map[val]
        #             except:
        #                 pass
        #         return l
        #     add_to_arg_types(str(action),reset_to_ground(list(o.variable_list.keys()),o.variable_list))
        #     head_pred=Term(str(action),list(o.variable_list.keys()))
        #     body_preds = []
        #     for atom in o.precondition_pos:
        #         for i in range(len(atom.predicate)):
        #             atom.predicate[i] = re.sub("-", "_", atom.predicate[i])
        #         add_to_arg_types(atom.predicate[0], reset_to_ground(atom.predicate[1:], o.variable_list))
        #         body_preds.append(Term(atom.predicate[0], atom.predicate[1:]))
        #     for atom in o.precondition_neg:
        #         for i in range(len(atom.predicate)):
        #             atom.predicate[i] = re.sub("-", "_", atom.predicate[i])
        #         add_to_arg_types(atom.predicate[0], reset_to_ground(atom.predicate[1:], o.variable_list))
        #         body_preds.append(Term("not " + atom.predicate[0], atom.predicate[1:]))
        #     ilasp_str = str(head_pred) + " :- "
        #     for pred in body_preds:
        #         ilasp_str += str(pred) + ", "
        #     ilasp_str = ilasp_str[:-2]
        #     apr = ActionPreconditionsRule(ilasp_str)
        #     effects = []
        #     for atom in o.effect_pos:
        #         for i in range(len(atom.predicate)):
        #             atom.predicate[i] = re.sub("-", "_", atom.predicate[i])
        #         add_to_arg_types(atom.predicate[0], reset_to_ground(atom.predicate[1:], o.variable_list))
        #         effects.append(Term(atom.predicate[0], atom.predicate[1:]))
        #     for atom in o.effect_neg:
        #         for i in range(len(atom.predicate)):
        #             atom.predicate[i] = re.sub("-", "_", atom.predicate[i])
        #         add_to_arg_types(atom.predicate[0], reset_to_ground(atom.predicate[1:], o.variable_list))
        #         effects.append(Term("not " + atom.predicate[0], atom.predicate[1:]))
        #     apr.effects=effects
        # if action not in self.perfect_action_models:
        #     self.perfect_action_models[action] = []
        # self.perfect_action_models[action].append(apr)
        # action.msg = {"apr": self.perfect_action_models[action], "name": action.name}
        # action.apr = self.perfect_action_models[action]

    def set_data_filename(self, f_str):
        # So we can save data anytime
        self.data_filename = f_str

    def add_server_message(self, json_msg: {}):
        st = time()
        if "msgs" in json_msg:
            self.pddl_writer.hangleMessages(json_msg)
            self.check_messages(json_msg)
            # check messages
        print("UPDATE: %.3f sec" % (time() - st))

        # print(json_msg)
        self.game_state.update(self.last_action)
        self.loc['x'] = self.game_state.map_obj_player_x
        self.loc[
            'y'] = self.game_state.map_obj_player_y  ####Should put all this in agent.py so it is universal. agent can hold its own x,y,then. needed for throw action
        # self.exploratory_agent.add_server_message(json_msg)

    def check_messages(self, msg_from_server):
        for msgblob in msg_from_server["msgs"]:
            if "messages" in msgblob:
                for message in msgblob["messages"]:
                    if "text" in message:
                        if message["text"].find('You have escaped!') != -1:
                            print("The Agent has escaped the dungeon")
                            self.ready_to_delete = True
                            return
                        elif message["text"].find('You die...') != -1:
                            print("The Agent has died in the dungeon")
                            self.ready_to_delete = True
                            return
                        elif message["text"].find('This will make you lose the game!') != -1:
                            print("The Agent tried to leave the dungeon without the orb")
                            self.plan_action_queue = [agent.CrawlAIAgent._respond_no] + self.plan_action_queue
                            return

    def __str__(self):
        return self.action_selection_type_str + str(self.context_size) + str(self.do_planning)

    def entering_new_state(self):
        self.no_more_goal_contexts_this_state = False
        self.goal_contexts_tried_this_state = []

    def save_data(self, dir, filename=None):
        if not self.data_already_saved:
            dir = dir + '_agent_data/'
            filename_str = None
            if self.data_filename:
                filename_str = self.data_filename
            elif filename:
                filename_str = filename

            filename_str = 'c{}_'.format(self.context_size) + filename_str

            with open(dir + filename_str, 'w+') as f:
                f.write('actions,scores\n')
                actions = self.data['actions']
                scores = self.data['scores']
                # print("actions are {0}\nand scores are{1}".format(actions,scores))
                assert len(actions) == len(scores)
                for i in range(min(len(actions), len(scores))):
                    f.write('{0},{1}\n'.format(actions[i], scores[i]))
                f.flush()
            logging.info("Successfully wrote data to {0}".format(filename_str))

            self.data_already_saved = True

    def _next_action_exploratory_new(self):
        # get next action from plan if there exists one
        if len(self.plan_action_queue) > 0:
            next_action = self.plan_action_queue.pop()
            return (self.get_action_from_str(next_action[0]), list(next_action[1:]))

        # otherwise, attempt planning if:
        # - we have learned at least one rule for an action
        num_learned_action_models = len(self.action_models)
        if self.do_planning and num_learned_action_models > 0:
            print("Performing planning!")
            # TODO refactor this code block
            self.inductive_learning_of_all_actions(do_learning_now=True)
            self.last_learned = self.action_counter
            tried_contexts = []
            # if self.do_planning:
            #     self.do_learning()
            count = -1
            while self.do_planning and self.plan_action_queue == [] and self.learned_new_models:
                # print("Choosing a goal context out of {} tried_contexts".format(len(tried_contexts)))
                goal_contexts, finshed_contexts = self.planning_agent.cg.choose_context(tried_contexts)
                # print("all contexts: {}, goal_contexts: {}, finished_contexts: {}".format(
                # len(self.planning_agent.cg.all_contexts), len(goal_contexts), len(finshed_contexts)))
                for context in finshed_contexts:
                    self.planning_agent.cg.all_contexts.pop(context_i, None)
                self.active_contexts = (list(filter(lambda x: list(x.keys())[0] != context_i, self.active_contexts)))

                if not goal_contexts:
                    break
            tried_contexts += goal_contexts
            while self.plan_action_queue == [] and len(goal_contexts) > 0:
                count += 1
                goal_context = random.choice(goal_contexts)
                goal_contexts.remove(goal_context)
                self.plan_action_queue = self.planning_agent.cg.planning_for_context_as_goal(goal_context, self)
                print("  Goal is {}\n    Plan (len={}) is: {}".format(goal_context, len(self.plan_action_queue),
                                                                      self.plan_action_queue))
                if not self.plan_action_queue:
                    self.plan_action_queue = []
                self.goal_context = goal_context

        # find an action that hasn't been taken in this context yet, if it exists
        # TODO order actions by highest number of current contexts they have NOT been taken in
        active_contexts = self.active_contexts
        actions_not_taken_to_context_counts = {}  # key is action, count is number of active contexts it HASN'T been taken in
        for context_i in active_contexts:
            # take a random action that hasn't been taken in this state yet
            # TODO return
            pass
        pass

    def _next_action_exploratory(self):
        # 1. If we have a plan, execute the next action in that plan
        if len(self.plan_action_queue) > 0:  # get next action from plan if there exists one
            print("Executing next action of queue, {} actions remaining".format(len(self.plan_action_queue) - 1))
            next_action = self.plan_action_queue.pop()
            return (self.get_action_from_str(next_action[0]), list(next_action[1:]))

        # 2. See if we can take any actions now that have not been taken before in currently active contexts
        no_action_from_contexts = False
        actions_to_take = self._get_actions_for_most_contexts()
        next_action = None
        if len(actions_to_take) > 0:
            next_action = random.choice(actions_to_take)
            next_action_formatted = (self.get_action_from_str(next_action[0]), tuple(next_action[1:]))
            while not no_action_from_contexts and next_action_formatted not in self.remaining_actions_for_current_state[self.game_state.state_hash]:
                #print("Had to choose new action because {} has already been taken".format(next_action))
                actions_to_take.remove(next_action)
                if len(actions_to_take) > 0:
                    next_action = random.choice(actions_to_take)
                else:
                    no_action_from_contexts = True
        if not no_action_from_contexts and next_action: # I know, double negatives are ugly
            self._record_action_taken_in_this_state(next_action)
            return (self.get_action_from_str(next_action[0]), list(next_action[1:]))

        # 3. Attempt to plan if this is a planning agent
        if self.do_planning:
            self._perform_planning()
            if len(self.plan_action_queue) > 0:
                next_action = self.plan_action_queue.pop()
                print("Taking first action of newly generated plan: {}".format(next_action))
                return (self.get_action_from_str(next_action[0]), list(next_action[1:]))

        # 4. Otherwise just take a random action
        return self._next_action_random()

    def _perform_planning(self):
        if self.no_more_goal_contexts_this_state:
            return

        print("Performing planning!")
        self.inductive_learning_of_all_actions(do_learning_now=True)
        self.last_learned = self.action_counter
        tried_contexts = []
        # if self.do_planning:
        #     self.do_learning()
        count = -1
        while self.do_planning and self.plan_action_queue == [] and self.learned_new_models:
            print("Choosing a goal context out of {} tried_contexts".format(len(tried_contexts)))
            goal_contexts, finished_contexts = self.planning_agent.cg.choose_context(tried_contexts)

            for context_i in finished_contexts:
                self.planning_agent.cg.all_contexts.pop(context_i, None) # we actually remove contexts that have been fully explored
                self.active_contexts = (list(filter(lambda x: list(x.keys())[0] != context_i, self.active_contexts)))

            if not goal_contexts:
                break

            tried_contexts += goal_contexts

            print("all contexts: {}, goal_contexts: {}, finished_contexts: {}".format(
                len(self.planning_agent.cg.all_contexts), len(goal_contexts), len(finished_contexts)))

            # now go through all the goal contexts to see if we can reach one of them
            while self.plan_action_queue == [] and len(goal_contexts) > 0:
                count += 1
                goal_context = random.choice(goal_contexts)
                goal_contexts.remove(goal_context)

                if goal_context in self.goal_contexts_tried_this_state:
                    continue
                else:
                    self.plan_action_queue = self.planning_agent.cg.planning_for_context_as_goal(goal_context, self)
                    #print("  Goal is {}\n    Plan (len={}) is: {}".format(goal_context, len(self.plan_action_queue),
                                                                          #self.plan_action_queue))
                    self.goal_contexts_tried_this_state.append(goal_context)
                    if not self.plan_action_queue:
                        self.plan_action_queue = []
            #self.goal_context = goal_context

            if len(self.goal_contexts_tried_this_state) == len(self.planning_agent.cg.all_contexts):
                self.no_more_goal_contexts_this_state = True
                print("No more goal contexts this state")

    def _get_actions_for_most_contexts(self):

        counts_least_actions_taken_in_active_contexts = {}
        for context_to_bindings_list in self.active_contexts:
            context = context_to_bindings_list.keys()
            context_i = list(context)[0]
            seen = set()
            for binding in context_to_bindings_list[context_i]:
                binding_parts = binding.split(',')
                mapping = {}
                # print("c is is {}".format(c))
                for arg in self.planning_agent.cg.all_contexts[context_i]['vars']:
                    mapping[arg] = binding_parts[int(self.planning_agent.cg.all_contexts[context_i]['vars'][arg])]
                    # print("  adding mapping[{}]:{} to mapping for context {}".format(arg,mapping[arg],context))
                # for obj in self.game_state.obj_mapping:
                #     mapping[obj]=obj
                skip_list = ['id', 'vars']
                for tmp_action in self.planning_agent.cg.all_contexts[context_i]:
                    if tmp_action in skip_list:
                        continue
                    action_name = tmp_action[0]
                    # get the agent's current x and y position

                    args = [mapping[x] for x in tmp_action[1:]]
                    action = tuple([action_name] + args)
                    if action not in counts_least_actions_taken_in_active_contexts:
                        counts_least_actions_taken_in_active_contexts[action] = 0
                    # if tmp_action in seen:#Possible limiter
                    #     continue
                    # else:
                    #     seen.add(tmp_action)
                    # if max(counts.values()) > 1:
                    #    print("here!")
                    if self.planning_agent.cg.all_contexts[context_i][tmp_action] == 0 and self.do_planning:
                        # print(tmp_action, self.planning_agent.cg.all_contexts[context][tmp_action],context)
                        counts_least_actions_taken_in_active_contexts[
                            action] += 1  # 00/(self.planning_agent.cg.all_contexts[context][tmp_action]+.1)
                    elif not self.do_planning:
                        counts_least_actions_taken_in_active_contexts[action] += 100 / (
                                self.planning_agent.cg.all_contexts[context_i][tmp_action] + .1)
        actions_untaken_in_most_contexts = []
        score = 0
        for action in counts_least_actions_taken_in_active_contexts:
            if counts_least_actions_taken_in_active_contexts[action] == 0:
                continue
            if counts_least_actions_taken_in_active_contexts[action] > score:
                score = counts_least_actions_taken_in_active_contexts[action]
                actions_untaken_in_most_contexts = []
                actions_untaken_in_most_contexts.append(action)
            elif counts_least_actions_taken_in_active_contexts[action] == score:
                actions_untaken_in_most_contexts.append(action)
        self.action_set_score = score
        self.action_set_length = len(actions_untaken_in_most_contexts)

        return actions_untaken_in_most_contexts

    def _next_action_exploratory_old(self):
        '''
        Perform an exploration by first trying actions that have not been taken in an active context and second by
        planning
        '''

        if len(self.plan_action_queue) > 0:  # get next action from plan if there exists one
            print("Executing next action of queue, {} actions remaining".format(len(self.plan_action_queue) - 1))
            next_action = self.plan_action_queue.pop()
            return (self.get_action_from_str(next_action[0]), list(next_action[1:]))
        else:

            if actions_untaken_in_most_contexts == [] and self.do_planning:
                print("Performing planning!")
                self.inductive_learning_of_all_actions(do_learning_now=True)
                self.last_learned = self.action_counter
                tried_contexts = []
                # if self.do_planning:
                #     self.do_learning()
                count = -1
                while self.do_planning and self.plan_action_queue == [] and self.learned_new_models:
                    print("Choosing a goal context out of {} tried_contexts".format(len(tried_contexts)))
                    goal_contexts, finished_contexts = self.planning_agent.cg.choose_context(tried_contexts)

                    for context_i in finished_contexts:
                        self.planning_agent.cg.all_contexts.pop(context_i, None)
                        self.active_contexts = (
                            list(filter(lambda x: list(x.keys())[0] != context_i, self.active_contexts)))

                    if not goal_contexts:
                        break

                    tried_contexts += goal_contexts

                    print("all contexts: {}, goal_contexts: {}, finished_contexts: {}".format(
                        len(self.planning_agent.cg.all_contexts), len(goal_contexts), len(finished_contexts)))

                    # now go through all the goal contexts to see if we can reach one of them
                    while self.plan_action_queue == [] and len(goal_contexts) > 0:
                        count += 1
                        goal_context = random.choice(goal_contexts)
                        goal_contexts.remove(goal_context)
                        self.plan_action_queue = self.planning_agent.cg.planning_for_context_as_goal(goal_context, self)
                        print("  Goal is {}\n    Plan (len={}) is: {}".format(goal_context, len(self.plan_action_queue),
                                                                              self.plan_action_queue))
                        if not self.plan_action_queue:
                            self.plan_action_queue = []
                    self.goal_context = goal_context

            if self.plan_action_queue == []:
                # print('WE HAVE TOTAL COVERAGE, NO NEED TO EXPLORE')
                self.learned_new_models = False
                return self._next_action_random()
            else:
                # take the first action of the plan we just found
                next_action = self.plan_action_queue.pop()
                print("Taking first action of newly generated plan: {}".format(next_action))
                return (self.get_action_from_str(next_action[0]), list(next_action[1:]))

        # MORE RECENT OLD STUFF
        # if counts == {}:
        #     print("Return random action because counts are zero")
        #     return self._next_action_random()
        # # print("about to pick action from action_set of which there are {} options".format(len(action_set)))
        # action = random.choice(action_set)
        #
        # next_action = self.get_action_from_str(action[0])
        # args = list(action[1:])
        # return next_action, args

        # OLDER STUFF

        # # order actions by lowest context count
        # counts = []
        # for action, context_data in self.context_counts.items():
        #     counts.append((action, context_data[1]))
        #
        # counts = sorted(counts, key=lambda data: data[1])
        # min_context = counts[0][1]
        # num_actions = 1
        # while num_actions < len(counts) and min_context == counts[num_actions][1]:
        #     num_actions += 1
        #
        # # choose an action from a set of the lowest
        # next_action = random.choice(counts[:num_actions])[0]
        # print(next_action.msg)

    # else:
    #    next_action = self._next_action_random()

    # print('ACTION: ' + str(next_action))
    # return next_action.get_json()
    # print(next_action)
    # return (self.get_action_from_str(next_action[0]), list(next_action[1:]))
    # return next_action,args

    def get_active_contexts(self):
        return self.active_contexts

    def _next_action_callable(self, next_action):
        arglist = []

        info = getfullargspec(next_action.msg['func'])
        for arg in info.args:
            if arg == "self":
                arglist.append(self)
            if arg in info.annotations:
                if type(0) == info.annotations[arg]:
                    argument = random.choice([self.loc[arg] - 1, self.loc[arg] + 1])
                    arglist.append(argument)
        actions = (next_action.msg['func'](*arglist))
        arglist.remove(self)
        next_action.args = arglist
        # actions=actions[::-1] #reverse list
        next_action.msg['actions'] = actions
        return next_action

    def _get_all_possible_grounded_actions(self):
        self.all_possible_grounded_actions = []
        for action_i in self.all_actions:
            list_of_arg_lists = []  # order matters, i.e. [0] is the list of all possible values for arg1, etc
            for arg in action_i.args:
                arg_choices_list = self.game_state.obj_types[arg]
                list_of_arg_lists.append(arg_choices_list)

            all_arg_combinations = itertools.product(*list_of_arg_lists)
            self.all_possible_grounded_actions += [(action_i, args) for args in list(all_arg_combinations)]
        return self.all_possible_grounded_actions

    def get_random_action_queue_for_state(self, gamestate_hash):
        if gamestate_hash not in self.remaining_actions_for_current_state.keys():
            local_all_possible_grounded_actions = self._get_all_possible_grounded_actions()
            random.shuffle(local_all_possible_grounded_actions)
            self.remaining_actions_for_current_state[gamestate_hash] = local_all_possible_grounded_actions

        return self.remaining_actions_for_current_state[gamestate_hash]

    def _record_action_taken_in_this_state(self, action):
        action_formatted = (self.get_action_from_str(action[0]), tuple(action[1:]))
        try:
            self.remaining_actions_for_current_state[self.game_state.state_hash].remove(action_formatted)
        except:
            print("Warning - tried to remove action that was not in remaining actions")

    def _next_action_random(self):
        # won't repeat in a state
        if self.game_state.state_hash in self.remaining_actions_for_current_state.keys():
            if len(self.remaining_actions_for_current_state[self.game_state.state_hash]) > 0:
                next_action = self.remaining_actions_for_current_state[self.game_state.state_hash].pop()
                return next_action[0], list(next_action[1])

        local_all_possible_grounded_actions = self._get_all_possible_grounded_actions()
        random.shuffle(local_all_possible_grounded_actions)
        self.remaining_actions_for_current_state[self.game_state.state_hash] = local_all_possible_grounded_actions
        next_action = self.remaining_actions_for_current_state[self.game_state.state_hash].pop()
        return next_action[0], list(next_action[1])

        #
        # next_action = random.choice(self.all_actions)  # type: agent.Action
        # args = []
        # for arg in next_action.args:
        #     args.append(random.choice(self.game_state.obj_types[arg]))
        # # print("Random Action: {0}".format(chosen_action))
        # if 'func' in next_action.msg:
        #     next_action = self._next_action_callable(next_action)
        # return (next_action, args)

    def _next_action_human_input(self):
        while True:
            # Ask human user for their input
            text = input("What is your next move?")
            chosen_action = None
            if '8' in text:
                chosen_action = agent.CrawlAIAgent._move_N
            elif '9' in text:
                chosen_action = agent.CrawlAIAgent._move_NE
            elif '7' in text:
                chosen_action = agent.CrawlAIAgent._move_NW
            elif '4' in text:
                chosen_action = agent.CrawlAIAgent._move_W
            elif '1' in text:
                chosen_action = agent.CrawlAIAgent._move_SW
            elif '2' in text:
                chosen_action = agent.CrawlAIAgent._move_S
            elif '3' in text:
                chosen_action = agent.CrawlAIAgent._move_SE
            elif '6' in text:
                chosen_action = agent.CrawlAIAgent._move_E

            elif text.isalpha():
                chosen_action = agent.CrawlAIAgent.create_input_action(text)

            if chosen_action:
                return chosen_action
            else:
                print("Unknown input: " + text + ", please try again.")

    def next_action(self):
        # prev, current = copy.deepcopy(self.previous_game_state), copy.deepcopy(self.game_state)
        # if prev and current:
        #     if len(self.action_history) > 0:
        #         last_action,args = self.action_history[-1]
        #         #print(
        #         #    "** action={0} | prev player x,y is {1} | post player x y {2}".format(last_action, prev.get_player_xy(),
        #         #                                                                          current.get_player_xy()))
        #         print("last_action is {}".format(last_action.name))
        #         self.interaction_history[last_action.name].append([prev, current,args])#changed from tuple to list

        self.previous_game_state = copy.deepcopy(self.game_state)
        # if self.did_learning:
        #     self.score = self.compute_action_models_score()

        # action = self._next_action_exploratory()
        # action = self._next_action_human_input()
        # place in set here
        asp_str = self.game_state.get_asp_str()
        self.active_contexts = []
        context_str = asp_str + "\n"
        counter = 0
        for context in self.planning_agent.cg.all_contexts:
            self.planning_agent.cg.all_contexts[context]['id'] = counter
            returnval = self.planning_agent.cg.build_context_string(context, counter)

            context_str += str(returnval[0]) + "\n"
            self.planning_agent.cg.all_contexts[context]['vars'] = returnval[2]
            counter += 1
        answer_set = (self.planning_agent.cg.get_context_answer_set(context_str))
        answer_set = self.planning_agent.cg.answer_set_list_from_raw_contexts(answer_set)
        for context in self.planning_agent.cg.all_contexts:
            if self.planning_agent.cg.in_context(answer_set, self.planning_agent.cg.all_contexts[context]['id']):
                new = {context: answer_set['context' + str(self.planning_agent.cg.all_contexts[context]['id'])]}
                self.active_contexts.append(new)
                # print("   Active Context: {}".format(new))

        action, args = self._next_action_func()

        self.last_action = tuple([str(action)] + args)

        self.action_history.append((action, args))

        self.data['actions'].append(self.action_counter)
        # if self.perform_learning: self.data['scores'].append(self.score)
        self.action_counter += 1
        # print(self.action_counter)
        if self.action_counter >= self.total_actions_in_experiment:
            self.finished = True
        return (action, args)

    def compute_naive_models_score(self):
        # simple - +1 if it matches, 0 if it doesn't
        score = len(self.successes)

        for action, learned_rules in self.action_models.items():
            if str(action) not in self.successes:  # dont score again after already successful
                # print("Checking to see if learned rule is correct for action {0}".format(action))
                # print("[Perfect] {0}".format(self.perfect_action_models[action]))
                # print("[Learned] {0}".format(learned_rules[0]))
                # Just do the first one for now... will deal with multiple rules later
                if learned_rules[0] == self.perfect_action_models[action][0]:
                    # print("{} Action was successfully learned!".format(action))
                    score += 1
                    self.successes.append(str(action))

                if len(learned_rules) > 1:
                    print("**** FYI there was more than one learned rule...?!?! ******")
                    i = 0
                    for r in learned_rules:
                        print('\t[rule {0}] {1}'.format(i, r))
                        i += 1

        # print("Successes are: {}".format(self.successes))
        # print(
        #    "Learned Model Score is now {0:d} and number of actions executed is {1}".format(score, self.action_counter))

        # if len(self.data['scores']) > 0 and score < self.data['scores'][-1]:
        #    input("Somehow our learning score decreased...Please investigate and press enter to continue")

        return score

    def compute_action_models_score(self):
        running_score = 0
        start_time = time()
        score_set = permute_based_asp_distance(self)
        elapsed_time = time() - start_time
        print(elapsed_time)
        for action in score_set:
            print("{0} has a score of {1:.2f}".format(action, score_set[action]))
            running_score += score_set[action]
        # for action, learned_rules in self.action_models.items():
        #     #print("Checking to see if learned rule is correct for action {0}".format(action))
        #     #print("[Perfect] {0}".format(self.perfect_action_models[action]))
        #     #print("[Learned] {0}".format(learned_rules[0]))
        #     # Just do the first one for now... will deal with multiple rules later
        #     learned_rule_score = 0
        #     for learned_rule in  learned_rules: #TODO:We want to consider all rules, multiple things
        #         for perfect_rule in self.perfect_action_models[action]:
        #             t_learned_rule_score = perfect_rule.asp_distance(learned_rule, self.game_state)
        #             if t_learned_rule_score>learned_rule_score:
        #                 learned_rule_score=t_learned_rule_score
        #     print("{0} has a score of {1:.2f}".format(action,learned_rule_score))
        #     running_score += learned_rule_score
        #
        #
        #
        #     if len(learned_rules) > 1:
        #         print("**** FYI there was more than one learned rule...?!?! ******")
        #         i = 0
        #         for r in learned_rules:
        #             print('\t[rule {0}] {1}'.format(i, r))
        #             i += 1
        print("running score is {:.2f}".format(running_score))
        return running_score

    def get_ilasp_txt(self, action_name, pos, neg):
        ILASP_str = ""
        arguments = '('
        arg_types = get_arg_types(action_name)
        for arg in arg_types:
            arguments += 'var(' + arg + '),'
        arguments = arguments[:-1]
        arguments += ')).\n\n'
        arguments = re.sub("-", "_", arguments)
        ILASP_str += '\n#modeh(' + action_name + arguments

        def _build_args(action_args):
            args = ""
            for arg in action_args:
                args += str(arg) + ","
            args = args[:-1]
            return args

        # add positive examples
        for i in range(len(pos)):
            p = pos[i][0]  # type: gamestate.GameState
            ILASP_str += '#pos({' + action_name + '(' + _build_args(
                pos[i][2]) + ')}, {},{' + p.get_asp_str() + '}).\n\n'
        # add negative examples
        for i in range(len(neg)):
            n = neg[i][0]  # type: gamestate.GameState
            ILASP_str += '#pos({}, {' + action_name + '(' + _build_args(
                neg[i][2]) + ')},{' + n.get_asp_str() + '}).\n\n'

        action = self.get_action_from_str(action_name)
        action_args = get_arg_types(str(action))
        added = set()
        for key, val in get_all_arg_types().items():
            head = key
            body = val
            flag = False
            for arg in body:
                if arg not in action_args:
                    flag = True
            if flag:
                continue
            if head not in added and not self.get_action_from_str(head):
                ILASP_str += "#modeb(1," + head
                if not body == []:
                    ILASP_str += '('
                    for arg in body:
                        ILASP_str += "var(" + arg + "),"
                    ILASP_str = ILASP_str[:-1] + ")).\n"
                    added.add(head)
                else:
                    ILASP_str += ').\n'
        ILASP_str += '#maxv(6).\n'
        return ILASP_str

    def write_ilasp_files(self):
        print("Writing ilasp files")
        for example in self.learning_sets:
            # print(example)
            # print(self.learning_sets[example])
            action = (list(self.learning_sets[example].keys())[0])
            div = "~"
            filename = self.domain_ilasp_data_dir_str
            filename += str(self.run_num) + div + str(self) + div + action + div + str(example) + ".las"
            ilasp_text = self.get_ilasp_txt(action, self.learning_sets[example][action]['pos'],
                                            self.learning_sets[example][action]['neg'])
            with open(filename, 'w+') as f:
                f.write(ilasp_text)

    def do_learning(self):
        for example in self.learning_sets:
            print(example)
            threads = []
            learned = False
            for action in self.learning_sets[example]:
                learned = True
                t = BaseThread(name=action, target=self._inductive_learning_single_action,
                               args=(
                                   self.learning_sets[example][action]['pos'],
                                   self.learning_sets[example][action]['neg'],
                                   action,), callback=cb, callback_args=(action,))
                threads.append(t)
                print("starting learning of ", action)
                t.start()
            time_start = time()
            print("num threads", len(threads))
            for t in threads:
                print(t)
                t.join()
            time_stop = time()

            if learned:
                self.pddl_writer.load_coords(self.pddl_writer.inits)
                self.pddl_writer.write_files("domain.pddl", "problem.pddl", [])
                self.score = self.compute_action_models_score()
                print("Elapsed Time is: ", str(time_stop - time_start))
            self.data['scores'].append(self.score)

    def inductive_learning_of_all_actions(self, do_learning_now=False):
        '''
        Returns a string that can be written to a .las file and immediately given to
        ILASP to try to learn preconditions for that action

        Note: now refers to whether to actually perform learning or to just write out the ilasp file

        :return:
        '''
        learning_rate = 'everytime'  # 'at least one of each'

        def is_pos_example(example):
            prev_game_state = example[0]  # type:gamestate.GameState
            post_game_state = example[1]  # type:gamestate.GameState
            diff = None
            try:
                diff = example[5]
                if not diff == None:
                    return example[3]
            except:
                pass  # keep diff from being appended on multiple times
            prev_facts = list(set(prev_game_state.get_asp_str().split('.')))

            post_facts = list(set(post_game_state.get_asp_str().split('.')))
            for i in range(len(prev_facts)):
                while '\n' in prev_facts[i]:
                    prev_facts[i] = prev_facts[i][1:]
            for i in range(len(post_facts)):
                while '\n' in post_facts[i]:
                    post_facts[i] = post_facts[i][1:]  # remove \n from atoms
            prev_facts = set(prev_facts)
            post_facts = set(post_facts)
            nots = list(prev_facts - post_facts)
            for i in range(0, len(nots)):
                nots[i] = "not " + nots[i]
            diff = set(nots + list(post_facts - prev_facts))
            ldiff = list(diff)
            for i in range(0, len(ldiff)):
                item = re.split('\(|\)', ldiff[i])
                head = item[0]
                if 'cell' in head:
                    cell = item[1]
                    for i in range(0, len(ldiff)):
                        if cell in ldiff[i]:
                            ldiff[i] = ''
            diff = set(ldiff)
            try:
                diff.remove('')
            except:
                pass  # remove anything where the cell was deleted or added, i,e, that cell was added or removed from observations/LOS
            if len(diff) == 0:  # TODO: see if this needs to be changed. string order could differ pre_facts==post_facts
                if example[3]:
                    example.append(diff)
                return example[3]
            else:
                example.append(diff)
                # print(sorted(prev_facts))
                print(sorted(diff))
                # print(sorted(post_facts))
                return example[3]

        total_num_examples = 0

        when_to_learn = 1  # perform learning when you have at least these many examples (doesn't matter if pos or neg)

        # label examples as positive or negative
        for action in self.available_moves:
            action_str = str(action)
            if action_str in self.interaction_history.keys():
                total_num_examples += len(self.interaction_history[action_str])

        threads = []
        self.did_learning = False
        if not do_learning_now:
            self.learning_sets[total_num_examples] = {}

        # for action in self.available_moves:
        # print("processing action "+str(action))
        action = self.last_action[0]
        action_str = str(action)
        num_examples = 0
        pos = []
        neg = []

        if action_str in self.interaction_history.keys():
            # compute positive and negative examples
            for example in self.interaction_history[action_str]:
                if is_pos_example(example):
                    pos.append(example)
                else:
                    neg.append(example)
                num_examples += 1

        logging.info("{3} examples for {2}: {0} pos and {1} neg".format(len(pos), len(neg), action_str, num_examples))
        # print(learning_rate,num_examples,len(pos),len(neg))
        # print(action_str, len(pos), len(neg))

        # if learning_rate == 'at least one of each' and total_num_examples % self.actions_between_learning == 0:

        # if num_examples > 1 and len(pos) > 0 and len(neg) > 0:
        if action_str in self.calls_to_ilasp_per_action.keys():
            # print(total_num_examples)
            # print("calls_to_ilasp_per_action[{0}] is {1}".format(action_str, self.calls_to_ilasp_per_action[action_str]))
            if num_examples not in self.calls_to_ilasp_per_action[action_str]:
                # call ilasp
                self._inductive_learning_single_action(pos, neg, action_str)
                # record so that we don't repeat again until another batch of examples has been received
                self.calls_to_ilasp_per_action[action_str].append(num_examples)

        else:  # first time calling learning with this action
            # self.did_learning=True
            # print(str(self.calls_to_ilasp_per_action))
            # call ilasp
            if do_learning_now:  # do learning now
                # print('here')
                learning_threads = []
                for action in self.update:
                    if self.counts_by_action[action][1] - self.actions_between_plan_learning < \
                            self.counts_by_action[action][0]:
                        continue
                    self.learned_new_models = True
                    self.counts_by_action[action][0] = self.counts_by_action[action][1]
                    pos = []
                    neg = []
                    action_str = str(action)
                    if action_str in self.interaction_history.keys():
                        # compute positive and negative examples
                        for example in self.interaction_history[action_str]:
                            if is_pos_example(example):
                                pos.append(example)
                            else:
                                neg.append(example)
                    if len(pos) == 0:
                        continue
                    th = BaseThread(target=self._inductive_learning_single_action, args=(pos, neg, action_str,))
                    learning_threads.append(th)
                    th.start()
                for th in learning_threads:
                    th.join()
                self.update = set()
            else:
                # perform learning later
                self.learning_sets[total_num_examples][action_str] = {'pos': pos, 'neg': neg}

                filename = self.general_ilasp_data_dir_str + 'ILASP~{0}~{1}.las'.format(self.action_counter,
                                                                                        action_str)
                ilasp_text = self.get_ilasp_txt(action_str, pos, neg)
                with open(filename, 'w+') as f:
                    f.write(ilasp_text)
                print("Just wrote ilasp file to {}".format(filename))

            if do_learning_now:
                for model in self.action_models:
                    for rule in self.action_models[model]:
                        print(rule, rule.effects)

        if total_num_examples >= self.total_actions_in_experiment: self.finished = True
        logging.debug("Total of {0} examples for all actions ".format(total_num_examples))

    def _inductive_learning_single_action(self, pos, neg, action_name,
                                          effect=False):  # effect=True means trying to learn effects, takes perfect preconditions instead of learning them first, skips ILASP
        '''
        :param pos: positive examples
        :param neg: negative examples
        :param action_name: str of action to be learned
        :param asp_bg: background knowledge of the asp
        '''

        if effect:
            rules = [self.perfect_action_models_test[action_name]]
            self.action_models[action_name] = rules
            self._learn_effect_of_action(action_name, pos)
            return

        # all gamestates should have the same background (we'll work around this later on)
        if len(pos + neg) == 0:
            return

        ILASP_str = self.get_ilasp_txt(action_name, pos, neg)

        filename = self.general_ilasp_data_dir_str + 'ILASP~{0}~{1}.las'.format(self.action_counter, action_name)
        print(filename)
        with open(filename, 'w') as f:
            f.write(ILASP_str)

        try:
            completed_process = subprocess.run(
                ["ILASP", "--version=2i", "--clingo5", "-nc", "-ml=5", "--max-rule-length=5", filename],
                stdout=subprocess.PIPE)
            result_output_str = completed_process.stdout.decode()
        except:
            return

        if 'UNSAT' in result_output_str:
            logging.warning("Unable to learn anything - consider investigating {}".format(filename))
        else:
            rules = []
            for line in result_output_str.split('\n'):
                if action_name in line:
                    rules.append(ActionPreconditionsRule(line))
            self.action_models[action_name] = rules
            self._learn_effect_of_action(action_name, pos)

            # write out to learned rule so we don't learn this a second time
            result_output_filename = filename[0:-4] + '~learned_rule.txt'
            with open(result_output_filename, 'w') as f:
                f.write(result_output_str)
            print("Wrote out learned rule for action {} to file {}".format(action_name, result_output_filename))

    def _learn_effect_of_action(self, action_name, pos):
        for rule in self.action_models[action_name]:
            diffs = []
            for pe in pos:
                body = rule.body
                variables = set()
                for b in body:
                    varArray = (b.args)
                    for v in varArray:
                        if re.search("V[0-9]", v):
                            variables.add(v)
                lifted_vars = list(sorted(variables))
                variables = str(sorted(variables))
                variables = re.sub("\[", "(", variables)
                variables = re.sub("\]", ")", variables)
                variables = re.sub("'", "", variables)
                precond = rule.standardized_asp_rule_str
                precond = re.sub(action_name + "\(.*\) :-", action_name + variables + " :-", precond)
                buildString = pe[0].get_asp_str()
                buildString += "\n"
                buildString += precond  # +"\n#show "+action_name+
                # print(buildString)
                # print(rule.head)
                filename = self.general_asp_data_dir_str + 'ASP_{0}_{1}.lp'.format(self.learning_file_number,
                                                                                   action_name)
                # print(filename)
                with open(filename, 'w') as f:
                    f.write(buildString)
                self.learning_file_number += 1

                completed_process = subprocess.run(
                    ["clingo", filename],
                    stdout=subprocess.PIPE)
                answer_set = (completed_process.stdout.decode('utf-8').split('\n')[4].split(' '))
                answers = []
                for atom in answer_set:
                    sub_answers = {}
                    if action_name in atom:
                        vars = re.split('\(|\)', atom)
                        vars = vars[1].split(',')
                        counter = 0
                        for var in vars:
                            sub_answers[var] = lifted_vars[counter]
                            counter = counter + 1
                        answers.append(sub_answers)
                diff_root = list(pe[5])
                length = 0
                for i in range(0, len(diff_root)):
                    diff_split = re.split('\(|\)', diff_root[i])
                    try:
                        vars = diff_split[1].split(',')
                    except:
                        vars = []
                    length += len(vars)
                for ans in answers:
                    temp_len = length
                    diff = copy.deepcopy(diff_root)
                    for i in range(0, len(diff_root)):
                        for a in ans:
                            diff_split = re.split('\(|\)', diff[i])
                            try:
                                vars = diff_split[1].split(',')
                            except:
                                vars = []
                            for x in range(0, len(vars)):
                                if a == vars[x].strip():
                                    temp_len -= 1
                                    vars[x] = ans[a]
                            try:
                                diff_split[1] = vars
                            except:
                                diff_split.append(vars)
                            diff[i] = ""
                            for part in diff_split:
                                diff[i] += str(part)
                            diff[i] = re.sub('\[', '(', diff[i])
                            diff[i] = re.sub('\]', ')', diff[i])
                            diff[i] = re.sub("'", "", diff[i])
                    if temp_len == 0:
                        diffs.append(diff)  # SAVE
            diffs = [list(x) for x in set(tuple(x) for x in diffs)]
            for diff in diffs:
                for idx, d in enumerate(diff):
                    diff[idx] = Term(d)
                rule.effects = diff
                # print(rule.get_pddl_str())
            # print(rule.get_pddl_str())
            # print(rule)

    def update_context_counts(self):
        if self.action_selection_type_str == 'explore':
            if self.last_action is not None and (self.last_action[0]) in [str(a) for a in self.available_moves]:
                # print("UPDATING AGENT")

                # mapping = {}
                # for arg in self.planning_agent.cg.all_contexts[context]['vars']:
                #     mapping[arg] = c[int(self.planning_agent.cg.all_contexts[context]['vars'][arg])]
                # for obj in self.game_state.obj_mapping:
                #     mapping[obj] = obj
                # skip_list = ['id', 'vars']
                # for tmp_action in self.planning_agent.cg.all_contexts[context]:
                #     if tmp_action in skip_list:
                #         continue
                #     action_name = tmp_action[0]
                #     args = [mapping[x] for x in tmp_action[1:]]

                for context in self.active_contexts:
                    c = list(context.keys())[0]
                    action = (self.last_action)
                    mapping = (self.planning_agent.cg.all_contexts[c]['vars'])
                    arg_lists = []
                    for arg_set in context[c]:
                        arg_list = {}
                        arg_set = arg_set.split(',')
                        for idx, arg in enumerate(arg_set):
                            idx = str(idx)
                            for key in mapping:
                                if mapping[key] == idx:
                                    arg_list[arg] = (key)
                        arg_lists.append(arg_list)

                    laction = list(action)
                    types = get_arg_types(laction[0])
                    arg_num = len(types)
                    action_set = set()
                    for arg_list in arg_lists:
                        tmp_action = copy.deepcopy(laction)
                        count = 0
                        for arg in arg_list:
                            for idx, part in enumerate(laction):
                                if idx == 0:
                                    continue
                                if arg == part:
                                    tmp_action[idx] = arg_list[arg]
                                    count += 1
                        if count == arg_num:  # 1 instead of all
                            action_set.add(tuple(tmp_action))
                    for action in action_set:
                        self.planning_agent.cg.all_contexts[c][action] += 1
                    # print(self.planning_agent.cg.all_contexts[c])
                    # print(action_set)
                    # print(c)

    def cmpl_coords(self, pos):
        return pos[0] is not None and pos[1] is not None

    def term_exists(self, terms, pred, obj):
        found = False
        for term in terms:
            if term.pred_str == pred and len(term.args) > 0 and term.args[0] == obj:
                found = True
        return found

    # convert the list of fact changes to LogicHandler
    # for the last step to a series of Term objects
    # takes a position dictionary to keep track of the x(), pos_x(), y(), and pos_y()
    # for determining at() terms
    def convert_lh_facts_to_terms(self, old_facts, new_facts, pos_dict):
        move_list = list(map(str, agent.CrawlAIAgent.move_actions))  # move list to create Terms
        terms = []
        negated = True
        flip_count = len(old_facts)
        for fact in itertools.chain(old_facts, new_facts):
            if not negated and fact[0] == '-':
                t = Term('=', [str(fact[-1]), str(fact[1]), '-', str(fact[2])], move_list)
                terms.append(t)
            elif not negated and (fact[0] == 'x' or fact[0] == 'pos_x'):
                obj = fact[1]
                coord = str(fact[-1])
                if obj in pos_dict:
                    if pos_dict[obj][0] is not coord:
                        if self.cmpl_coords(pos_dict[obj]):
                            if not self.term_exists(terms, 'not at', obj):
                                t = Term('not at', [obj, pos_dict[obj][0], pos_dict[obj][1]], move_list)
                                terms.append(t)
                        pos_dict[obj][0] = coord
                        if self.cmpl_coords(pos_dict[obj]):
                            if not self.term_exists(terms, 'at', obj):
                                t = Term('at', [obj, pos_dict[obj][0], pos_dict[obj][1]], move_list)
                                terms.append(t)
                else:
                    pos_dict[obj] = [coord, None]
            elif not negated and (fact[0] == 'y' or fact[0] == 'pos_y'):
                obj = fact[1]
                coord = str(fact[-1])
                if obj in pos_dict:
                    if pos_dict[obj][1] is not coord:
                        if self.cmpl_coords(pos_dict[obj]):
                            if not self.term_exists(terms, 'not at', obj):
                                t = Term('not at', [obj, pos_dict[obj][0], pos_dict[obj][1]], move_list)
                                terms.append(t)
                        pos_dict[obj][1] = coord
                        if self.cmpl_coords(pos_dict[obj]):
                            if not self.term_exists(terms, 'at', obj):
                                t = Term('at', [obj, pos_dict[obj][0], pos_dict[obj][1]], move_list)
                                terms.append(t)
                else:
                    pos_dict[obj] = [None, coord]
            elif fact[0] == 'is_wall':
                pred = 'wall'
                if negated:
                    pred = 'not wall'
                t = Term(pred, [fact[1]], move_list)
                terms.append(t)

            flip_count -= 1
            if flip_count == 0:
                negated = not negated

        return (terms)

    def step_complete(self):
        # prints the effects of step based on fact changes in LogicHandler
        effect_of_step = self.convert_lh_facts_to_terms(self.lh.get_false_facts(), self.lh.get_delta_facts(),
                                                        self.pos_dict)
        print('EFFECT OF STEP\n' + '\n'.join(map(str, effect_of_step)))

        # self.lh.reset_delta_facts() # resets the LogicHandlers tracking of fact changes

        # self.update_context_counts()

        # Uncomment these two lines when we want to see context graphs (it slows down the agent so we don't use when running experiments)
        # prefix_str = 'exploratory-step%04d-' % self.action_counter + datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d--%H-%M-%S')
        # self.save_figure_context_data(prefix_str, 'context_graphs', 'context_info')

        # self.save_figure_2('context_graphs/exploratory-step%04d-' % self.action_counter + datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d--%H-%M-%S'))

    def ready_to_delete_game(self):
        if len(self.data['scores']) > self.total_actions_in_experiment or self.finished:
            return True

        return False


if __name__ == "__main__":

    agent_1 = RelationalLearningAgent()

    for model in agent_1.perfect_action_models.values():
        print(model.get_pddl_str())
