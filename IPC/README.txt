To run experiments follow these steps:

1. python3 IPC_pddl_main.py exp dcss_problem_left_side_maze.pddl
2. python3 run_ilasp_in_parallel_with_params.py <folder name with .las files> 1 1200 15
3.
4.
...



#### Dependencies

- missing pddlpy, then run 'pip3 install pddlpy'
- getting error 'no module name '__builtin__'' , then first try 'pip3 install antlr4-python3-runtime' and if that doesn't work,
  try manually installing it from https://pypi.org/project/antlr4-python3-runtime/. If you have to manually install, after
  you tar -xzvf, run 'python3 setup.py install'
- missing matplotlib, then run 'pip3 install matplotlib'
- missing ply, then run 'pip3 install ply'

- don't forget to make sure there are the following folders under IPC:
  'ilasp_data'
  'asp_data'
  'dcss_ilasp_data'
