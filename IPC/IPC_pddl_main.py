import json
import time
import logging
import sys
import pddlpy
import threading
import signal
import datetime
import functools
import signal
import os
import pprint
from threads import BaseThread, cb
from relational_learning_agent import RelationalLearningAgent
from term import Term
import copy
import re
from pypddl_parser.pypddl_parser.pddlparser import PDDLParser
import argparse
from term import add_to_arg_types

finished = False


# def parse():
#     usage = 'python3 main.py <DOMAIN> <INSTANCE>'
#     description = 'pypddl-parser is a PDDL parser built on top of ply.'
#     parser = argparse.ArgumentParser(usage=usage, description=description)
#
#     parser.add_argument('domain',  type=str, help='path to PDDL domain file')
#     parser.add_argument('problem', type=str, help='path to PDDL problem file')
#
#     return parser.parse_args()

def clean(string):
    return re.sub("-", "_", string)


def clean_pddl(tempdomain, tempproblem):
    tempdomain._name = clean(tempdomain._name)

    for idx, types in enumerate(tempdomain._types):
        tempdomain._types[idx] = clean(types)

    for pred in tempdomain._predicates:
        pred._name = clean(pred._name)
        arg_types = []
        for arg in pred._args:
            arg._name = clean(arg._name)
            arg._type = clean(arg._type)
            arg_types.append(arg._type)
        add_to_arg_types(pred.name, arg_types)
    # print(tempdomain._predicates)  # list of Predicate objects

    for op in tempdomain._operators:
        op._name = clean(op._name)
        arg_types = []
        for arg in op._params:
            arg._name = clean(arg._name)
            arg._type = clean(arg._type)
            arg_types.append(arg.type)
        add_to_arg_types(op.name, arg_types)

        for idx, pre in enumerate(op._precond):
            pre._predicate._name = clean(pre._predicate._name)
            for idx, arg in enumerate(pre._predicate._args):
                pre._predicate._args[idx] = clean(arg)

        for idx, eff in enumerate(op._effects):
            eff = eff[1]
            op._effects[idx] = eff
            eff._predicate._name = clean(eff._predicate._name)
            for idx, arg in enumerate(eff._predicate._args):
                eff._predicate._args[idx] = clean(arg)

    tempproblem._name = clean(tempproblem._name)
    new_objects = {}
    for key, val in tempproblem._objects.items():
        new_objects[clean(key)] = []
        add_to_arg_types(clean(key), [clean(key)])
        for v in val:
            new_objects[clean(key)].append(clean(v))

    tempproblem._objects = new_objects

    for pred in tempproblem._init:
        tempproblem._init.remove(pred)
        tempproblem._init.add(clean(pred))

    tempproblem._domain = clean(tempproblem._domain)

    tempdomprob = Object()
    tempdomprob.domain = tempdomain
    tempdomprob.problem = tempproblem
    return tempdomprob


class Object(object):
    pass


def single_run(action_type_str, context_size, domprob, dir, plan='No', run_num=None, action_limit=10000, expn=-1):
    print("expn is {}".format(expn))
    ai = RelationalLearningAgent(action_type_str, context_size, domprob, plan, run_num=run_num, action_limit=action_limit, expn=expn)

    # OLD
    # if action_type_str:
    #     agent_file_name = '{0}-{1}-action_{2}-context_size_{3}.csv'.format(str(ai), datetime.datetime.strftime(
    #         datetime.datetime.now(), '%Y-%m-%d--%H-%M-%S'), action_type_str, context_size)
    # else:
    #     agent_file_name = str(ai) + '-' + datetime.datetime.strftime(datetime.datetime.now(),
    #                                                                  '%Y-%m-%d--%H-%M-%S') + '.csv'
    #
    #
    # ai.set_data_filename(agent_file_name)

    run(ai, dir)


def take_action(ai, action, args):
    print(action.effects)
    ai.prev_actions_tried = ai.actions_tried
    ai.actions_tried+=1
    for effect in action.effects:
        effect = copy.deepcopy(effect)
        for idx, val in enumerate(effect.args):
            val = val.strip()
            idy = (int(val[1]))
            effect.args[idx] = args[idy]
        if effect.pred_str[:3] == "not":
            new_term = (Term(effect.pred_str[4:], effect.args))
            pop_list = []
            for term in ai.game_state.state:
                if term == new_term:
                    pop_list.append(term)
            for term in pop_list:
                ai.game_state.remove_term_from_state(term)
                print("REMOVING:", term)
        else:
            ai.game_state.add_term_from_state(effect)
            print("ADDING:", effect)
            #print("Action rule is {}".format(action))
    ai.states.append(ai.game_state.get_asp_str())
    ai.entering_new_state()

    #print("Actions tried {} and previous actions tried {}".format(ai.actions_tried, ai.prev_actions_tried))
    #print(
    #    "gamestate 1 hash: {}\nASP_STR: {}\n\ngamestate 2 hash: {}\nASP_STR: {}".format(ai.game_state.state_hash,
    #                                                                                    ai.game_state.get_full_state_str(),
    #                                                                                    ai.previous_game_state.state_hash,
    #                                                                                    ai.previous_game_state.get_full_state_str()))


def get_asp_strs(ai,next_action):
    asp_strs = []
    for rule in next_action.apr:
        # print("  Rule is {}".format(rule))
        t_rule = copy.deepcopy(rule)
        head = t_rule.head[0]
        body = t_rule.body
        remove = []
        for arg in head.args:
            flag = True
            for term in body:
                for barg in term.args:
                    if arg == barg:
                        flag = False
            if flag:
                remove.append(arg)
        for arg in remove:
            head.args.remove(arg)
        arg_len = len(head.args)
        arg_set = set()
        rule_str = " :- "
        for b in body:
            for arg in b.args:
                arg_set.add(arg)
            rule_str += str(b) + ","
        arg_set = sorted(arg_set)
        arg_str = "("
        for arg in arg_set:
            arg_str += arg + ","
        arg_str = head.pred_str + arg_str[:-1] + ")"
        new_head = Term(arg_str)
        rule_str = arg_str + rule_str
        rule_str = rule_str[:-1] + "."
        asp_str = ai.game_state.get_asp_str()
        asp_str += "\n" + rule_str + "\n" + "#show " + head.pred_str + "/" + str(len(new_head.args)) + ".\n"
        asp_strs.append((asp_str, arg_len, rule))
    return asp_strs

def run(ai, dir):
    if ai.action_selection_type_str != 'random':
        ai.planning_agent.cg.generateContexts(ai)

    print("There are {} unique contexts of size {}".format(len(ai.planning_agent.cg.all_contexts.keys()), ai.context_size))

    num_contexts = len(ai.planning_agent.cg.all_contexts.keys())
    try:
        with open(str(num_contexts) + '_contexts_of_size_'+str(ai.context_size)+".txt", 'w') as f:
            context_strs = [str(context) for context in ai.planning_agent.cg.all_contexts.keys()]
            context_strs = sorted(context_strs)
            for context in context_strs:
                f.write(context+"\n")

    except:
        pass



    ai.states = []
    ai.states.append(ai.game_state.get_asp_str())

    beginning_time = time.time()
    print()
    while not ai.finished:
        start = time.time()
        next_action, args = ai.next_action()

        ai.prev_actions_tried = ai.actions_tried

        agent_locs = [fact for fact in ai.states[-1].split('\n') if 'agentat' in fact]

        ai.counts_by_action[next_action][1] += 1
        ai.update.add(next_action)

        #time.sleep(0.5)
        #print ("Total Actions Taken:", ai.action_counter)
        #print("Next action apr is {}".format(next_action.apr))

        for asp_str, arg_len, rule in get_asp_strs(ai,next_action):
            answer_set = ai.planning_agent.cg.get_context_answer_set(asp_str)
            answer_set = ai.planning_agent.cg.answer_set_list_from_raw(answer_set)
            ai.update_context_counts()
            pos = False
            for answer, answer_argss in answer_set:
                answer_argss = answer_argss.split(",")
                answer_args = answer_argss[:arg_len]
                #print("args            {}\nanswer_args are {}".format(args,answer_args))
                if args == answer_args:
                    pos = True
                    take_action(ai, rule, answer_argss)
            prev, current = copy.deepcopy(ai.previous_game_state), copy.deepcopy(ai.game_state)
            last_action, args = ai.action_history[-1]
            if pos:
                ai.interaction_history[last_action.name].append([prev, current, args, pos, answer_argss])
            else:
                ai.interaction_history[last_action.name].append([prev, current, args, pos, None])

            if pos and ai.perform_learning:
                print("Performing learning")
                ai.inductive_learning_of_all_actions()
            #print("Current game state hash is {}".format(ai.game_state.state_hash))
            iter_time = time.time()-start
            total_run_time = time.time() - beginning_time

            # #A - number of actions executed
            # A-set-L - action set length
            # A-Set-S - action set score
            # NA - next action
            # NTA - num tried actions
            # ALCS - Rand Actions Left Curr State
            # NAC - num active contexts
            # GCNT - goal contexts not tried
            print(
                "#A {:5d} | A-Set-L {:4d} | A-Set-S {:8f} | {:17} | NextA: {:32} | NumTA {:3d} | ALCS {:5d} | NAC {:3d} | GCNT {:7d} | iter_time {:.2f} | elaps_time {:.2f}".format(
                    ai.action_counter, ai.action_set_length, ai.action_set_score, str(agent_locs[0]), str((next_action, args)),
                    ai.actions_tried, len(ai.get_random_action_queue_for_state(ai.game_state.state_hash)), len(ai.get_active_contexts()), len(ai.planning_agent.cg.all_contexts) - len(ai.goal_contexts_tried_this_state), iter_time, total_run_time))

    ai.write_ilasp_files()
    #ai.do_learning()
    #ai.save_data(dir)


if __name__ == "__main__":
    whole_exp_start_time = time.time()
    cli_args = sys.argv
    experiment_timestamp = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d--%H-%M-%S')

    if len(cli_args) < 4:
        print("Incorrect number of arguments, correct usage:")
        print("python3 IPC_pddl_main.py [exp/random] <pddl problem file> <experiment number>")
        print("[random/exp/plan]: refers to exploration strategy")
        print("<pddl problem file>: example is dcss_problem_left_side_maze.pddl")
        print("<action limit>: maximum number of actions agent will execute")
        print("<experiment number>: a new unique experiment number for all runs in this experiment")

        sys.exit()

    explore_strat = cli_args[1]
    pddl_problem_file = cli_args[2]
    action_limit = int(cli_args[3])
    experiment_id = int(cli_args[4])


    # check that experiment id is new, and if so write to file of previous experiments
    prior_experiments = []
    if os.path.exists('experiments_summary.txt'):
        with open('experiments_summary.txt','r') as f:
            for line in f.readlines():
                if 'Prior Experiments: ' in line:
                    exp_list_start = len('Prior Experiments: ')
                    prior_experiments = eval(line[exp_list_start:].strip())
    else:
        print("experiments_summary.txt file did not exist, creating new one")

    #print("Found prior experiments of {}".format(prior_experiments))
    if experiment_id in prior_experiments:
        print("Sorry, you must enter a new experiment id not found in prior experiments\nPrior Experiments are: {}\n".format(prior_experiments))
        sys.exit()

    with open('experiments_summary.txt','a+') as exp_summary_f:
        exp_summary_f.write('\nPrior Experiments: {}\n' .format(prior_experiments + [experiment_id]))
        exp_summary_f.flush()

        exp_summary_output_str = '\nNew Experiment {} began on {}:\n'.format(experiment_id, datetime.datetime.strftime(datetime.datetime.now(), '%a %b %d at %H:%M:%S, %Y'))

        context_sizes = []
        agent_action_selection_types = []
        if cli_args[1] == 'plan':
            agent_action_selection_types = ['planning']  # ['random','explore']
            context_sizes = [2] #, 3, 4]
        if cli_args[1] == 'exp':
            agent_action_selection_types = ['explore']  # ['random','explore']
            context_sizes = [2] #,3,4]
        elif cli_args[1] == 'random' :
            agent_action_selection_types = ['random']  # ['random','explore']


        run_num = 0
        total_runs = 10

        pddl_dir = pddl_problem_file.split("_")[0]
        domain = "pddl_domains/" + pddl_dir + "_domain.pddl"
        full_pddl_problem_file = "pddl_domains/" + pddl_problem_file
        tempdomain = PDDLParser.parse(domain)
        tempproblem = PDDLParser.parse(full_pddl_problem_file)
        domprob = clean_pddl(tempdomain, tempproblem)
        catchup = False
        sizes = [4]

        # make fresh directories for ilasp_data, {domprob"_ilasp_data}, and asp_data
        #os.makedirs(domprob.domain.name)

        #if catchup:
        #    for context_size in sizes:
        #        single_run("explore", context_size, domprob, pddl_dir, "Yes", run_num=run_num)
        #        print("Finished ", "explore", context_size, "Planning")
        #    run_num += 1

        while run_num < total_runs:
            for action_type_str in agent_action_selection_types:
                if action_type_str == 'planning':
                    action_type_str = 'explore' # planning is just explore with plan set to yes
                    #for context_size in context_sizes:
                    #    single_run(action_type_str, context_size, domprob, pddl_dir, "No", run_num=run_num,action_limit=action_limit)
                    #    print("Finished ", action_type_str, context_size, "No Planning")
                    for context_size in context_sizes:
                        start = time.time()
                        #print("Now running {},{},{},{} at ".format(action_type_str, context_size, domprob, pddl_dir))
                        planning_str = "Yes" # "No"
                        single_run(action_type_str, context_size, domprob, pddl_dir, planning_str, run_num=run_num,action_limit=action_limit, expn=experiment_id)
                        exp_summary_output_str += "\tFinished Run {} with ActionType {} and ContextSize {} and Planning ({}) | Took {} seconds\n".format(run_num, action_type_str, context_size, planning_str, time.time() - start)
                        #print("Finished Run {} with ActionType {} and ContextSize {} and Planning ({}) | Took {} seconds".format(run_num, action_type_str, context_size, planning_str, time.time()-start))
                elif action_type_str == 'explore':
                    #for context_size in context_sizes:
                    #    single_run(action_type_str, context_size, domprob, pddl_dir, "No", run_num=run_num,action_limit=action_limit)
                    #    print("Finished ", action_type_str, context_size, "No Planning")
                    for context_size in context_sizes:
                        start = time.time()
                        #print("Now running {},{},{},{} at ".format(action_type_str, context_size, domprob, pddl_dir))
                        planning_str = "No" # "No"
                        single_run(action_type_str, context_size, domprob, pddl_dir, planning_str, run_num=run_num,action_limit=action_limit, expn=experiment_id)
                        exp_summary_output_str += "\tFinished Run {} with ActionType {} and ContextSize {} and Planning ({}) | Took {} seconds\n".format(run_num, action_type_str, context_size, planning_str, time.time() - start)
                        #print("Finished Run {} with ActionType {} and ContextSize {} and Planning ({}) | Took {} seconds".format(run_num, action_type_str, context_size, planning_str, time.time()-start))
                elif action_type_str == 'random':
                    start = time.time()
                    single_run(action_type_str, 3, domprob, pddl_dir, plan='no', run_num=run_num,action_limit=action_limit, expn=experiment_id)
                    exp_summary_output_str += "\tFinished Run {} with ActionType {} and ContextSize {} and Planning ({}) | Took {} seconds\n".format(
                        run_num, action_type_str, 'Default 3', 'No', time.time() - start)
                    #print("Finished ", action_type_str)
            run_num += 1
        exp_summary_output_str += '  Experiment finished cleanly with no problems\n'
        exp_summary_output_str += '  Entire Experiment {} took {} seconds and finished on {}\n'.format(experiment_id, time.time() - whole_exp_start_time, datetime.datetime.strftime(datetime.datetime.now(), '%a %b %d at %H:%M:%S, %Y'))
        exp_summary_f.write(exp_summary_output_str)