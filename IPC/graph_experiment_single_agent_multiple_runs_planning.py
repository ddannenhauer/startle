import copy
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib import cm
import os
import sys
# get the most recent data file

if len(sys.argv) >= 2:
    print("Incorrect number of arguments, correct usage:")
    print("python3 graph_experiment_single_agent.py")
    sys.exit()
else:
    csv_filenames = {}
    csv_filenames[33] = 'temp_csv_files/expn_33_run_0_temp_dec_13_scores.csv'
    csv_filenames[42] = 'temp_csv_files/expn_42_run_0_temp_dec_13_scores.csv'
    csv_filenames[209] = 'temp_csv_files/expn_209_run_0_temp_dec_13_scores.csv'

    avg_precision_runs_to_actiontypes_to_scores = {} # example: {33: {'move_s': {1156 : 0.75, 1523: 0.8}, 'move_w': {1200:0.3, 1600:0.4}}}
    avg_recall_runs_to_actiontypes_to_scores = {}
    global_precision_runs_to_actiontypes_to_scores = {}
    global_recall_runs_to_actiontypes_to_scores = {}
    f1_runs_to_actiontypes_to_scores = {}


    for run, filename in csv_filenames.items():
        header = True
        with open(filename, 'r') as f:
            for line in f.readlines():
                if header:
                    header = False
                else:
                    row = line.strip().split(',')
                    run_id = int(row[0])
                    agent_name = str(row[1])
                    action_name = str(row[2])
                    action_count = int(row[3])
                    avg_precision = float(row[4])
                    avg_recall = float(row[5])
                    global_precision = float(row[6])
                    global_recall = float(row[7])

                    f1_score = 2* (avg_precision * avg_recall / (avg_precision + avg_recall))

                    if run_id != 0:
                        print("Problem with {}, there's a run_id != 0".format(filename))
                        sys.exit()

                    # avg_precision
                    if run not in avg_precision_runs_to_actiontypes_to_scores.keys():
                        avg_precision_runs_to_actiontypes_to_scores[run] = {}

                    if action_name not in avg_precision_runs_to_actiontypes_to_scores[run].keys():
                        avg_precision_runs_to_actiontypes_to_scores[run][action_name] = {}

                    avg_precision_runs_to_actiontypes_to_scores[run][action_name][action_count] = avg_precision

                    # avg_recall
                    if run not in avg_recall_runs_to_actiontypes_to_scores.keys():
                        avg_recall_runs_to_actiontypes_to_scores[run] = {}

                    if action_name not in avg_recall_runs_to_actiontypes_to_scores[run].keys():
                        avg_recall_runs_to_actiontypes_to_scores[run][action_name] = {}

                    avg_recall_runs_to_actiontypes_to_scores[run][action_name][action_count] = avg_recall

                    # global_precision
                    if run not in global_precision_runs_to_actiontypes_to_scores.keys():
                        global_precision_runs_to_actiontypes_to_scores[run] = {}

                    if action_name not in global_precision_runs_to_actiontypes_to_scores[run].keys():
                        global_precision_runs_to_actiontypes_to_scores[run][action_name] = {}

                    global_precision_runs_to_actiontypes_to_scores[run][action_name][action_count] = global_precision

                    # global_recall
                    if run not in global_recall_runs_to_actiontypes_to_scores.keys():
                        global_recall_runs_to_actiontypes_to_scores[run] = {}

                    if action_name not in global_recall_runs_to_actiontypes_to_scores[run].keys():
                        global_recall_runs_to_actiontypes_to_scores[run][action_name] = {}

                    global_recall_runs_to_actiontypes_to_scores[run][action_name][action_count] = global_recall

                    # f1 scores
                    if run not in f1_runs_to_actiontypes_to_scores.keys():
                        f1_runs_to_actiontypes_to_scores[run] = {}

                    if action_name not in f1_runs_to_actiontypes_to_scores[run].keys():
                        f1_runs_to_actiontypes_to_scores[run][action_name] = {}

                    f1_runs_to_actiontypes_to_scores[run][action_name][action_count] = f1_score

    graph_each_flag = False

    # left off here, keep track of all scores
    action_avg_ap = {} # key is actiontype, then dict with key action_count , val is a list of scores
    action_avg_ar = {}
    action_avg_gp = {}
    action_avg_gr = {}
    action_avg_f1 = {}

    # Now graph each action of each run
    for run in csv_filenames.keys():

        #avg_precision
        for actiontype in avg_precision_runs_to_actiontypes_to_scores[run].keys():

            if actiontype not in action_avg_ap.keys():
                action_avg_ap[actiontype] = {}

            if actiontype not in action_avg_ar.keys():
                action_avg_ar[actiontype] = {}

            if actiontype not in action_avg_gp.keys():
                action_avg_gp[actiontype] = {}

            if actiontype not in action_avg_gr.keys():
                action_avg_gr[actiontype] = {}

            if actiontype not in action_avg_f1.keys():
                action_avg_f1[actiontype] = {}


            if graph_each_flag:
                f, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, sharex='col', sharey='row')

            xvals_ap = []
            yvals_ap = []

            for action_count in sorted(avg_precision_runs_to_actiontypes_to_scores[run][actiontype].keys()):
                ap = avg_precision_runs_to_actiontypes_to_scores[run][actiontype][action_count]
                xvals_ap.append(action_count)
                yvals_ap.append(ap)
                if action_count not in action_avg_ap[actiontype].keys():
                    action_avg_ap[actiontype][action_count] = [ap]
                else:
                    action_avg_ap[actiontype][action_count].append(ap)

            if graph_each_flag:
                ax1.plot(xvals_ap,yvals_ap)
                ax1.set_title('avg_precision')

            xvals_ar = []
            yvals_ar = []
            for action_count in sorted(avg_recall_runs_to_actiontypes_to_scores[run][actiontype].keys()):
                ar = avg_recall_runs_to_actiontypes_to_scores[run][actiontype][action_count]
                xvals_ar.append(action_count)
                yvals_ar.append(ar)
                if action_count not in action_avg_ar[actiontype].keys():
                    action_avg_ar[actiontype][action_count] = [ar]
                else:
                    action_avg_ar[actiontype][action_count].append(ar)

            if graph_each_flag:
                ax2.plot(xvals_ar, yvals_ar)
                ax2.set_title('avg_recall')

            xvals_gp = []
            yvals_gp = []
            for action_count in sorted(global_precision_runs_to_actiontypes_to_scores[run][actiontype].keys()):
                gp = global_precision_runs_to_actiontypes_to_scores[run][actiontype][action_count]
                xvals_gp.append(action_count)
                yvals_gp.append(gp)
                if action_count not in action_avg_gp[actiontype].keys():
                    action_avg_gp[actiontype][action_count] = [gp]
                else:
                    action_avg_gp[actiontype][action_count].append(gp)
            if graph_each_flag:
                ax3.plot(xvals_gp, yvals_gp)
                ax3.set_title('global_precision')

            xvals_gr = []
            yvals_gr = []
            for action_count in sorted(global_recall_runs_to_actiontypes_to_scores[run][actiontype].keys()):
                gr = global_recall_runs_to_actiontypes_to_scores[run][actiontype][action_count]
                xvals_gr.append(action_count)
                yvals_gr.append(gr)
                if action_count not in action_avg_gr[actiontype].keys():
                    action_avg_gr[actiontype][action_count] = [gr]
                else:
                    action_avg_gr[actiontype][action_count].append(gr)
            if graph_each_flag:
                ax4.plot(xvals_gr, yvals_gr)
                ax4.set_title('global_recall')

            xvals_f1 = []
            yvals_f1 = []
            for action_count in sorted(f1_runs_to_actiontypes_to_scores[run][actiontype].keys()):
                f1 = f1_runs_to_actiontypes_to_scores[run][actiontype][action_count]
                xvals_f1.append(action_count)
                yvals_f1.append(f1)
                if action_count not in action_avg_f1[actiontype].keys():
                    action_avg_f1[actiontype][action_count] = [f1]
                else:
                    action_avg_f1[actiontype][action_count].append(f1)

            if graph_each_flag:
                plt.suptitle("Run {} | Action {} ".format(run,actiontype))
                plt.show()


    action_avg_ap_run_sum = {}
    action_avg_ar_run_sum = {}
    action_avg_gp_run_sum = {}
    action_avg_gr_run_sum = {}
    action_avg_f1_run_sum = {}

    for actiontype in action_avg_ap.keys():
        #f, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, sharex='col', sharey='row')
        gs = gridspec.GridSpec(2, 2)

        plt.figure()



          # row 0, col 1
        #plt.plot([0, 1])

          # row 1, span all columns
        #plt.plot([0, 1])

        for action_count, vals in action_avg_ap[actiontype].items():
            avg = vals[0]
            if len(vals) > 1:
                avg = sum(vals) / len(vals)

            action_avg_ap_run_sum[action_count] = avg

        for action_count, vals in action_avg_ar[actiontype].items():
            avg = vals[0]
            if len(vals) > 1:
                avg = sum(vals) / len(vals)

            action_avg_ar_run_sum[action_count] = avg

        for action_count, vals in action_avg_gp[actiontype].items():
            avg = vals[0]
            if len(vals) > 1:
                avg = sum(vals) / len(vals)

            action_avg_gp_run_sum[action_count] = avg

        for action_count, vals in action_avg_gr[actiontype].items():
            avg = vals[0]
            if len(vals) > 1:
                avg = sum(vals) / len(vals)

            action_avg_gr_run_sum[action_count] = avg

        for action_count, vals in action_avg_f1[actiontype].items():
            avg = vals[0]
            if len(vals) > 1:
                avg = sum(vals) / len(vals)

            action_avg_f1_run_sum[action_count] = avg

        # now sort and graph all 4 per this action
        x = []
        y = []
        ax_p = plt.subplot(gs[0, 0])  # row 0, col 0
        for action_count in sorted(action_avg_ap_run_sum.keys()):
            x.append(action_count)
            y.append(action_avg_ap_run_sum[action_count])
            plt.plot(x, y)
            plt.title('avg_precision')

        x = []
        y = []
        ax_r = plt.subplot(gs[0, 1])
        for action_count in sorted(action_avg_ar_run_sum.keys()):
            x.append(action_count)
            y.append(action_avg_ar_run_sum[action_count])
            plt.plot(x, y)
            plt.title('avg_recall')

        # f1_score
        x = []
        y = []
        ax_f1 = plt.subplot(gs[1, :])
        for action_count in sorted(action_avg_f1_run_sum.keys()):
            x.append(action_count)
            y.append(action_avg_ar_run_sum[action_count])
            plt.plot(x, y)
            plt.title('f1_score')


        # x = []
        # y = []
        # for action_count in sorted(action_avg_gp_run_sum.keys()):
        #     x.append(action_count)
        #     y.append(action_avg_gp_run_sum[action_count])
        #     ax3.plot(x, y)
        #     ax3.set_title('global_precision')
        #
        # x = []
        # y = []
        # for action_count in sorted(action_avg_gr_run_sum.keys()):
        #     x.append(action_count)
        #     y.append(action_avg_gr_run_sum[action_count])
        #     ax4.plot(x, y)
        #     ax4.set_title('global_recall')

        plt.suptitle("Average Across Runs for Action {} ".format(actiontype))
        plt.tight_layout(rect=[0, 0.03, 1, 0.95])
        fn = 'early_graphs_12_17/{}.png'.format(actiontype)
        plt.savefig(fn)
        print("Wrote out new graph to {}".format(fn))
        #plt.show()

    # plt.plot(actions_random,score_random,'-b',label='Random')
    # plt.plot(actions_exploratory3,score_exploratory3,'-r',label='Exploration3')
    # plt.plot(actions_exploratory4,score_exploratory4,'-g',label='Exploration4')
    # plt.plot(actions_exploratory5,score_exploratory5,'-y',label='Exploration5')
    # plt.legend()
    # plt.xlabel("Number of Actions Executed")
    # plt.ylabel("Accuracy")
    # plt.title("Learning Accuracy per Number of Actions Executed")
    # plt.savefig(domain+'_graph.png')

