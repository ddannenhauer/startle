move_se(V1,V3) :- not wall(V1,V2), west(V0,V1), north(V2,V3), agentat(V0,V2).
move_se(V2,V1) :- wall(V0,V1), west(V0,V2), north(V3,V1), agentat(V0,V3).
Pre-processing  : 3.876s
Solve time      : 25872s
Total           : 25875.8s
