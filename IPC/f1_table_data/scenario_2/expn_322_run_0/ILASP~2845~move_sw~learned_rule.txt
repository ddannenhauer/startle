move_sw(V2,V3) :- agentat(V0,V1), west(V2,V0), north(V1,V3), not wall(V2,V3).
Pre-processing  : 5.033s
Solve time      : 8205.85s
Total           : 8210.88s
