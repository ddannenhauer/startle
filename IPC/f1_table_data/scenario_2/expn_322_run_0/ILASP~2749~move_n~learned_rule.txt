move_n(V0,V2) :- agentat(V0,V1), north(V2,V1), not cdoor(V0,V2), not wall(V0,V2).
Pre-processing  : 3.342s
Solve time      : 2797.31s
Total           : 2800.65s
