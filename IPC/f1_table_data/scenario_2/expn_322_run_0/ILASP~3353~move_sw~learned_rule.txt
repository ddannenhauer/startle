move_sw(V2,V3) :- agentat(V0,V1), west(V2,V0), north(V1,V3), not wall(V2,V3).
Pre-processing  : 5.445s
Solve time      : 6285.06s
Total           : 6290.51s
