move_sw(V2,V3) :- agentat(V0,V1), west(V2,V0), north(V1,V3), not wall(V2,V3).
Pre-processing  : 2.042s
Solve time      : 2355.59s
Total           : 2357.64s
