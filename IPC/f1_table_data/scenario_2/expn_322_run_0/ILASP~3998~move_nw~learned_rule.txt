move_nw(V2,V3) :- agentat(V0,V1), odoor(V2,V3), west(V2,V0).
move_nw(V3,V4) :- agentat(V0,V1), odoor(V0,V2), west(V3,V0), north(V4,V1).
move_nw(V2,V3) :- agentat(V0,V1), west(V2,V0), north(V3,V1), wall(V2,V1).
Pre-processing  : 3.97s
Solve time      : 54810.2s
Total           : 54814.2s
