move_nw(V0,V2) :- west(V0,V1), not wall(V1,V2), north(V2,V3), agentat(V1,V3).
Pre-processing  : 5.067s
Solve time      : 5494.36s
Total           : 5499.43s
