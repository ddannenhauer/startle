import copy
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib import cm
import os
import sys
# get the most recent data file

if len(sys.argv) >= 2:
    print("Incorrect number of arguments, correct usage:")
    print("python3 graph_experiment_single_agent.py")
    sys.exit()
else:
    csv_filenames = {}
    csv_filenames['random'] = ['scenario_1/expn_35_run_0_scores.csv','scenario_1/expn_35_run_1_scores.csv','scenario_1/expn_35_run_2_scores.csv']
    csv_filenames['explore'] = ['scenario_1/expn_46_run_0_scores.csv', 'scenario_1/expn_46_run_1_scores.csv', 'scenario_1/expn_46_run_2_scores.csv']
    csv_filenames['planning'] =  ['scenario_1/expn_33_run_0_scores.csv', 'scenario_1/expn_42_run_0_scores.csv', 'scenario_1/expn_209_run_0_scores.csv']

    avg_precision_runs_to_actiontypes_to_scores = {} # example: {'random': {'move_s': {1156 : 0.75, 1523: 0.8}, 'move_w': {1200:0.3, 1600:0.4}}}
    avg_recall_runs_to_actiontypes_to_scores = {}
    global_precision_runs_to_actiontypes_to_scores = {}
    global_recall_runs_to_actiontypes_to_scores = {}
    f1_runs_to_actiontypes_to_scores = {}

    predefined_action_limit = 4000 # this should match the other graphs

    agent_to_max_precision_pre_limit = {} # key is agent, val is list of precision values
    agent_to_max_recall_pre_limit = {}
    agent_to_max_f1_pre_limit = {}

    global_max_action_count = 0
    for agent_str, files in csv_filenames.items():
        for filename in files:
            header = True
            last_action_count = 0
            max_precision_action_count = {} #, key is action, [0] is precision, [1] is action count it was recorded
            max_recall_action_count = {}  # [0] is precision, [1] is action count it was recorded
            max_f1_action_count = {}  # [0] is precision, [1] is action count it was recorded
            with open(filename, 'r') as f:
                for line in f.readlines():
                    if header:
                        header = False
                    else:
                        row = line.strip().split(',')
                        run_id = int(row[0])
                        agent_name = str(row[1])
                        action_name = str(row[2])
                        action_count = int(row[3])
                        avg_precision = float(row[4])
                        avg_recall = float(row[5])
                        global_precision = float(row[6])
                        global_recall = float(row[7])

                        if action_count > global_max_action_count:
                            global_max_action_count = action_count

                        f1_score = 2* (avg_precision * avg_recall / (avg_precision + avg_recall))

                        # setup
                        if action_name not in max_precision_action_count.keys():
                            max_precision_action_count[action_name] = [0,0]

                        if action_name not in max_recall_action_count.keys():
                            max_recall_action_count[action_name] = [0,0]

                        if action_name not in max_f1_action_count.keys():
                            max_f1_action_count[action_name] = [0,0]

                        # store data
                        if action_count > max_precision_action_count[action_name][1] and action_count <= predefined_action_limit:
                            max_precision_action_count[action_name] = [avg_precision,action_count]

                        if action_count > max_recall_action_count[action_name][1] and action_count <= predefined_action_limit:
                            max_recall_action_count[action_name] = [avg_recall,action_count]

                        if action_count > max_f1_action_count[action_name][1] and action_count <= predefined_action_limit:
                            max_f1_action_count[action_name] = [f1_score,action_count]


                        if run_id != 0:
                            print("Problem with {}, there's a run_id != 0".format(filename))
                            sys.exit()

                        # avg_precision setup
                        if agent_str not in avg_precision_runs_to_actiontypes_to_scores.keys():
                            avg_precision_runs_to_actiontypes_to_scores[agent_str] = {}

                        if action_name not in avg_precision_runs_to_actiontypes_to_scores[agent_str].keys():
                            avg_precision_runs_to_actiontypes_to_scores[agent_str][action_name] = {}

                        # avg_recall setup
                        if agent_str not in avg_recall_runs_to_actiontypes_to_scores.keys():
                            avg_recall_runs_to_actiontypes_to_scores[agent_str] = {}

                        if action_name not in avg_recall_runs_to_actiontypes_to_scores[agent_str].keys():
                            avg_recall_runs_to_actiontypes_to_scores[agent_str][action_name] = {}


                        # global_precision setup
                        if agent_str not in global_precision_runs_to_actiontypes_to_scores.keys():
                            global_precision_runs_to_actiontypes_to_scores[agent_str] = {}

                        if action_name not in global_precision_runs_to_actiontypes_to_scores[agent_str].keys():
                            global_precision_runs_to_actiontypes_to_scores[agent_str][action_name] = {}


                        # global_recall setup
                        if agent_str not in global_recall_runs_to_actiontypes_to_scores.keys():
                            global_recall_runs_to_actiontypes_to_scores[agent_str] = {}

                        if action_name not in global_recall_runs_to_actiontypes_to_scores[agent_str].keys():
                            global_recall_runs_to_actiontypes_to_scores[agent_str][action_name] = {}


                        # f1 scores setup
                        if agent_str not in f1_runs_to_actiontypes_to_scores.keys():
                            f1_runs_to_actiontypes_to_scores[agent_str] = {}

                        if action_name not in f1_runs_to_actiontypes_to_scores[agent_str].keys():
                            f1_runs_to_actiontypes_to_scores[agent_str][action_name] = {}


                        # now write the scores for the data points we have
                        if action_count in avg_precision_runs_to_actiontypes_to_scores[agent_str][action_name].keys():
                            avg_precision_runs_to_actiontypes_to_scores[agent_str][action_name][action_count].append(avg_precision)
                        else:
                            avg_precision_runs_to_actiontypes_to_scores[agent_str][action_name][action_count] = [avg_precision]

                        if action_count in avg_recall_runs_to_actiontypes_to_scores[agent_str][action_name].keys():
                            avg_recall_runs_to_actiontypes_to_scores[agent_str][action_name][action_count].append(
                                avg_recall)
                        else:
                            avg_recall_runs_to_actiontypes_to_scores[agent_str][action_name][action_count] = [avg_recall]

                        if action_count in global_precision_runs_to_actiontypes_to_scores[agent_str][action_name].keys():
                            global_precision_runs_to_actiontypes_to_scores[agent_str][action_name][action_count].append(global_precision)
                        else:
                            global_precision_runs_to_actiontypes_to_scores[agent_str][action_name][action_count] = [global_precision]


                        if action_count in global_recall_runs_to_actiontypes_to_scores[agent_str][action_name].keys():
                            global_recall_runs_to_actiontypes_to_scores[agent_str][action_name][action_count].append(global_recall)
                        else:
                            global_recall_runs_to_actiontypes_to_scores[agent_str][action_name][action_count] = [global_recall]


                        if action_count in f1_runs_to_actiontypes_to_scores[agent_str][action_name].keys():
                            f1_runs_to_actiontypes_to_scores[agent_str][action_name][action_count].append(f1_score)
                        else:
                            f1_runs_to_actiontypes_to_scores[agent_str][action_name][action_count] = [f1_score]

                # now store the max values for this run
                if agent_str not in agent_to_max_precision_pre_limit.keys():
                    agent_to_max_precision_pre_limit[agent_str] = [max_precision_action_count]
                else:
                    agent_to_max_precision_pre_limit[agent_str].append(max_precision_action_count)

                if agent_str not in agent_to_max_recall_pre_limit.keys():
                    agent_to_max_recall_pre_limit[agent_str] = [max_recall_action_count]
                else:
                    agent_to_max_recall_pre_limit[agent_str].append(max_recall_action_count)

                if agent_str not in agent_to_max_f1_pre_limit.keys():
                    agent_to_max_f1_pre_limit[agent_str] = [max_f1_action_count]
                else:
                    agent_to_max_f1_pre_limit[agent_str].append(max_f1_action_count)

    agent_order = ['random', 'explore', 'planning']
    move_order = ['move_w', 'move_e', 'move_n', 'move_s', 'move_nw', 'move_ne', 'move_sw', 'move_se',
                  'close_door_w', 'close_door_e', 'close_door_n', 'close_door_s', 'close_door_nw',
                  'close_door_ne',
                  'close_door_sw', 'close_door_se', 'open_door_w', 'open_door_e', 'open_door_n',
                  'open_door_s',
                  'open_door_nw', 'open_door_ne', 'open_door_sw', 'open_door_se']
    metric_order = ['precision', 'recall', 'F1']

    for move in move_order:
        data = [move.replace('_','\\_')]
        for agent in agent_order:
            for metric in metric_order:
                if metric == 'precision':
                    sum = 0
                    len = 0
                    for run in agent_to_max_precision_pre_limit[agent]:
                        if move in run.keys():
                            sum += run[move][0]
                            len+=1
                        else:
                            sum+=0
                            len+=1
                    p = sum / len
                    data.append(p)
                elif metric == 'recall':
                    sum = 0
                    len = 0
                    for run in agent_to_max_recall_pre_limit[agent]:
                        if move in run.keys():
                            sum += run[move][0]
                            len += 1
                        else:
                            sum+=0
                            len+=1
                    r = sum / len
                    data.append(r)
                elif metric == 'F1':
                    sum = 0
                    len = 0
                    for run in agent_to_max_f1_pre_limit[agent]:
                        if move in run.keys():
                            sum += run[move][0]
                            len += 1
                        else:
                            sum += 0
                            len+=1
                    f1 = sum / len
                    data.append(f1)
                else:
                    print("ERROR - unknown metric")

        print("{} & {:.0%} & {:.0%} & {:.0%} & {:.0%} & {:.0%} & {:.0%} & {:.0%} & {:.0%} & {:.0%}\\\\".format(*data).replace('%',''))


    sys.exit()

        # now here fill in the missing blanks
    last_ap = 0
    for action_count in sorted(avg_precision_runs_to_actiontypes_to_scores[agent_str][action_name].keys()):
        for k in range(last_ap,action_count):
            avg_precision_runs_to_actiontypes_to_scores[agent_str][action_name]


    if predefined_action_limit < global_max_action_count:
        global_max_action_count = predefined_action_limit

    graph_each_flag = False

    # left off here, keep track of all scores
    action_avg_ap = {} # example: {'random' : {'move_n': {action_count: list of scores}}}
    action_avg_ar = {}
    action_avg_gp = {}
    action_avg_gr = {}
    action_avg_f1 = {}

    # Now graph each action of each run
    for agent_str in csv_filenames.keys():

        # setup data structures
        if agent_str not in action_avg_ap.keys():
            action_avg_ap[agent_str] = {}

        if agent_str not in action_avg_ar.keys():
            action_avg_ar[agent_str] = {}

        if agent_str not in action_avg_gp.keys():
            action_avg_gp[agent_str] = {}

        if agent_str not in action_avg_gr.keys():
            action_avg_gr[agent_str] = {}

        if agent_str not in action_avg_f1.keys():
            action_avg_f1[agent_str] = {}

        #avg_precision
        for actiontype in avg_precision_runs_to_actiontypes_to_scores[agent_str].keys():

            if actiontype not in action_avg_ap[agent_str].keys():
                action_avg_ap[agent_str][actiontype] = {}

            if actiontype not in action_avg_ar[agent_str].keys():
                action_avg_ar[agent_str][actiontype] = {}

            if actiontype not in action_avg_gp[agent_str].keys():
                action_avg_gp[agent_str][actiontype] = {}

            if actiontype not in action_avg_gr[agent_str].keys():
                action_avg_gr[agent_str][actiontype] = {}

            if actiontype not in action_avg_f1[agent_str].keys():
                action_avg_f1[agent_str][actiontype] = {}

            xvals_ap = []
            yvals_ap = []
            running_ap = 0
            for i in range(global_max_action_count):
                if i in avg_precision_runs_to_actiontypes_to_scores[agent_str][actiontype].keys():
                    running_ap = avg_precision_runs_to_actiontypes_to_scores[agent_str][actiontype][i]

                xvals_ap.append(i)
                yvals_ap.append(running_ap)
                if i in action_avg_ap[agent_str][actiontype].keys():
                    action_avg_ap[agent_str][actiontype][i].append(running_ap)
                else:
                    action_avg_ap[agent_str][actiontype][i] = [running_ap]



            xvals_ar = []
            yvals_ar = []
            running_ar = 0
            for i in range(global_max_action_count):
                if i in avg_recall_runs_to_actiontypes_to_scores[agent_str][actiontype].keys():
                    running_ar = avg_recall_runs_to_actiontypes_to_scores[agent_str][actiontype][i]
                xvals_ar.append(i)
                yvals_ar.append(running_ar)
                if i not in action_avg_ar[agent_str][actiontype].keys():
                    action_avg_ar[agent_str][actiontype][i] = [running_ar]
                else:
                    action_avg_ar[agent_str][actiontype][i].append(running_ar)


            xvals_gp = []
            yvals_gp = []
            running_gp = 0
            for i in range(global_max_action_count):
                if i in global_precision_runs_to_actiontypes_to_scores[agent_str][actiontype].keys():
                    running_gp = global_precision_runs_to_actiontypes_to_scores[agent_str][actiontype][i]
                xvals_gp.append(i)
                yvals_gp.append(running_gp)
                if i not in action_avg_gp[agent_str][actiontype].keys():
                    action_avg_gp[agent_str][actiontype][i] = [running_gp]
                else:
                    action_avg_gp[agent_str][actiontype][i].append(running_gp)

            xvals_gr = []
            yvals_gr = []
            running_gr = 0
            for i in range(global_max_action_count):
                if i in global_recall_runs_to_actiontypes_to_scores[agent_str][actiontype].keys():
                    running_gr = global_recall_runs_to_actiontypes_to_scores[agent_str][actiontype][i]
                xvals_gr.append(i)
                yvals_gr.append(running_gr)
                if i not in action_avg_gr[agent_str][actiontype].keys():
                    action_avg_gr[agent_str][actiontype][i] = [running_gr]
                else:
                    action_avg_gr[agent_str][actiontype][i].append(running_gr)

            xvals_f1 = []
            yvals_f1 = []
            running_f1 = 0
            for i in range(global_max_action_count):
                if i in f1_runs_to_actiontypes_to_scores[agent_str][actiontype].keys():
                    running_f1 = f1_runs_to_actiontypes_to_scores[agent_str][actiontype][i]
                xvals_f1.append(i)
                yvals_f1.append(running_f1)
                if i not in action_avg_f1[agent_str][actiontype].keys():
                    action_avg_f1[agent_str][actiontype][i] = [running_f1]
                else:
                    action_avg_f1[agent_str][actiontype][i].append(running_f1)


    action_avg_ap_run_sum = {}
    action_avg_ar_run_sum = {}
    action_avg_gp_run_sum = {}
    action_avg_gr_run_sum = {}
    action_avg_f1_run_sum = {}

    actiontype_to_f1_plot_data = {} # key is action, val is tuple (x, y)

    agent_to_ap_list_avgs = {} # example: {'random' : {'move_e' : {actioncount: [x1, x2, x3]}}} where
                              # x1 is the average precision from run 1, x2 is from run 2, etc
    agent_to_ar_list_avgs = {}
    agent_to_f1_list_avgs = {}

    for agent_str in csv_filenames.keys():

        if agent_str not in agent_to_ap_list_avgs.keys():
            agent_to_ap_list_avgs[agent_str] = {}

        if agent_str not in agent_to_ar_list_avgs.keys():
            agent_to_ar_list_avgs[agent_str] = {}

        if agent_str not in agent_to_f1_list_avgs.keys():
            agent_to_f1_list_avgs[agent_str] = {}

        for actiontype in action_avg_ap[agent_str].keys():

            if actiontype not in agent_to_ap_list_avgs[agent_str].keys():
                agent_to_ap_list_avgs[agent_str][actiontype] = {}

            if actiontype not in agent_to_ar_list_avgs[agent_str].keys():
                agent_to_ar_list_avgs[agent_str][actiontype] = {}

            if actiontype not in agent_to_f1_list_avgs[agent_str].keys():
                agent_to_f1_list_avgs[agent_str][actiontype] = {}

            for action_count, vals in action_avg_ap[agent_str][actiontype].items():
                avg = vals[0]
                if len(vals) > 1:
                    avg = sum(vals) / len(vals)

                #action_avg_ap_run_sum[action_count] = avg
                if action_count not in agent_to_ap_list_avgs[agent_str][actiontype].keys():
                    agent_to_ap_list_avgs[agent_str][actiontype][action_count] = [avg]
                else:
                    agent_to_ap_list_avgs[agent_str][actiontype][action_count].append(avg)


            for action_count, vals in action_avg_ar[agent_str][actiontype].items():
                avg = vals[0]
                if len(vals) > 1:
                    avg = sum(vals) / len(vals)

                #action_avg_ar_run_sum[action_count] = avg
                if action_count not in agent_to_ar_list_avgs[agent_str][actiontype].keys():
                    agent_to_ar_list_avgs[agent_str][actiontype][action_count] = [avg]
                else:
                    agent_to_ar_list_avgs[agent_str][actiontype][action_count].append(avg)


            # for action_count, vals in action_avg_gp[actiontype].items():
            #     avg = vals[0]
            #     if len(vals) > 1:
            #         avg = sum(vals) / len(vals)
            #
            #     action_avg_gp_run_sum[action_count] = avg

            # for action_count, vals in action_avg_gr[actiontype].items():
            #     avg = vals[0]
            #     if len(vals) > 1:
            #         avg = sum(vals) / len(vals)
            #
            #     action_avg_gr_run_sum[action_count] = avg

            for action_count, vals in action_avg_f1[agent_str][actiontype].items():
                avg = vals[0]
                if len(vals) > 1:
                    avg = sum(vals) / len(vals)

                #action_avg_f1_run_sum[action_count] = avg
                if action_count not in agent_to_f1_list_avgs[agent_str][actiontype].keys():
                    agent_to_f1_list_avgs[agent_str][actiontype][action_count] = [avg]
                else:
                    agent_to_f1_list_avgs[agent_str][actiontype][action_count].append(avg)

    # now go through the table in this order
    agent_order = ['random', 'explore', 'planning']
    move_order = ['move_w', 'move_e','move_n', 'move_s','move_nw', 'move_ne', 'move_sw', 'move_se',
                  'close_door_w', 'close_door_e','close_door_n','close_door_s','close_door_nw','close_door_ne',
                  'close_door_sw','close_door_se','open_door_w','open_door_e','open_door_n','open_door_s',
                  'open_door_nw','open_door_ne','open_door_sw','open_door_se']
    metric_order = ['precision', 'recall', 'F1']

    #max_p_under_limit = sorted(list(filter(lambda x: x<= predefined_action_limit,agent_to_ap_list_avgs[agent][move].keys())))

    #max_r_under_limit = 0
    #max_f1_under_limit = 0


    for move in move_order:
        data = [move]
        for agent in agent_order:
            for metric in metric_order:
                if metric == 'precision':
                    p =  sum(agent_to_ap_list_avgs[agent][move][predefined_action_limit-1]) / len(agent_to_ap_list_avgs[agent][move][predefined_action_limit-1])
                    data.append(p)
                elif metric == 'recall':
                    r = sum(agent_to_ar_list_avgs[agent][move][predefined_action_limit-1]) / len(
                        agent_to_ar_list_avgs[agent][move][predefined_action_limit-1])
                    data.append(r)
                elif metric == 'F1':
                    f1 = sum(agent_to_f1_list_avgs[agent][move][predefined_action_limit-1]) / len(
                        agent_to_f1_list_avgs[agent][move][predefined_action_limit-1])
                    data.append(f1)
                else:
                    print("ERROR - unknown metric")

        print("{} & {} & {} & {}& {} & {} & {}& {} & {} & {}\\\\".format(*data))


    #
    # for agent_str in agent_to_ap_list_avgs.keys():
    #     for movename in agent_to_ap_list_avgs[agent_str]
    #
    #
    #         # now sort and graph all 4 per this action
    #         x = []
    #         y = []
    #         ax_p = plt.subplot(gs[0, 0])  # row 0, col 0
    #         for action_count in sorted(action_avg_ap_run_sum.keys()):
    #             x.append(action_count)
    #             y.append(action_avg_ap_run_sum[action_count])
    #         plt.plot(x, y)
    #         plt.title('avg_precision')
    #
    #         x = []
    #         y = []
    #         ax_r = plt.subplot(gs[0, 1])
    #         for action_count in sorted(action_avg_ar_run_sum.keys()):
    #             x.append(action_count)
    #             y.append(action_avg_ar_run_sum[action_count])
    #         plt.plot(x, y)
    #         plt.title('avg_recall')
    #
    #         # f1_score
    #         x = []
    #         y = []
    #         ax_f1 = plt.subplot(gs[1, :])
    #         for action_count in sorted(action_avg_f1_run_sum.keys()):
    #             x.append(action_count)
    #             y.append(action_avg_ar_run_sum[action_count])
    #         actiontype_to_f1_plot_data[actiontype] = (x, y)
    #         plt.plot(x, y)
    #         plt.title('f1_score')
    #
    #
    #         # x = []
    #         # y = []
    #         # for action_count in sorted(action_avg_gp_run_sum.keys()):
    #         #     x.append(action_count)
    #         #     y.append(action_avg_gp_run_sum[action_count])
    #         #     ax3.plot(x, y)
    #         #     ax3.set_title('global_precision')
    #         #
    #         # x = []
    #         # y = []
    #         # for action_count in sorted(action_avg_gr_run_sum.keys()):
    #         #     x.append(action_count)
    #         #     y.append(action_avg_gr_run_sum[action_count])
    #         #     ax4.plot(x, y)
    #         #     ax4.set_title('global_recall')
    #
    #         plt.suptitle("Average Across Runs for Action {} ".format(actiontype))
    #         plt.tight_layout(rect=[0, 0.03, 1, 0.95])
    #         fn = 'early_graphs_12_17_smoother/{}.png'.format(actiontype)
    #         #plt.savefig(fn)
    #         #print("Wrote out new graph to {}".format(fn))
    #         #plt.show()
    #
    # # print("Now graphing all F1 scores for move actions on one big figure")
    # # move_actions = ['move_n','move_ne','move_nw','move_s','move_se','move_sw','move_e','move_w']
    # # fig, axes = plt.subplots(nrows=8, ncols=1)#, figsize=(5, 5))
    # #
    # #
    # # row = 0
    # # #first_x_tick_labels = None
    # # #first_y_tick_labels = None
    # # for m_action in move_actions:
    # #     axes[row].set_title("Planning")
    # #     axes[row].plot(*actiontype_to_f1_plot_data[m_action])
    # #     axes[row].set_xlabel("F1 Score")
    # #     axes[row].set_ylabel("Actions Executed")
    # #
    # # fig.tight_layout()
    # # plt.show()
    #
    # # plt.plot(actions_random,score_random,'-b',label='Random')
    # # plt.plot(actions_exploratory3,score_exploratory3,'-r',label='Exploration3')
    # # plt.plot(actions_exploratory4,score_exploratory4,'-g',label='Exploration4')
    # # plt.plot(actions_exploratory5,score_exploratory5,'-y',label='Exploration5')
    # # plt.legend()
    # # plt.xlabel("Number of Actions Executed")
    # # plt.ylabel("Accuracy")
    # # plt.title("Learning Accuracy per Number of Actions Executed")
    # # plt.savefig(domain+'_graph.png')
    #
