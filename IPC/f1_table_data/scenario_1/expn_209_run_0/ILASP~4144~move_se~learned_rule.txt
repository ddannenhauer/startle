move_se(V2,V1) :- cdoor(V0,V1), west(V0,V2), agentat(V0,V3).
move_se(V0,V3) :- odoor(V0,V1), north(V2,V3), west(V4,V0), agentat(V4,V2).
Pre-processing  : 2.927s
Solve time      : 54808.5s
Total           : 54811.4s
