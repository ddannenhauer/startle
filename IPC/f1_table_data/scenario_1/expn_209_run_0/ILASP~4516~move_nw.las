
#modeh(move_nw(var(xcoord),var(ycoord))).

#pos({move_nw(x9,y4)}, {},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
odoor(x8,y4).
agentat(x8,y3).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x8,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
agentat(x1, y1).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x4,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
agentat(x1, y1).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x1,y1)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
agentat(x1, y1).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x1,y2)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
agentat(x1, y1).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x2,y3)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
agentat(x1, y1).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x2,y1)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
agentat(x1, y1).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x8,y5)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
agentat(x1, y1).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x8,y3)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
agentat(x1, y1).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x9,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
agentat(x1, y1).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x7,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x1,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x6,y1)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x1,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x5,y1)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x4,y1)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x8,y1)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x9,y3)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x8,y2)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x1,y1)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x7,y3)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x4,y3)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x2,y3)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x4,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x8,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x4,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x4,y3)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x3,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x6,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x3,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x8,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x3,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x7,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x3,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x7,y1)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x3,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x6,y3)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x3,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x2,y5)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x3,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x3,y1)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x3,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x7,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x4,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x5,y1)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x4,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x3,y5)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x4,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x3,y3)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x4,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x2,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x4,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x5,y5)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x4,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x3,y2)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x4,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x3,y2)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x6,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x6,y1)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x2,y1)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x9,y1)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x6,y2)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x1,y3)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x2,y2)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x4,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x6,y5)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x4,y2)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x1,y5)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x7,y2)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x8,y3)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x3,y5)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x7,y1)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x3,y1)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x9,y5)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x4,y5)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x5,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x5,y3)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x5,y2)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x8,y5)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x3,y3)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x1,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x2,y5)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x9,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x7,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x2,y3)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x7,y5)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x3,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x1,y2)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x5,y5)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x9,y2)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x8,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x2,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x6,y3)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x6,y3)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x9,y2)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x1,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x9,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x9,y5)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x2,y3)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x8,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x9,y1)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x7,y5)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x3,y3)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x8,y3)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x1,y3)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x5,y3)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x3,y2)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x6,y5)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x2,y1)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x7,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x7,y2)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x1,y2)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x6,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x9,y3)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x1,y1)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x5,y1).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x1,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x1,y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x1,y3)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x1,y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x8,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x1,y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x8,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
cdoor(x8, y4).
north(y4, y3).
wall(x3, y4).
agentat(x8,y5).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x8,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
odoor(x8,y4).
agentat(x8,y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x8,y3)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
odoor(x8,y4).
agentat(x8,y3).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x7,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
odoor(x8,y4).
agentat(x9,y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x8,y5)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
odoor(x8,y4).
agentat(x9,y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x3,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
odoor(x8,y4).
agentat(x9,y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x7,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
odoor(x8,y4).
agentat(x7,y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x8,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
odoor(x8,y4).
agentat(x7,y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x9,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
odoor(x8,y4).
agentat(x6,y5).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x9,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
odoor(x8,y4).
agentat(x6,y5).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x9,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
odoor(x8,y4).
agentat(x6,y5).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x8,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
odoor(x8,y4).
agentat(x8,y5).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x8,y5)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
agentat(x8,y5).
cdoor(x8,y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x7,y3)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
agentat(x8,y5).
odoor(x8,y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x7,y5)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
agentat(x8,y5).
odoor(x8,y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x1,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
agentat(x8,y5).
odoor(x8,y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x9,y3)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
agentat(x8,y5).
odoor(x8,y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x3,y2)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
agentat(x8,y5).
odoor(x8,y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x3,y5)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
agentat(x8,y5).
odoor(x8,y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x5,y5)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
agentat(x8,y5).
odoor(x8,y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x7,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
agentat(x8,y5).
odoor(x8,y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x2,y5)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
agentat(x8,y5).
odoor(x8,y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x2,y3)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
agentat(x8,y5).
odoor(x8,y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x5,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
agentat(x8,y5).
odoor(x8,y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x4,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
agentat(x8,y5).
odoor(x8,y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x7,y3)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
cdoor(x8,y4).
agentat(x9,y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x1,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
cdoor(x8,y4).
agentat(x9,y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x6,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
cdoor(x8,y4).
agentat(x9,y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x4,y5)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
cdoor(x8,y4).
agentat(x8,y3).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x9,y5)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
cdoor(x8,y4).
agentat(x8,y3).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x2,y1)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
cdoor(x8,y4).
agentat(x8,y3).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x9,y3)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
cdoor(x8,y4).
agentat(x8,y3).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x1,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
cdoor(x8,y4).
agentat(x8,y3).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x3,y3)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
cdoor(x8,y4).
agentat(x8,y3).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x7,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
cdoor(x8,y4).
agentat(x8,y3).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x8,y1)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
cdoor(x8,y4).
agentat(x8,y3).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x3,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
cdoor(x8,y4).
agentat(x8,y3).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x7,y5)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
cdoor(x8,y4).
agentat(x8,y3).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x2,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
cdoor(x8,y4).
agentat(x9,y3).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x8,y2)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
cdoor(x8,y4).
agentat(x9,y3).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x1,y3)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
cdoor(x8,y4).
agentat(x9,y3).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x7,y2)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
cdoor(x8,y4).
agentat(x9,y3).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x8,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
cdoor(x8,y4).
agentat(x9,y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x5,y3)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
cdoor(x8,y4).
agentat(x9,y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x3,y5)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
cdoor(x8,y4).
agentat(x9,y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x1,y5)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
cdoor(x8,y4).
agentat(x9,y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x1,y1)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
cdoor(x8,y4).
agentat(x9,y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x7,y2)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
cdoor(x8,y4).
agentat(x9,y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x2,y5)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
cdoor(x8,y4).
agentat(x9,y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x2,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
cdoor(x8,y4).
agentat(x9,y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x9,y3)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
cdoor(x8,y4).
agentat(x9,y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x2,y3)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
cdoor(x8,y4).
agentat(x9,y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x6,y3)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
cdoor(x8,y4).
agentat(x9,y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x4,y5)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
cdoor(x8,y4).
agentat(x9,y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x1,y5)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
agentat(x9,y4).
odoor(x8,y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x4,y5)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
odoor(x8,y4).
agentat(x9,y3).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x5,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
odoor(x8,y4).
agentat(x9,y3).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x4,y2)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
odoor(x8,y4).
agentat(x9,y3).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x9,y3)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
odoor(x8,y4).
agentat(x9,y3).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x6,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
odoor(x8,y4).
agentat(x9,y3).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x5,y1)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
odoor(x8,y4).
agentat(x9,y3).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x2,y1)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
cdoor(x8,y4).
agentat(x7,y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x1,y3)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
cdoor(x8,y4).
agentat(x7,y4).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x7,y2)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
cdoor(x8,y4).
agentat(x7,y3).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x6,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
cdoor(x8,y4).
agentat(x7,y3).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x1,y5)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
cdoor(x8,y4).
agentat(x7,y3).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x8,y2)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
cdoor(x8,y4).
agentat(x7,y3).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x3,y5)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
cdoor(x8,y4).
agentat(x7,y3).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x9,y3)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
cdoor(x8,y4).
agentat(x7,y3).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x3,y2)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
cdoor(x8,y4).
agentat(x7,y3).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x8,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
cdoor(x8,y4).
agentat(x7,y3).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x8,y3)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
cdoor(x8,y4).
agentat(x7,y3).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x8,y5)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
cdoor(x8,y4).
agentat(x7,y3).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x7,y4)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
cdoor(x8,y4).
agentat(x7,y3).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x4,y5)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
cdoor(x8,y4).
agentat(x7,y3).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#pos({}, {move_nw(x7,y1)},{wall(x3, y2).
west(x2, x1).
wall(x6, y1).
wall(x2, y4).
west(x6, x5).
west(x4, x3).
west(x7, x6).
north(y2, y1).
north(y3, y2).
west(x5, x4).
west(x9, x8).
wall(x2, y2).
wall(x4, y4).
north(y5, y4).
wall(x4, y2).
wall(x1, y2).
wall(x6, y4).
west(x3, x2).
west(x8, x7).
wall(x5, y4).
wall(x6, y3).
wall(x6, y2).
north(y4, y3).
wall(x3, y4).
cdoor(x8,y4).
agentat(x7,y3).
xcoord(x3).
xcoord(x1).
xcoord(x4).
xcoord(x5).
ycoord(y2).
ycoord(y5).
ycoord(y4).
xcoord(x2).
xcoord(x6).
xcoord(x7).
ycoord(y3).
ycoord(y1).
xcoord(x9).
xcoord(x8).
}).

#modeb(1,odoor(var(xcoord),var(ycoord))).
#modeb(1,north(var(ycoord),var(ycoord))).
#modeb(1,ycoord(var(ycoord))).
#modeb(1,cdoor(var(xcoord),var(ycoord))).
#modeb(1,xcoord(var(xcoord))).
#modeb(1,wall(var(xcoord),var(ycoord))).
#modeb(1,west(var(xcoord),var(xcoord))).
#modeb(1,agentat(var(xcoord),var(ycoord))).
#maxv(6).
