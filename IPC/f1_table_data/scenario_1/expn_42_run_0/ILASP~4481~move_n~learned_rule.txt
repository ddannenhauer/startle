move_n(V2,V0) :- north(V0,V1), agentat(V2,V1), not cdoor(V2,V0), not wall(V2,V0).
Pre-processing  : 7.497s
Solve time      : 11415.8s
Total           : 11423.3s
