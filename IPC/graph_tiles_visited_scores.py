import copy
import matplotlib.pyplot as plt
from matplotlib import cm
import os
import sys
# get the most recent data file

if len(sys.argv) < 2:
    print("Incorrect number of arguments, correct usage:")
    print("python3 graph_tiles_visited_scores.py <csv filename>")
    print("<csv filename> results from single agent run of data from compute scores in the form of a .csv file")
    sys.exit()
else:
    csvfilename=sys.argv[1]

    agents_to_runs_to_scores = {} # key is agent ('random', 'explore', 'planning'),
                                  # value is a dict mapping runs to score

    action_limit = 4000

    print("Reading in score file...")
    header = True
    with open(csvfilename, 'r') as f:
        for line in f.readlines():
            if header:
                header = False
            else:
                row = line.strip().split(',')
                agent_type = row[0]
                run_id = int(row[1])
                action_count = int(row[2])
                running_score = int(row[3])

                if agent_type not in agents_to_runs_to_scores.keys():
                    agents_to_runs_to_scores[agent_type] = {}

                if run_id not in agents_to_runs_to_scores[agent_type].keys():
                    agents_to_runs_to_scores[agent_type][run_id] = [running_score]
                else:
                    agents_to_runs_to_scores[agent_type][run_id].append(running_score)

    print("Computing averages and max/min scores")
    agents_to_average_run = {} # key is agent type, value is average run scores
    agents_to_max_run = {}  # key is agent type, value is max run scores
    agents_to_min_run = {}  # key is agent type, value is min run scores

    overall_max_of_all_runs = 0 # for graph visualization purposes

    for agent in agents_to_runs_to_scores.keys():
        for i in range(action_limit):
            running_avg = 0
            num_runs = 0
            curr_min = None
            curr_max = None
            for run in agents_to_runs_to_scores[agent].keys():
                score = agents_to_runs_to_scores[agent][run][i]
                running_avg += score
                num_runs += 1

                # do min
                if curr_min:
                    if score < curr_min:
                        curr_min = score
                    else:
                        pass
                else:
                    curr_min = score

                # do max
                if curr_max:
                    if score > curr_max:
                        curr_max = score
                    else:
                        pass
                else:
                    curr_max = score

                if score > overall_max_of_all_runs:
                    overall_max_of_all_runs = score

            avg = running_avg / num_runs # this should work I believe

            if agent not in agents_to_average_run.keys():
                agents_to_average_run[agent] = []
                agents_to_max_run[agent] = []
                agents_to_min_run[agent] = []

            agents_to_average_run[agent].append(avg)
            agents_to_max_run[agent].append(curr_max)
            agents_to_min_run[agent].append(curr_min)


    line_styles = {'random':'-r','explore':'-b','planning':'-g'}
    face_colors = {'random':'red', 'explore':'blue', 'planning':'green'}
    line_style_i = 0
    for agent in agents_to_average_run.keys():
        plt.plot(range(1,len(agents_to_average_run[agent])+1),agents_to_average_run[agent],line_styles[agent],label=agent, linewidth=3.0)
        plt.fill_between(range(1,len(agents_to_average_run[agent])+1), agents_to_max_run[agent], agents_to_min_run[agent], facecolor=face_colors[agent], alpha=0.2)
        line_style_i+=1
        plt.ylim(1, overall_max_of_all_runs)
        plt.xlim(left=1)
        plt.legend()
        plt.show()

    line_style_i = 0
    agent_plots = {}
    for agent in agents_to_average_run.keys():
        agent_plots[agent], = plt.plot(range(1, len(agents_to_average_run[agent]) + 1), agents_to_average_run[agent],
                 line_styles[agent], label=agent, linewidth=3.0)
        plt.fill_between(range(1, len(agents_to_average_run[agent]) + 1), agents_to_max_run[agent],
                         agents_to_min_run[agent], facecolor=face_colors[agent], alpha=0.2)
        plt.ylim(1,overall_max_of_all_runs)
        plt.xlim(left=1)
        line_style_i += 1

    plt.legend([agent_plots['random'],agent_plots['explore'], agent_plots['planning']], ['Random', 'Explore Local', 'Planning'])
    plt.xlabel("Number of Actions")
    plt.ylabel("Number of Unique Tiles Visited")
    plt.title("Unique Tiles Visited per Action Selection Approach")
    plt.show()

