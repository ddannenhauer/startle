import re
class Action:

    def __init__(self,name:str,msg:{},args=None):
        self.name = name
        self.msg = msg
        self.args = args
    def _hash__(self):
        return hash(self.name+str(self.args))

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return self.name

    def get_json(self):
        return self.msg


class CrawlAIAgent:


    def next_action(self):
        raise NotImplementedError("An agent must implement next_action() so that it will return an action to the game")

    def add_server_message(self,json_msg:{}):
        raise NotImplementedError("An agent must implement add_server_message which takes the most recent server"
                                  " message. This message is mostly used to ensure the agent has a current state of"
                                  " the game")

    def ready_to_delete_game(self):
        raise NotImplementedError("An agent must implement ready_to_delete_game() which denotes when the agent should"
                                  " quit and delete the game")

    def initiate_actions(self,domprob):
        raise NotImplementedError("An agent must implement initiate_actions(domprob) which takes in a pddlpy.DomainProblem object and creates the action set")

    def create_input_action(text):
        for a in CrawlAIAgent.all_actions:
            if a.get_json()['msg'] == 'input' and a.get_json()['text'] == text:
                return a

        return Action('undefined', {'msg': 'input', 'text': text})


    loc={'x':None,'y':None}
    # movement
    _move_N_h = Action('north_h', {'msg': 'key', 'keycode': -254})

                     
    all_actions = []
    all_actions_by_name={}
    for action in all_actions:
        all_actions_by_name[str(action)]=action
    print(all_actions_by_name.keys())

    def get_action_from_str(self,name):
        for action in self.all_actions:
            if action.name==name:
                return action
        return None

