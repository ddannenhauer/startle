# new state begin
cdoor(x2, y2).
west(x0, x1).
west(x2, x1).
west(x3, x2).
west(x4, x3).
north(y4, y3).
north(y3, y2).
north(y2, y1).
north(y0, y1),
xcoord(x0).
xcoord(x1).
xcoord(x2).
xcoord(x3).
xcoord(x4).
ycoord(y0).
ycoord(y1).
ycoord(y2).
ycoord(y3).
ycoord(y4).
wall(x0, y0).
wall(x0, y1).
wall(x0, y2).
wall(x0, y3).
wall(x0, y4).
wall(x1, y0).
wall(x2, y0).
wall(x3, y0).
wall(x4, y0).
wall(x4, y1).
wall(x4, y2).
wall(x4, y3).
wall(x1, y4).
wall(x2, y4).
wall(x3, y4).
wall(x4, y4).
agentat(x1, y1).
# new state end

# new state begin
cdoor(x2, y2).
west(x0, x1).
west(x2, x1).
west(x3, x2).
west(x4, x3).
north(y4, y3).
north(y3, y2).
north(y2, y1).
north(y0, y1),
xcoord(x0).
xcoord(x1).
xcoord(x2).
xcoord(x3).
xcoord(x4).
ycoord(y0).
ycoord(y1).
ycoord(y2).
ycoord(y3).
ycoord(y4).
wall(x0, y0).
wall(x0, y1).
wall(x0, y2).
wall(x0, y3).
wall(x0, y4).
wall(x1, y0).
wall(x2, y0).
wall(x3, y0).
wall(x4, y0).
wall(x4, y1).
wall(x4, y2).
wall(x4, y3).
wall(x1, y4).
wall(x2, y4).
wall(x3, y4).
wall(x4, y4).
agentat(x2, y1).
# new state end

# new state begin
cdoor(x2, y2).
west(x0, x1).
west(x2, x1).
west(x3, x2).
west(x4, x3).
north(y4, y3).
north(y3, y2).
north(y2, y1).
north(y0, y1),
xcoord(x0).
xcoord(x1).
xcoord(x2).
xcoord(x3).
xcoord(x4).
ycoord(y0).
ycoord(y1).
ycoord(y2).
ycoord(y3).
ycoord(y4).
wall(x0, y0).
wall(x0, y1).
wall(x0, y2).
wall(x0, y3).
wall(x0, y4).
wall(x1, y0).
wall(x2, y0).
wall(x3, y0).
wall(x4, y0).
wall(x4, y1).
wall(x4, y2).
wall(x4, y3).
wall(x1, y4).
wall(x2, y4).
wall(x3, y4).
wall(x4, y4).
agentat(x3, y1).
# new state end

# new state begin
cdoor(x2, y2).
west(x0, x1).
west(x2, x1).
west(x3, x2).
west(x4, x3).
north(y4, y3).
north(y3, y2).
north(y2, y1).
north(y0, y1),
xcoord(x0).
xcoord(x1).
xcoord(x2).
xcoord(x3).
xcoord(x4).
ycoord(y0).
ycoord(y1).
ycoord(y2).
ycoord(y3).
ycoord(y4).
wall(x0, y0).
wall(x0, y1).
wall(x0, y2).
wall(x0, y3).
wall(x0, y4).
wall(x1, y0).
wall(x2, y0).
wall(x3, y0).
wall(x4, y0).
wall(x4, y1).
wall(x4, y2).
wall(x4, y3).
wall(x1, y4).
wall(x2, y4).
wall(x3, y4).
wall(x4, y4).
agentat(x1, y2).
# new state end

# new state begin
cdoor(x2, y2).
west(x0, x1).
west(x2, x1).
west(x3, x2).
west(x4, x3).
north(y4, y3).
north(y3, y2).
north(y2, y1).
north(y0, y1),
xcoord(x0).
xcoord(x1).
xcoord(x2).
xcoord(x3).
xcoord(x4).
ycoord(y0).
ycoord(y1).
ycoord(y2).
ycoord(y3).
ycoord(y4).
wall(x0, y0).
wall(x0, y1).
wall(x0, y2).
wall(x0, y3).
wall(x0, y4).
wall(x1, y0).
wall(x2, y0).
wall(x3, y0).
wall(x4, y0).
wall(x4, y1).
wall(x4, y2).
wall(x4, y3).
wall(x1, y4).
wall(x2, y4).
wall(x3, y4).
wall(x4, y4).
agentat(x1, y3).
# new state end

# new state begin
cdoor(x2, y2).
west(x0, x1).
west(x2, x1).
west(x3, x2).
west(x4, x3).
north(y4, y3).
north(y3, y2).
north(y2, y1).
north(y0, y1),
xcoord(x0).
xcoord(x1).
xcoord(x2).
xcoord(x3).
xcoord(x4).
ycoord(y0).
ycoord(y1).
ycoord(y2).
ycoord(y3).
ycoord(y4).
wall(x0, y0).
wall(x0, y1).
wall(x0, y2).
wall(x0, y3).
wall(x0, y4).
wall(x1, y0).
wall(x2, y0).
wall(x3, y0).
wall(x4, y0).
wall(x4, y1).
wall(x4, y2).
wall(x4, y3).
wall(x1, y4).
wall(x2, y4).
wall(x3, y4).
wall(x4, y4).
agentat(x2, y3).
# new state end

# new state begin
cdoor(x2, y2).
west(x0, x1).
west(x2, x1).
west(x3, x2).
west(x4, x3).
north(y4, y3).
north(y3, y2).
north(y2, y1).
north(y0, y1),
xcoord(x0).
xcoord(x1).
xcoord(x2).
xcoord(x3).
xcoord(x4).
ycoord(y0).
ycoord(y1).
ycoord(y2).
ycoord(y3).
ycoord(y4).
wall(x0, y0).
wall(x0, y1).
wall(x0, y2).
wall(x0, y3).
wall(x0, y4).
wall(x1, y0).
wall(x2, y0).
wall(x3, y0).
wall(x4, y0).
wall(x4, y1).
wall(x4, y2).
wall(x4, y3).
wall(x1, y4).
wall(x2, y4).
wall(x3, y4).
wall(x4, y4).
agentat(x3, y2).
# new state end

# new state begin
cdoor(x2, y2).
west(x0, x1).
west(x2, x1).
west(x3, x2).
west(x4, x3).
north(y4, y3).
north(y3, y2).
north(y2, y1).
north(y0, y1),
xcoord(x0).
xcoord(x1).
xcoord(x2).
xcoord(x3).
xcoord(x4).
ycoord(y0).
ycoord(y1).
ycoord(y2).
ycoord(y3).
ycoord(y4).
wall(x0, y0).
wall(x0, y1).
wall(x0, y2).
wall(x0, y3).
wall(x0, y4).
wall(x1, y0).
wall(x2, y0).
wall(x3, y0).
wall(x4, y0).
wall(x4, y1).
wall(x4, y2).
wall(x4, y3).
wall(x1, y4).
wall(x2, y4).
wall(x3, y4).
wall(x4, y4).
agentat(x3, y3).
# new state end

# new state begin
odoor(x2, y2).
west(x0, x1).
west(x2, x1).
west(x3, x2).
west(x4, x3).
north(y4, y3).
north(y3, y2).
north(y2, y1).
north(y0, y1),
xcoord(x0).
xcoord(x1).
xcoord(x2).
xcoord(x3).
xcoord(x4).
ycoord(y0).
ycoord(y1).
ycoord(y2).
ycoord(y3).
ycoord(y4).
wall(x0, y0).
wall(x0, y1).
wall(x0, y2).
wall(x0, y3).
wall(x0, y4).
wall(x1, y0).
wall(x2, y0).
wall(x3, y0).
wall(x4, y0).
wall(x4, y1).
wall(x4, y2).
wall(x4, y3).
wall(x1, y4).
wall(x2, y4).
wall(x3, y4).
wall(x4, y4).
agentat(x1, y1).
# new state end

# new state begin
odoor(x2, y2).
west(x0, x1).
west(x2, x1).
west(x3, x2).
west(x4, x3).
north(y4, y3).
north(y3, y2).
north(y2, y1).
north(y0, y1),
xcoord(x0).
xcoord(x1).
xcoord(x2).
xcoord(x3).
xcoord(x4).
ycoord(y0).
ycoord(y1).
ycoord(y2).
ycoord(y3).
ycoord(y4).
wall(x0, y0).
wall(x0, y1).
wall(x0, y2).
wall(x0, y3).
wall(x0, y4).
wall(x1, y0).
wall(x2, y0).
wall(x3, y0).
wall(x4, y0).
wall(x4, y1).
wall(x4, y2).
wall(x4, y3).
wall(x1, y4).
wall(x2, y4).
wall(x3, y4).
wall(x4, y4).
agentat(x2, y1).
# new state end

# new state begin
odoor(x2, y2).
west(x0, x1).
west(x2, x1).
west(x3, x2).
west(x4, x3).
north(y4, y3).
north(y3, y2).
north(y2, y1).
north(y0, y1),
xcoord(x0).
xcoord(x1).
xcoord(x2).
xcoord(x3).
xcoord(x4).
ycoord(y0).
ycoord(y1).
ycoord(y2).
ycoord(y3).
ycoord(y4).
wall(x0, y0).
wall(x0, y1).
wall(x0, y2).
wall(x0, y3).
wall(x0, y4).
wall(x1, y0).
wall(x2, y0).
wall(x3, y0).
wall(x4, y0).
wall(x4, y1).
wall(x4, y2).
wall(x4, y3).
wall(x1, y4).
wall(x2, y4).
wall(x3, y4).
wall(x4, y4).
agentat(x3, y1).
# new state end

# new state begin
odoor(x2, y2).
west(x0, x1).
west(x2, x1).
west(x3, x2).
west(x4, x3).
north(y4, y3).
north(y3, y2).
north(y2, y1).
north(y0, y1),
xcoord(x0).
xcoord(x1).
xcoord(x2).
xcoord(x3).
xcoord(x4).
ycoord(y0).
ycoord(y1).
ycoord(y2).
ycoord(y3).
ycoord(y4).
wall(x0, y0).
wall(x0, y1).
wall(x0, y2).
wall(x0, y3).
wall(x0, y4).
wall(x1, y0).
wall(x2, y0).
wall(x3, y0).
wall(x4, y0).
wall(x4, y1).
wall(x4, y2).
wall(x4, y3).
wall(x1, y4).
wall(x2, y4).
wall(x3, y4).
wall(x4, y4).
agentat(x1, y2).
# new state end

# new state begin
odoor(x2, y2).
west(x0, x1).
west(x2, x1).
west(x3, x2).
west(x4, x3).
north(y4, y3).
north(y3, y2).
north(y2, y1).
north(y0, y1),
xcoord(x0).
xcoord(x1).
xcoord(x2).
xcoord(x3).
xcoord(x4).
ycoord(y0).
ycoord(y1).
ycoord(y2).
ycoord(y3).
ycoord(y4).
wall(x0, y0).
wall(x0, y1).
wall(x0, y2).
wall(x0, y3).
wall(x0, y4).
wall(x1, y0).
wall(x2, y0).
wall(x3, y0).
wall(x4, y0).
wall(x4, y1).
wall(x4, y2).
wall(x4, y3).
wall(x1, y4).
wall(x2, y4).
wall(x3, y4).
wall(x4, y4).
agentat(x1, y3).
# new state end

# new state begin
odoor(x2, y2).
west(x0, x1).
west(x2, x1).
west(x3, x2).
west(x4, x3).
north(y4, y3).
north(y3, y2).
north(y2, y1).
north(y0, y1),
xcoord(x0).
xcoord(x1).
xcoord(x2).
xcoord(x3).
xcoord(x4).
ycoord(y0).
ycoord(y1).
ycoord(y2).
ycoord(y3).
ycoord(y4).
wall(x0, y0).
wall(x0, y1).
wall(x0, y2).
wall(x0, y3).
wall(x0, y4).
wall(x1, y0).
wall(x2, y0).
wall(x3, y0).
wall(x4, y0).
wall(x4, y1).
wall(x4, y2).
wall(x4, y3).
wall(x1, y4).
wall(x2, y4).
wall(x3, y4).
wall(x4, y4).
agentat(x2, y3).
# new state end

# new state begin
odoor(x2, y2).
west(x0, x1).
west(x2, x1).
west(x3, x2).
west(x4, x3).
north(y4, y3).
north(y3, y2).
north(y2, y1).
north(y0, y1),
xcoord(x0).
xcoord(x1).
xcoord(x2).
xcoord(x3).
xcoord(x4).
ycoord(y0).
ycoord(y1).
ycoord(y2).
ycoord(y3).
ycoord(y4).
wall(x0, y0).
wall(x0, y1).
wall(x0, y2).
wall(x0, y3).
wall(x0, y4).
wall(x1, y0).
wall(x2, y0).
wall(x3, y0).
wall(x4, y0).
wall(x4, y1).
wall(x4, y2).
wall(x4, y3).
wall(x1, y4).
wall(x2, y4).
wall(x3, y4).
wall(x4, y4).
agentat(x3, y2).
# new state end

# new state begin
odoor(x2, y2).
west(x0, x1).
west(x2, x1).
west(x3, x2).
west(x4, x3).
north(y4, y3).
north(y3, y2).
north(y2, y1).
north(y0, y1),
xcoord(x0).
xcoord(x1).
xcoord(x2).
xcoord(x3).
xcoord(x4).
ycoord(y0).
ycoord(y1).
ycoord(y2).
ycoord(y3).
ycoord(y4).
wall(x0, y0).
wall(x0, y1).
wall(x0, y2).
wall(x0, y3).
wall(x0, y4).
wall(x1, y0).
wall(x2, y0).
wall(x3, y0).
wall(x4, y0).
wall(x4, y1).
wall(x4, y2).
wall(x4, y3).
wall(x1, y4).
wall(x2, y4).
wall(x3, y4).
wall(x4, y4).
agentat(x3, y3).
# new state end

